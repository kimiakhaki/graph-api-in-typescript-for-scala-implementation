import sbt.Keys.mainClass

ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "3.3.1"

stUseScalaJsDom := false

jsEnv := new org.scalajs.jsenv.nodejs.NodeJSEnv()


val sharedSettings = libraryDependencies ++= Seq(
  "org.scalablytyped" %%% "Scala-TS-Facade" % "1.0.0-292320",
  "org.scalablytyped" %%% "typescript" % "5.3.3-4d8fa4"
)

lazy val bar =
  crossProject(JSPlatform, JVMPlatform).in(file(".")).settings(
      name := crossProjectPlatform.value.identifier
    ).
    enablePlugins(ScalablyTypedConverterPlugin).
    enablePlugins(ScalaJSBundlerPlugin).
    settings(
    ).
    settings(
      sharedSettings
    ).
    jvmSettings(
    ).
    jsSettings(
      scalaJSUseMainModuleInitializer := true,
        mainClass := Some("MyMainShared")

    )

lazy val root = project.in(file(".")).
  aggregate(bar.js, bar.jvm).
  settings(
    name := "Scala-TS-Facade",
    publish := {},
    publishLocal := {},
  )