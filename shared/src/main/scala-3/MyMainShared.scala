import graph_ts_facade._
import scala.scalajs.js

object MyMainShared {

  def main(args: Array[String]): Unit = {

    val myNode1: Node = new Node("1", "NodeA")
    val myNode2: Node = new Node("2","NodeB")

    val myEdge1: Edge = new Edge("1","EdgeA", myNode1, myNode2)


    val myGraph: Graph[Node, Edge] = new Graph[Node, Edge]("1", "GraphA", js.Array(myNode1, myNode2), js.Array(myEdge1))

  }
}
