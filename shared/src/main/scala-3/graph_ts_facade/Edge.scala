package graph_ts_facade

import typings.scalaTSFacade._
import scala.scalajs.js

class Edge(_id: String, _title: String, _source: srcMainTsNodeMod.Node, _target: srcMainTsNodeMod.Node) extends srcMainTsEdgeMod.Edge:
  var id: String = _id
  var title: String = _title
  var source: srcMainTsNodeMod.Node = _source
  var target: srcMainTsNodeMod.Node = _target
