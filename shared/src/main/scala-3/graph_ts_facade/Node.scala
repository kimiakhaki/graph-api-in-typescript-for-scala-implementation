package graph_ts_facade

import typings.scalaTSFacade.srcMainTsNodeMod

//method 1
class Node(_id: String, _title: String) extends srcMainTsNodeMod.Node:
  var id: String = _id
  var title: String = _title

//method 2
object Node extends srcMainTsNodeMod.Node.mkInterface:
  def mkFunction(_id: String, _title: String): srcMainTsNodeMod.Node = new Node(_id, _title)

