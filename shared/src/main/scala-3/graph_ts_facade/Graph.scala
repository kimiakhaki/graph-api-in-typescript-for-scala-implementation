package graph_ts_facade

import typings.scalaTSFacade.srcMainTsGraphMod

import scala.scalajs.js

class Graph[NT, ET](_id: String, _title: String, _nodes: js.Array[NT], _edges: js.Array[ET]) extends srcMainTsGraphMod.Graph[NT, ET]:
  var id: String = _id
  var title: String = _title
  var nodes: js.Array[NT] = _nodes
  var edges: js.Array[ET] = _edges

  // Adds a node to the graph
  def addNode(node: NT): Unit = {
    nodes += node
  }

  // Removes a node from the graph based on its id
  def removeNode(nodeId: String): Unit = {
    nodes = nodes.filterNot(_ == nodeId)
  }

  // Adds an edge between source and target nodes
  def addEdge(edge: ET): Unit = {
    edges += edge
  }

  // Removes an edge between source and target nodes
  def removeEdge(source: NT, target: NT): Unit = {
    edges = edges.filterNot(_ == (source, target))
  }

  // Returns the number of nodes in the graph
  def size(): Double = nodes.size