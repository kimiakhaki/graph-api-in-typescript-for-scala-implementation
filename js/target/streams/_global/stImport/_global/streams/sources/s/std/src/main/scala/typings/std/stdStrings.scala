package typings.std

import typings.std.WebAssembly.ImportExportKind
import typings.std.WebAssembly.ValueType
import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

object stdStrings {
  
  @js.native
  sealed trait default
    extends StObject
       with ColorSpaceConversion
       with NotificationPermission
       with PremultiplyAlpha
       with RequestCache
       with ResponseType
       with WebGLPowerPreference
  inline def default: default = "default".asInstanceOf[default]
  
  @js.native
  sealed trait `2-digit` extends StObject
  inline def `2-digit`: `2-digit` = "2-digit".asInstanceOf[`2-digit`]
  
  @js.native
  sealed trait `2d`
    extends StObject
       with OffscreenRenderingContextId
  inline def `2d`: `2d` = "2d".asInstanceOf[`2d`]
  
  @js.native
  sealed trait `2x`
    extends StObject
       with OverSampleType
  inline def `2x`: `2x` = "2x".asInstanceOf[`2x`]
  
  @js.native
  sealed trait `4x`
    extends StObject
       with OverSampleType
  inline def `4x`: `4x` = "4x".asInstanceOf[`4x`]
  
  @js.native
  sealed trait ANGLE_instanced_arrays extends StObject
  inline def ANGLE_instanced_arrays: ANGLE_instanced_arrays = "ANGLE_instanced_arrays".asInstanceOf[ANGLE_instanced_arrays]
  
  @js.native
  sealed trait AnimationEvent extends StObject
  inline def AnimationEvent: AnimationEvent = "AnimationEvent".asInstanceOf[AnimationEvent]
  
  @js.native
  sealed trait AnimationPlaybackEvent extends StObject
  inline def AnimationPlaybackEvent: AnimationPlaybackEvent = "AnimationPlaybackEvent".asInstanceOf[AnimationPlaybackEvent]
  
  @js.native
  sealed trait AudioProcessingEvent extends StObject
  inline def AudioProcessingEvent: AudioProcessingEvent = "AudioProcessingEvent".asInstanceOf[AudioProcessingEvent]
  
  @js.native
  sealed trait BT
    extends StObject
       with MSTransportType
  inline def BT: BT = "BT".asInstanceOf[BT]
  
  @js.native
  sealed trait BeforeUnloadEvent extends StObject
  inline def BeforeUnloadEvent: BeforeUnloadEvent = "BeforeUnloadEvent".asInstanceOf[BeforeUnloadEvent]
  
  @js.native
  sealed trait ClipboardEvent extends StObject
  inline def ClipboardEvent: ClipboardEvent = "ClipboardEvent".asInstanceOf[ClipboardEvent]
  
  @js.native
  sealed trait CloseEvent extends StObject
  inline def CloseEvent: CloseEvent = "CloseEvent".asInstanceOf[CloseEvent]
  
  @js.native
  sealed trait CompositionEvent extends StObject
  inline def CompositionEvent: CompositionEvent = "CompositionEvent".asInstanceOf[CompositionEvent]
  
  @js.native
  sealed trait CustomEvent extends StObject
  inline def CustomEvent: CustomEvent = "CustomEvent".asInstanceOf[CustomEvent]
  
  @js.native
  sealed trait DeviceMotionEvent extends StObject
  inline def DeviceMotionEvent: DeviceMotionEvent = "DeviceMotionEvent".asInstanceOf[DeviceMotionEvent]
  
  @js.native
  sealed trait DeviceOrientationEvent extends StObject
  inline def DeviceOrientationEvent: DeviceOrientationEvent = "DeviceOrientationEvent".asInstanceOf[DeviceOrientationEvent]
  
  @js.native
  sealed trait DragEvent extends StObject
  inline def DragEvent: DragEvent = "DragEvent".asInstanceOf[DragEvent]
  
  @js.native
  sealed trait EXT_blend_minmax extends StObject
  inline def EXT_blend_minmax: EXT_blend_minmax = "EXT_blend_minmax".asInstanceOf[EXT_blend_minmax]
  
  @js.native
  sealed trait EXT_frag_depth extends StObject
  inline def EXT_frag_depth: EXT_frag_depth = "EXT_frag_depth".asInstanceOf[EXT_frag_depth]
  
  @js.native
  sealed trait EXT_sRGB extends StObject
  inline def EXT_sRGB: EXT_sRGB = "EXT_sRGB".asInstanceOf[EXT_sRGB]
  
  @js.native
  sealed trait EXT_shader_texture_lod extends StObject
  inline def EXT_shader_texture_lod: EXT_shader_texture_lod = "EXT_shader_texture_lod".asInstanceOf[EXT_shader_texture_lod]
  
  @js.native
  sealed trait EXT_texture_filter_anisotropic extends StObject
  inline def EXT_texture_filter_anisotropic: EXT_texture_filter_anisotropic = "EXT_texture_filter_anisotropic".asInstanceOf[EXT_texture_filter_anisotropic]
  
  @js.native
  sealed trait Embedded
    extends StObject
       with MSTransportType
  inline def Embedded: Embedded = "Embedded".asInstanceOf[Embedded]
  
  @js.native
  sealed trait ErrorEvent extends StObject
  inline def ErrorEvent: ErrorEvent = "ErrorEvent".asInstanceOf[ErrorEvent]
  
  @js.native
  sealed trait Event extends StObject
  inline def Event: Event = "Event".asInstanceOf[Event]
  
  @js.native
  sealed trait Events extends StObject
  inline def Events: Events = "Events".asInstanceOf[Events]
  
  @js.native
  sealed trait FIDO_2_0 extends StObject
  inline def FIDO_2_0: FIDO_2_0 = "FIDO_2_0".asInstanceOf[FIDO_2_0]
  
  @js.native
  sealed trait Float32Array extends StObject
  inline def Float32Array: Float32Array = "Float32Array".asInstanceOf[Float32Array]
  
  @js.native
  sealed trait Float64Array extends StObject
  inline def Float64Array: Float64Array = "Float64Array".asInstanceOf[Float64Array]
  
  @js.native
  sealed trait FocusEvent extends StObject
  inline def FocusEvent: FocusEvent = "FocusEvent".asInstanceOf[FocusEvent]
  
  @js.native
  sealed trait FocusNavigationEvent extends StObject
  inline def FocusNavigationEvent: FocusNavigationEvent = "FocusNavigationEvent".asInstanceOf[FocusNavigationEvent]
  
  @js.native
  sealed trait GamepadEvent extends StObject
  inline def GamepadEvent: GamepadEvent = "GamepadEvent".asInstanceOf[GamepadEvent]
  
  @js.native
  sealed trait HRTF
    extends StObject
       with PanningModelType
  inline def HRTF: HRTF = "HRTF".asInstanceOf[HRTF]
  
  @js.native
  sealed trait HashChangeEvent extends StObject
  inline def HashChangeEvent: HashChangeEvent = "HashChangeEvent".asInstanceOf[HashChangeEvent]
  
  @js.native
  sealed trait IDBVersionChangeEvent extends StObject
  inline def IDBVersionChangeEvent: IDBVersionChangeEvent = "IDBVersionChangeEvent".asInstanceOf[IDBVersionChangeEvent]
  
  @js.native
  sealed trait InputEvent extends StObject
  inline def InputEvent: InputEvent = "InputEvent".asInstanceOf[InputEvent]
  
  @js.native
  sealed trait Int16Array extends StObject
  inline def Int16Array: Int16Array = "Int16Array".asInstanceOf[Int16Array]
  
  @js.native
  sealed trait Int32Array extends StObject
  inline def Int32Array: Int32Array = "Int32Array".asInstanceOf[Int32Array]
  
  @js.native
  sealed trait Int8Array extends StObject
  inline def Int8Array: Int8Array = "Int8Array".asInstanceOf[Int8Array]
  
  @js.native
  sealed trait KeyboardEvent extends StObject
  inline def KeyboardEvent: KeyboardEvent = "KeyboardEvent".asInstanceOf[KeyboardEvent]
  
  @js.native
  sealed trait ListeningStateChangedEvent extends StObject
  inline def ListeningStateChangedEvent: ListeningStateChangedEvent = "ListeningStateChangedEvent".asInstanceOf[ListeningStateChangedEvent]
  
  @js.native
  sealed trait MSCandidateWindowHide extends StObject
  inline def MSCandidateWindowHide: MSCandidateWindowHide = "MSCandidateWindowHide".asInstanceOf[MSCandidateWindowHide]
  
  @js.native
  sealed trait MSCandidateWindowShow extends StObject
  inline def MSCandidateWindowShow: MSCandidateWindowShow = "MSCandidateWindowShow".asInstanceOf[MSCandidateWindowShow]
  
  @js.native
  sealed trait MSCandidateWindowUpdate extends StObject
  inline def MSCandidateWindowUpdate: MSCandidateWindowUpdate = "MSCandidateWindowUpdate".asInstanceOf[MSCandidateWindowUpdate]
  
  @js.native
  sealed trait MediaEncryptedEvent extends StObject
  inline def MediaEncryptedEvent: MediaEncryptedEvent = "MediaEncryptedEvent".asInstanceOf[MediaEncryptedEvent]
  
  @js.native
  sealed trait MediaKeyMessageEvent extends StObject
  inline def MediaKeyMessageEvent: MediaKeyMessageEvent = "MediaKeyMessageEvent".asInstanceOf[MediaKeyMessageEvent]
  
  @js.native
  sealed trait MediaQueryListEvent extends StObject
  inline def MediaQueryListEvent: MediaQueryListEvent = "MediaQueryListEvent".asInstanceOf[MediaQueryListEvent]
  
  @js.native
  sealed trait MediaStreamErrorEvent extends StObject
  inline def MediaStreamErrorEvent: MediaStreamErrorEvent = "MediaStreamErrorEvent".asInstanceOf[MediaStreamErrorEvent]
  
  @js.native
  sealed trait MediaStreamEvent extends StObject
  inline def MediaStreamEvent: MediaStreamEvent = "MediaStreamEvent".asInstanceOf[MediaStreamEvent]
  
  @js.native
  sealed trait MediaStreamTrackEvent extends StObject
  inline def MediaStreamTrackEvent: MediaStreamTrackEvent = "MediaStreamTrackEvent".asInstanceOf[MediaStreamTrackEvent]
  
  @js.native
  sealed trait MessageEvent extends StObject
  inline def MessageEvent: MessageEvent = "MessageEvent".asInstanceOf[MessageEvent]
  
  @js.native
  sealed trait MouseEvent extends StObject
  inline def MouseEvent: MouseEvent = "MouseEvent".asInstanceOf[MouseEvent]
  
  @js.native
  sealed trait MouseEvents extends StObject
  inline def MouseEvents: MouseEvents = "MouseEvents".asInstanceOf[MouseEvents]
  
  @js.native
  sealed trait MutationEvent extends StObject
  inline def MutationEvent: MutationEvent = "MutationEvent".asInstanceOf[MutationEvent]
  
  @js.native
  sealed trait MutationEvents extends StObject
  inline def MutationEvents: MutationEvents = "MutationEvents".asInstanceOf[MutationEvents]
  
  @js.native
  sealed trait NFC
    extends StObject
       with MSTransportType
  inline def NFC: NFC = "NFC".asInstanceOf[NFC]
  
  @js.native
  sealed trait NFD extends StObject
  inline def NFD: NFD = "NFD".asInstanceOf[NFD]
  
  @js.native
  sealed trait NFKC extends StObject
  inline def NFKC: NFKC = "NFKC".asInstanceOf[NFKC]
  
  @js.native
  sealed trait NFKD extends StObject
  inline def NFKD: NFKD = "NFKD".asInstanceOf[NFKD]
  
  @js.native
  sealed trait OES_element_index_uint extends StObject
  inline def OES_element_index_uint: OES_element_index_uint = "OES_element_index_uint".asInstanceOf[OES_element_index_uint]
  
  @js.native
  sealed trait OES_standard_derivatives extends StObject
  inline def OES_standard_derivatives: OES_standard_derivatives = "OES_standard_derivatives".asInstanceOf[OES_standard_derivatives]
  
  @js.native
  sealed trait OES_texture_float extends StObject
  inline def OES_texture_float: OES_texture_float = "OES_texture_float".asInstanceOf[OES_texture_float]
  
  @js.native
  sealed trait OES_texture_float_linear extends StObject
  inline def OES_texture_float_linear: OES_texture_float_linear = "OES_texture_float_linear".asInstanceOf[OES_texture_float_linear]
  
  @js.native
  sealed trait OES_texture_half_float extends StObject
  inline def OES_texture_half_float: OES_texture_half_float = "OES_texture_half_float".asInstanceOf[OES_texture_half_float]
  
  @js.native
  sealed trait OES_texture_half_float_linear extends StObject
  inline def OES_texture_half_float_linear: OES_texture_half_float_linear = "OES_texture_half_float_linear".asInstanceOf[OES_texture_half_float_linear]
  
  @js.native
  sealed trait OES_vertex_array_object extends StObject
  inline def OES_vertex_array_object: OES_vertex_array_object = "OES_vertex_array_object".asInstanceOf[OES_vertex_array_object]
  
  @js.native
  sealed trait OfflineAudioCompletionEvent extends StObject
  inline def OfflineAudioCompletionEvent: OfflineAudioCompletionEvent = "OfflineAudioCompletionEvent".asInstanceOf[OfflineAudioCompletionEvent]
  
  @js.native
  sealed trait OverflowEvent extends StObject
  inline def OverflowEvent: OverflowEvent = "OverflowEvent".asInstanceOf[OverflowEvent]
  
  @js.native
  sealed trait PageTransitionEvent extends StObject
  inline def PageTransitionEvent: PageTransitionEvent = "PageTransitionEvent".asInstanceOf[PageTransitionEvent]
  
  @js.native
  sealed trait PaymentMethodChangeEvent extends StObject
  inline def PaymentMethodChangeEvent: PaymentMethodChangeEvent = "PaymentMethodChangeEvent".asInstanceOf[PaymentMethodChangeEvent]
  
  @js.native
  sealed trait PaymentRequestUpdateEvent extends StObject
  inline def PaymentRequestUpdateEvent: PaymentRequestUpdateEvent = "PaymentRequestUpdateEvent".asInstanceOf[PaymentRequestUpdateEvent]
  
  @js.native
  sealed trait PermissionRequestedEvent extends StObject
  inline def PermissionRequestedEvent: PermissionRequestedEvent = "PermissionRequestedEvent".asInstanceOf[PermissionRequestedEvent]
  
  @js.native
  sealed trait PointerEvent extends StObject
  inline def PointerEvent: PointerEvent = "PointerEvent".asInstanceOf[PointerEvent]
  
  @js.native
  sealed trait PopStateEvent extends StObject
  inline def PopStateEvent: PopStateEvent = "PopStateEvent".asInstanceOf[PopStateEvent]
  
  @js.native
  sealed trait ProgressEvent extends StObject
  inline def ProgressEvent: ProgressEvent = "ProgressEvent".asInstanceOf[ProgressEvent]
  
  @js.native
  sealed trait PromiseRejectionEvent extends StObject
  inline def PromiseRejectionEvent: PromiseRejectionEvent = "PromiseRejectionEvent".asInstanceOf[PromiseRejectionEvent]
  
  @js.native
  sealed trait RTCDTMFToneChangeEvent extends StObject
  inline def RTCDTMFToneChangeEvent: RTCDTMFToneChangeEvent = "RTCDTMFToneChangeEvent".asInstanceOf[RTCDTMFToneChangeEvent]
  
  @js.native
  sealed trait RTCDataChannelEvent extends StObject
  inline def RTCDataChannelEvent: RTCDataChannelEvent = "RTCDataChannelEvent".asInstanceOf[RTCDataChannelEvent]
  
  @js.native
  sealed trait RTCDtlsTransportStateChangedEvent extends StObject
  inline def RTCDtlsTransportStateChangedEvent: RTCDtlsTransportStateChangedEvent = "RTCDtlsTransportStateChangedEvent".asInstanceOf[RTCDtlsTransportStateChangedEvent]
  
  @js.native
  sealed trait RTCErrorEvent extends StObject
  inline def RTCErrorEvent: RTCErrorEvent = "RTCErrorEvent".asInstanceOf[RTCErrorEvent]
  
  @js.native
  sealed trait RTCIceCandidatePairChangedEvent extends StObject
  inline def RTCIceCandidatePairChangedEvent: RTCIceCandidatePairChangedEvent = "RTCIceCandidatePairChangedEvent".asInstanceOf[RTCIceCandidatePairChangedEvent]
  
  @js.native
  sealed trait RTCIceGathererEvent extends StObject
  inline def RTCIceGathererEvent: RTCIceGathererEvent = "RTCIceGathererEvent".asInstanceOf[RTCIceGathererEvent]
  
  @js.native
  sealed trait RTCIceTransportStateChangedEvent extends StObject
  inline def RTCIceTransportStateChangedEvent: RTCIceTransportStateChangedEvent = "RTCIceTransportStateChangedEvent".asInstanceOf[RTCIceTransportStateChangedEvent]
  
  @js.native
  sealed trait RTCPeerConnectionIceErrorEvent extends StObject
  inline def RTCPeerConnectionIceErrorEvent: RTCPeerConnectionIceErrorEvent = "RTCPeerConnectionIceErrorEvent".asInstanceOf[RTCPeerConnectionIceErrorEvent]
  
  @js.native
  sealed trait RTCPeerConnectionIceEvent extends StObject
  inline def RTCPeerConnectionIceEvent: RTCPeerConnectionIceEvent = "RTCPeerConnectionIceEvent".asInstanceOf[RTCPeerConnectionIceEvent]
  
  @js.native
  sealed trait RTCSsrcConflictEvent extends StObject
  inline def RTCSsrcConflictEvent: RTCSsrcConflictEvent = "RTCSsrcConflictEvent".asInstanceOf[RTCSsrcConflictEvent]
  
  @js.native
  sealed trait RTCTrackEvent extends StObject
  inline def RTCTrackEvent: RTCTrackEvent = "RTCTrackEvent".asInstanceOf[RTCTrackEvent]
  
  @js.native
  sealed trait SVGUnload extends StObject
  inline def SVGUnload: SVGUnload = "SVGUnload".asInstanceOf[SVGUnload]
  
  @js.native
  sealed trait SVGZoom extends StObject
  inline def SVGZoom: SVGZoom = "SVGZoom".asInstanceOf[SVGZoom]
  
  @js.native
  sealed trait SVGZoomEvent extends StObject
  inline def SVGZoomEvent: SVGZoomEvent = "SVGZoomEvent".asInstanceOf[SVGZoomEvent]
  
  @js.native
  sealed trait SVGZoomEvents extends StObject
  inline def SVGZoomEvents: SVGZoomEvents = "SVGZoomEvents".asInstanceOf[SVGZoomEvents]
  
  @js.native
  sealed trait SecurityPolicyViolationEvent extends StObject
  inline def SecurityPolicyViolationEvent: SecurityPolicyViolationEvent = "SecurityPolicyViolationEvent".asInstanceOf[SecurityPolicyViolationEvent]
  
  @js.native
  sealed trait SpeechRecognitionErrorEvent extends StObject
  inline def SpeechRecognitionErrorEvent: SpeechRecognitionErrorEvent = "SpeechRecognitionErrorEvent".asInstanceOf[SpeechRecognitionErrorEvent]
  
  @js.native
  sealed trait SpeechRecognitionEvent extends StObject
  inline def SpeechRecognitionEvent: SpeechRecognitionEvent = "SpeechRecognitionEvent".asInstanceOf[SpeechRecognitionEvent]
  
  @js.native
  sealed trait SpeechSynthesisErrorEvent extends StObject
  inline def SpeechSynthesisErrorEvent: SpeechSynthesisErrorEvent = "SpeechSynthesisErrorEvent".asInstanceOf[SpeechSynthesisErrorEvent]
  
  @js.native
  sealed trait SpeechSynthesisEvent extends StObject
  inline def SpeechSynthesisEvent: SpeechSynthesisEvent = "SpeechSynthesisEvent".asInstanceOf[SpeechSynthesisEvent]
  
  @js.native
  sealed trait StorageEvent extends StObject
  inline def StorageEvent: StorageEvent = "StorageEvent".asInstanceOf[StorageEvent]
  
  @js.native
  sealed trait TextEvent extends StObject
  inline def TextEvent: TextEvent = "TextEvent".asInstanceOf[TextEvent]
  
  @js.native
  sealed trait TouchEvent extends StObject
  inline def TouchEvent: TouchEvent = "TouchEvent".asInstanceOf[TouchEvent]
  
  @js.native
  sealed trait TrackEvent extends StObject
  inline def TrackEvent: TrackEvent = "TrackEvent".asInstanceOf[TrackEvent]
  
  @js.native
  sealed trait TransitionEvent extends StObject
  inline def TransitionEvent: TransitionEvent = "TransitionEvent".asInstanceOf[TransitionEvent]
  
  @js.native
  sealed trait UIEvent extends StObject
  inline def UIEvent: UIEvent = "UIEvent".asInstanceOf[UIEvent]
  
  @js.native
  sealed trait UIEvents extends StObject
  inline def UIEvents: UIEvents = "UIEvents".asInstanceOf[UIEvents]
  
  @js.native
  sealed trait USB
    extends StObject
       with MSTransportType
  inline def USB: USB = "USB".asInstanceOf[USB]
  
  @js.native
  sealed trait Uint16Array extends StObject
  inline def Uint16Array: Uint16Array = "Uint16Array".asInstanceOf[Uint16Array]
  
  @js.native
  sealed trait Uint32Array extends StObject
  inline def Uint32Array: Uint32Array = "Uint32Array".asInstanceOf[Uint32Array]
  
  @js.native
  sealed trait Uint8Array extends StObject
  inline def Uint8Array: Uint8Array = "Uint8Array".asInstanceOf[Uint8Array]
  
  @js.native
  sealed trait Uint8ClampedArray extends StObject
  inline def Uint8ClampedArray: Uint8ClampedArray = "Uint8ClampedArray".asInstanceOf[Uint8ClampedArray]
  
  @js.native
  sealed trait VRDisplayEvent extends StObject
  inline def VRDisplayEvent: VRDisplayEvent = "VRDisplayEvent".asInstanceOf[VRDisplayEvent]
  
  @js.native
  sealed trait `VRDisplayEvent ` extends StObject
  inline def `VRDisplayEvent `: `VRDisplayEvent ` = ("VRDisplayEvent ").asInstanceOf[`VRDisplayEvent `]
  
  @js.native
  sealed trait WEBGL_color_buffer_float extends StObject
  inline def WEBGL_color_buffer_float: WEBGL_color_buffer_float = "WEBGL_color_buffer_float".asInstanceOf[WEBGL_color_buffer_float]
  
  @js.native
  sealed trait WEBGL_compressed_texture_astc extends StObject
  inline def WEBGL_compressed_texture_astc: WEBGL_compressed_texture_astc = "WEBGL_compressed_texture_astc".asInstanceOf[WEBGL_compressed_texture_astc]
  
  @js.native
  sealed trait WEBGL_compressed_texture_s3tc extends StObject
  inline def WEBGL_compressed_texture_s3tc: WEBGL_compressed_texture_s3tc = "WEBGL_compressed_texture_s3tc".asInstanceOf[WEBGL_compressed_texture_s3tc]
  
  @js.native
  sealed trait WEBGL_compressed_texture_s3tc_srgb extends StObject
  inline def WEBGL_compressed_texture_s3tc_srgb: WEBGL_compressed_texture_s3tc_srgb = "WEBGL_compressed_texture_s3tc_srgb".asInstanceOf[WEBGL_compressed_texture_s3tc_srgb]
  
  @js.native
  sealed trait WEBGL_debug_renderer_info extends StObject
  inline def WEBGL_debug_renderer_info: WEBGL_debug_renderer_info = "WEBGL_debug_renderer_info".asInstanceOf[WEBGL_debug_renderer_info]
  
  @js.native
  sealed trait WEBGL_debug_shaders extends StObject
  inline def WEBGL_debug_shaders: WEBGL_debug_shaders = "WEBGL_debug_shaders".asInstanceOf[WEBGL_debug_shaders]
  
  @js.native
  sealed trait WEBGL_depth_texture extends StObject
  inline def WEBGL_depth_texture: WEBGL_depth_texture = "WEBGL_depth_texture".asInstanceOf[WEBGL_depth_texture]
  
  @js.native
  sealed trait WEBGL_draw_buffers extends StObject
  inline def WEBGL_draw_buffers: WEBGL_draw_buffers = "WEBGL_draw_buffers".asInstanceOf[WEBGL_draw_buffers]
  
  @js.native
  sealed trait WEBGL_lose_context extends StObject
  inline def WEBGL_lose_context: WEBGL_lose_context = "WEBGL_lose_context".asInstanceOf[WEBGL_lose_context]
  
  @js.native
  sealed trait WebGLContextEvent extends StObject
  inline def WebGLContextEvent: WebGLContextEvent = "WebGLContextEvent".asInstanceOf[WebGLContextEvent]
  
  @js.native
  sealed trait WheelEvent extends StObject
  inline def WheelEvent: WheelEvent = "WheelEvent".asInstanceOf[WheelEvent]
  
  @js.native
  sealed trait _empty
    extends StObject
       with CanPlayTypeResult
       with DirectionSetting
       with GamepadHand
       with GamepadMappingType
       with ReferrerPolicy
       with RequestDestination
       with ScrollSetting
       with XMLHttpRequestResponseType
  inline def _empty: _empty = "".asInstanceOf[_empty]
  
  @js.native
  sealed trait a extends StObject
  inline def a: a = "a".asInstanceOf[a]
  
  @js.native
  sealed trait `a-rate`
    extends StObject
       with AutomationRate
  inline def `a-rate`: `a-rate` = "a-rate".asInstanceOf[`a-rate`]
  
  @js.native
  sealed trait abbr extends StObject
  inline def abbr: abbr = "abbr".asInstanceOf[abbr]
  
  @js.native
  sealed trait abort extends StObject
  inline def abort: abort = "abort".asInstanceOf[abort]
  
  @js.native
  sealed trait aborted
    extends StObject
       with SpeechRecognitionErrorCode
  inline def aborted: aborted = "aborted".asInstanceOf[aborted]
  
  @js.native
  sealed trait accelerometer
    extends StObject
       with PermissionName
  inline def accelerometer: accelerometer = "accelerometer".asInstanceOf[accelerometer]
  
  @js.native
  sealed trait accumulate
    extends StObject
       with CompositeOperation
       with CompositeOperationOrAuto
       with IterationCompositeOperation
  inline def accumulate: accumulate = "accumulate".asInstanceOf[accumulate]
  
  @js.native
  sealed trait activated
    extends StObject
       with ServiceWorkerState
  inline def activated: activated = "activated".asInstanceOf[activated]
  
  @js.native
  sealed trait activating
    extends StObject
       with ServiceWorkerState
  inline def activating: activating = "activating".asInstanceOf[activating]
  
  @js.native
  sealed trait active
    extends StObject
       with ListeningState
       with RTCIceTcpCandidateType
  inline def active: active = "active".asInstanceOf[active]
  
  @js.native
  sealed trait add
    extends StObject
       with CompositeOperation
       with CompositeOperationOrAuto
  inline def add: add = "add".asInstanceOf[add]
  
  @js.native
  sealed trait address extends StObject
  inline def address: address = "address".asInstanceOf[address]
  
  @js.native
  sealed trait addsourcebuffer extends StObject
  inline def addsourcebuffer: addsourcebuffer = "addsourcebuffer".asInstanceOf[addsourcebuffer]
  
  @js.native
  sealed trait addtrack extends StObject
  inline def addtrack: addtrack = "addtrack".asInstanceOf[addtrack]
  
  @js.native
  sealed trait afterbegin
    extends StObject
       with InsertPosition
  inline def afterbegin: afterbegin = "afterbegin".asInstanceOf[afterbegin]
  
  @js.native
  sealed trait afterend
    extends StObject
       with InsertPosition
  inline def afterend: afterend = "afterend".asInstanceOf[afterend]
  
  @js.native
  sealed trait afterprint extends StObject
  inline def afterprint: afterprint = "afterprint".asInstanceOf[afterprint]
  
  @js.native
  sealed trait all
    extends StObject
       with ClientTypes
       with RTCIceGatherPolicy
       with RTCIceTransportPolicy
       with ServiceWorkerUpdateViaCache
  inline def all: all = "all".asInstanceOf[all]
  
  @js.native
  sealed trait allow
    extends StObject
       with MSWebViewPermissionState
  inline def allow: allow = "allow".asInstanceOf[allow]
  
  @js.native
  sealed trait allpass
    extends StObject
       with BiquadFilterType
  inline def allpass: allpass = "allpass".asInstanceOf[allpass]
  
  @js.native
  sealed trait alphabetic
    extends StObject
       with CanvasTextBaseline
  inline def alphabetic: alphabetic = "alphabetic".asInstanceOf[alphabetic]
  
  @js.native
  sealed trait alternate
    extends StObject
       with PlaybackDirection
  inline def alternate: alternate = "alternate".asInstanceOf[alternate]
  
  @js.native
  sealed trait `alternate-reverse`
    extends StObject
       with PlaybackDirection
  inline def `alternate-reverse`: `alternate-reverse` = "alternate-reverse".asInstanceOf[`alternate-reverse`]
  
  @js.native
  sealed trait `ambient-light-sensor`
    extends StObject
       with PermissionName
  inline def `ambient-light-sensor`: `ambient-light-sensor` = "ambient-light-sensor".asInstanceOf[`ambient-light-sensor`]
  
  @js.native
  sealed trait animationcancel extends StObject
  inline def animationcancel: animationcancel = "animationcancel".asInstanceOf[animationcancel]
  
  @js.native
  sealed trait animationend extends StObject
  inline def animationend: animationend = "animationend".asInstanceOf[animationend]
  
  @js.native
  sealed trait animationiteration extends StObject
  inline def animationiteration: animationiteration = "animationiteration".asInstanceOf[animationiteration]
  
  @js.native
  sealed trait animationstart extends StObject
  inline def animationstart: animationstart = "animationstart".asInstanceOf[animationstart]
  
  @js.native
  sealed trait answer
    extends StObject
       with RTCSdpType
  inline def answer: answer = "answer".asInstanceOf[answer]
  
  @js.native
  sealed trait any
    extends StObject
       with OrientationLockType
  inline def any: any = "any".asInstanceOf[any]
  
  @js.native
  sealed trait anyfunc extends StObject
  inline def anyfunc: anyfunc = "anyfunc".asInstanceOf[anyfunc]
  
  @js.native
  sealed trait applet extends StObject
  inline def applet: applet = "applet".asInstanceOf[applet]
  
  @js.native
  sealed trait application
    extends StObject
       with DisplayCaptureSurfaceType
  inline def application: application = "application".asInstanceOf[application]
  
  @js.native
  sealed trait applicationSlashxhtmlPlussignxml
    extends StObject
       with DOMParserSupportedType
  inline def applicationSlashxhtmlPlussignxml: applicationSlashxhtmlPlussignxml = "application/xhtml+xml".asInstanceOf[applicationSlashxhtmlPlussignxml]
  
  @js.native
  sealed trait applicationSlashxml
    extends StObject
       with DOMParserSupportedType
  inline def applicationSlashxml: applicationSlashxml = "application/xml".asInstanceOf[applicationSlashxml]
  
  @js.native
  sealed trait area extends StObject
  inline def area: area = "area".asInstanceOf[area]
  
  @js.native
  sealed trait arraybuffer
    extends StObject
       with BinaryType
       with XMLHttpRequestResponseType
  inline def arraybuffer: arraybuffer = "arraybuffer".asInstanceOf[arraybuffer]
  
  @js.native
  sealed trait article extends StObject
  inline def article: article = "article".asInstanceOf[article]
  
  @js.native
  sealed trait aside extends StObject
  inline def aside: aside = "aside".asInstanceOf[aside]
  
  @js.native
  sealed trait async extends StObject
  inline def async: async = "async".asInstanceOf[async]
  
  @js.native
  sealed trait attributes
    extends StObject
       with MutationRecordType
  inline def attributes: attributes = "attributes".asInstanceOf[attributes]
  
  @js.native
  sealed trait audio
    extends StObject
       with RequestDestination
  inline def audio: audio = "audio".asInstanceOf[audio]
  
  @js.native
  sealed trait `audio-busy`
    extends StObject
       with SpeechSynthesisErrorCode
  inline def `audio-busy`: `audio-busy` = "audio-busy".asInstanceOf[`audio-busy`]
  
  @js.native
  sealed trait `audio-capture`
    extends StObject
       with SpeechRecognitionErrorCode
  inline def `audio-capture`: `audio-capture` = "audio-capture".asInstanceOf[`audio-capture`]
  
  @js.native
  sealed trait `audio-hardware`
    extends StObject
       with SpeechSynthesisErrorCode
  inline def `audio-hardware`: `audio-hardware` = "audio-hardware".asInstanceOf[`audio-hardware`]
  
  @js.native
  sealed trait audioend extends StObject
  inline def audioend: audioend = "audioend".asInstanceOf[audioend]
  
  @js.native
  sealed trait audioinput
    extends StObject
       with MediaDeviceKind
  inline def audioinput: audioinput = "audioinput".asInstanceOf[audioinput]
  
  @js.native
  sealed trait audiooutput
    extends StObject
       with MediaDeviceKind
  inline def audiooutput: audiooutput = "audiooutput".asInstanceOf[audiooutput]
  
  @js.native
  sealed trait audioprocess extends StObject
  inline def audioprocess: audioprocess = "audioprocess".asInstanceOf[audioprocess]
  
  @js.native
  sealed trait audiostart extends StObject
  inline def audiostart: audiostart = "audiostart".asInstanceOf[audiostart]
  
  @js.native
  sealed trait audioworklet
    extends StObject
       with RequestDestination
  inline def audioworklet: audioworklet = "audioworklet".asInstanceOf[audioworklet]
  
  @js.native
  sealed trait auth
    extends StObject
       with PushEncryptionKeyName
  inline def auth: auth = "auth".asInstanceOf[auth]
  
  @js.native
  sealed trait auto
    extends StObject
       with CompositeOperationOrAuto
       with FillMode
       with FullscreenNavigationUI
       with NotificationDirection
       with PositionAlignSetting
       with RTCDtlsRole
       with ScrollBehavior
       with ScrollRestoration
  inline def auto: auto = "auto".asInstanceOf[auto]
  
  @js.native
  sealed trait auxclick extends StObject
  inline def auxclick: auxclick = "auxclick".asInstanceOf[auxclick]
  
  @js.native
  sealed trait b extends StObject
  inline def b: b = "b".asInstanceOf[b]
  
  @js.native
  sealed trait back_forward
    extends StObject
       with NavigationType
  inline def back_forward: back_forward = "back_forward".asInstanceOf[back_forward]
  
  @js.native
  sealed trait `background-fetch`
    extends StObject
       with PermissionName
  inline def `background-fetch`: `background-fetch` = "background-fetch".asInstanceOf[`background-fetch`]
  
  @js.native
  sealed trait `background-sync`
    extends StObject
       with PermissionName
  inline def `background-sync`: `background-sync` = "background-sync".asInstanceOf[`background-sync`]
  
  @js.native
  sealed trait backward extends StObject
  inline def backward: backward = "backward".asInstanceOf[backward]
  
  @js.native
  sealed trait backwards
    extends StObject
       with FillMode
  inline def backwards: backwards = "backwards".asInstanceOf[backwards]
  
  @js.native
  sealed trait `bad-grammar`
    extends StObject
       with SpeechRecognitionErrorCode
  inline def `bad-grammar`: `bad-grammar` = "bad-grammar".asInstanceOf[`bad-grammar`]
  
  @js.native
  sealed trait balanced
    extends StObject
       with AudioContextLatencyCategory
       with RTCBundlePolicy
  inline def balanced: balanced = "balanced".asInstanceOf[balanced]
  
  @js.native
  sealed trait bandpass
    extends StObject
       with BiquadFilterType
  inline def bandpass: bandpass = "bandpass".asInstanceOf[bandpass]
  
  @js.native
  sealed trait base extends StObject
  inline def base: base = "base".asInstanceOf[base]
  
  @js.native
  sealed trait basic
    extends StObject
       with ResponseType
  inline def basic: basic = "basic".asInstanceOf[basic]
  
  @js.native
  sealed trait bdi extends StObject
  inline def bdi: bdi = "bdi".asInstanceOf[bdi]
  
  @js.native
  sealed trait bdo extends StObject
  inline def bdo: bdo = "bdo".asInstanceOf[bdo]
  
  @js.native
  sealed trait beforebegin
    extends StObject
       with InsertPosition
  inline def beforebegin: beforebegin = "beforebegin".asInstanceOf[beforebegin]
  
  @js.native
  sealed trait beforeend
    extends StObject
       with InsertPosition
  inline def beforeend: beforeend = "beforeend".asInstanceOf[beforeend]
  
  @js.native
  sealed trait beforeinput extends StObject
  inline def beforeinput: beforeinput = "beforeinput".asInstanceOf[beforeinput]
  
  @js.native
  sealed trait beforeprint extends StObject
  inline def beforeprint: beforeprint = "beforeprint".asInstanceOf[beforeprint]
  
  @js.native
  sealed trait beforeunload extends StObject
  inline def beforeunload: beforeunload = "beforeunload".asInstanceOf[beforeunload]
  
  @js.native
  sealed trait `best fit` extends StObject
  inline def `best fit`: `best fit` = ("best fit").asInstanceOf[`best fit`]
  
  @js.native
  sealed trait bevel
    extends StObject
       with CanvasLineJoin
  inline def bevel: bevel = "bevel".asInstanceOf[bevel]
  
  @js.native
  sealed trait bitmaprenderer
    extends StObject
       with OffscreenRenderingContextId
  inline def bitmaprenderer: bitmaprenderer = "bitmaprenderer".asInstanceOf[bitmaprenderer]
  
  @js.native
  sealed trait ble
    extends StObject
       with AuthenticatorTransport
  inline def ble: ble = "ble".asInstanceOf[ble]
  
  @js.native
  sealed trait blob
    extends StObject
       with BinaryType
       with XMLHttpRequestResponseType
  inline def blob: blob = "blob".asInstanceOf[blob]
  
  @js.native
  sealed trait blocked extends StObject
  inline def blocked: blocked = "blocked".asInstanceOf[blocked]
  
  @js.native
  sealed trait blockquote extends StObject
  inline def blockquote: blockquote = "blockquote".asInstanceOf[blockquote]
  
  @js.native
  sealed trait bluetooth
    extends StObject
       with PermissionName
  inline def bluetooth: bluetooth = "bluetooth".asInstanceOf[bluetooth]
  
  @js.native
  sealed trait blur extends StObject
  inline def blur: blur = "blur".asInstanceOf[blur]
  
  @js.native
  sealed trait body extends StObject
  inline def body: body = "body".asInstanceOf[body]
  
  @js.native
  sealed trait `border-box`
    extends StObject
       with ResizeObserverBoxOptions
  inline def `border-box`: `border-box` = "border-box".asInstanceOf[`border-box`]
  
  @js.native
  sealed trait both
    extends StObject
       with FillMode
  inline def both: both = "both".asInstanceOf[both]
  
  @js.native
  sealed trait bottom
    extends StObject
       with CanvasTextBaseline
  inline def bottom: bottom = "bottom".asInstanceOf[bottom]
  
  @js.native
  sealed trait bounce extends StObject
  inline def bounce: bounce = "bounce".asInstanceOf[bounce]
  
  @js.native
  sealed trait boundary extends StObject
  inline def boundary: boundary = "boundary".asInstanceOf[boundary]
  
  @js.native
  sealed trait br extends StObject
  inline def br: br = "br".asInstanceOf[br]
  
  @js.native
  sealed trait browser
    extends StObject
       with DisplayCaptureSurfaceType
  inline def browser: browser = "browser".asInstanceOf[browser]
  
  @js.native
  sealed trait bufferedamountlow extends StObject
  inline def bufferedamountlow: bufferedamountlow = "bufferedamountlow".asInstanceOf[bufferedamountlow]
  
  @js.native
  sealed trait butt
    extends StObject
       with CanvasLineCap
  inline def butt: butt = "butt".asInstanceOf[butt]
  
  @js.native
  sealed trait button extends StObject
  inline def button: button = "button".asInstanceOf[button]
  
  @js.native
  sealed trait cached extends StObject
  inline def cached: cached = "cached".asInstanceOf[cached]
  
  @js.native
  sealed trait camera
    extends StObject
       with PermissionName
  inline def camera: camera = "camera".asInstanceOf[camera]
  
  @js.native
  sealed trait cancel extends StObject
  inline def cancel: cancel = "cancel".asInstanceOf[cancel]
  
  @js.native
  sealed trait canceled
    extends StObject
       with SpeechSynthesisErrorCode
  inline def canceled: canceled = "canceled".asInstanceOf[canceled]
  
  @js.native
  sealed trait `candidate-pair`
    extends StObject
       with RTCStatsType
  inline def `candidate-pair`: `candidate-pair` = "candidate-pair".asInstanceOf[`candidate-pair`]
  
  @js.native
  sealed trait canplay extends StObject
  inline def canplay: canplay = "canplay".asInstanceOf[canplay]
  
  @js.native
  sealed trait canplaythrough extends StObject
  inline def canplaythrough: canplaythrough = "canplaythrough".asInstanceOf[canplaythrough]
  
  @js.native
  sealed trait canvas extends StObject
  inline def canvas: canvas = "canvas".asInstanceOf[canvas]
  
  @js.native
  sealed trait caption extends StObject
  inline def caption: caption = "caption".asInstanceOf[caption]
  
  @js.native
  sealed trait captions
    extends StObject
       with TextTrackKind
  inline def captions: captions = "captions".asInstanceOf[captions]
  
  @js.native
  sealed trait center
    extends StObject
       with AlignSetting
       with CanvasTextAlign
       with LineAlignSetting
       with PositionAlignSetting
       with ScrollLogicalPosition
  inline def center: center = "center".asInstanceOf[center]
  
  @js.native
  sealed trait certificate
    extends StObject
       with RTCStatsType
  inline def certificate: certificate = "certificate".asInstanceOf[certificate]
  
  @js.native
  sealed trait change extends StObject
  inline def change: change = "change".asInstanceOf[change]
  
  @js.native
  sealed trait chapters
    extends StObject
       with TextTrackKind
  inline def chapters: chapters = "chapters".asInstanceOf[chapters]
  
  @js.native
  sealed trait characterData
    extends StObject
       with MutationRecordType
  inline def characterData: characterData = "characterData".asInstanceOf[characterData]
  
  @js.native
  sealed trait checking
    extends StObject
       with RTCIceConnectionState
       with RTCIceTransportState
  inline def checking: checking = "checking".asInstanceOf[checking]
  
  @js.native
  sealed trait childList
    extends StObject
       with MutationRecordType
  inline def childList: childList = "childList".asInstanceOf[childList]
  
  @js.native
  sealed trait circle extends StObject
  inline def circle: circle = "circle".asInstanceOf[circle]
  
  @js.native
  sealed trait cite extends StObject
  inline def cite: cite = "cite".asInstanceOf[cite]
  
  @js.native
  sealed trait `clamped-max`
    extends StObject
       with ChannelCountMode
  inline def `clamped-max`: `clamped-max` = "clamped-max".asInstanceOf[`clamped-max`]
  
  @js.native
  sealed trait classic
    extends StObject
       with WorkerType
  inline def classic: classic = "classic".asInstanceOf[classic]
  
  @js.native
  sealed trait click extends StObject
  inline def click: click = "click".asInstanceOf[click]
  
  @js.native
  sealed trait client
    extends StObject
       with RTCDtlsRole
  inline def client: client = "client".asInstanceOf[client]
  
  @js.native
  sealed trait clipPath extends StObject
  inline def clipPath: clipPath = "clipPath".asInstanceOf[clipPath]
  
  @js.native
  sealed trait `clipboard-read`
    extends StObject
       with PermissionName
  inline def `clipboard-read`: `clipboard-read` = "clipboard-read".asInstanceOf[`clipboard-read`]
  
  @js.native
  sealed trait `clipboard-write`
    extends StObject
       with PermissionName
  inline def `clipboard-write`: `clipboard-write` = "clipboard-write".asInstanceOf[`clipboard-write`]
  
  @js.native
  sealed trait close extends StObject
  inline def close: close = "close".asInstanceOf[close]
  
  @js.native
  sealed trait closed
    extends StObject
       with AudioContextState
       with RTCDataChannelState
       with RTCDtlsTransportState
       with RTCIceConnectionState
       with RTCIceTransportState
       with RTCPeerConnectionState
       with RTCSctpTransportState
       with RTCSignalingState
       with ReadyState
       with ShadowRootMode
  inline def closed: closed = "closed".asInstanceOf[closed]
  
  @js.native
  sealed trait closing
    extends StObject
       with RTCDataChannelState
  inline def closing: closing = "closing".asInstanceOf[closing]
  
  @js.native
  sealed trait code extends StObject
  inline def code: code = "code".asInstanceOf[code]
  
  @js.native
  sealed trait codec
    extends StObject
       with RTCStatsType
  inline def codec: codec = "codec".asInstanceOf[codec]
  
  @js.native
  sealed trait col extends StObject
  inline def col: col = "col".asInstanceOf[col]
  
  @js.native
  sealed trait colgroup extends StObject
  inline def colgroup: colgroup = "colgroup".asInstanceOf[colgroup]
  
  @js.native
  sealed trait compassneedscalibration extends StObject
  inline def compassneedscalibration: compassneedscalibration = "compassneedscalibration".asInstanceOf[compassneedscalibration]
  
  @js.native
  sealed trait complete
    extends StObject
       with DocumentReadyState
       with RTCIceGathererState
       with RTCIceGatheringState
  inline def complete: complete = "complete".asInstanceOf[complete]
  
  @js.native
  sealed trait completed
    extends StObject
       with RTCIceConnectionState
       with RTCIceTransportState
  inline def completed: completed = "completed".asInstanceOf[completed]
  
  @js.native
  sealed trait compositionend extends StObject
  inline def compositionend: compositionend = "compositionend".asInstanceOf[compositionend]
  
  @js.native
  sealed trait compositionstart extends StObject
  inline def compositionstart: compositionstart = "compositionstart".asInstanceOf[compositionstart]
  
  @js.native
  sealed trait compositionupdate extends StObject
  inline def compositionupdate: compositionupdate = "compositionupdate".asInstanceOf[compositionupdate]
  
  @js.native
  sealed trait connected
    extends StObject
       with RTCDtlsTransportState
       with RTCIceConnectionState
       with RTCIceTransportState
       with RTCPeerConnectionState
       with RTCSctpTransportState
  inline def connected: connected = "connected".asInstanceOf[connected]
  
  @js.native
  sealed trait connecting
    extends StObject
       with RTCDataChannelState
       with RTCDtlsTransportState
       with RTCPeerConnectionState
       with RTCSctpTransportState
  inline def connecting: connecting = "connecting".asInstanceOf[connecting]
  
  @js.native
  sealed trait connectionstatechange extends StObject
  inline def connectionstatechange: connectionstatechange = "connectionstatechange".asInstanceOf[connectionstatechange]
  
  @js.native
  sealed trait `content-box`
    extends StObject
       with ResizeObserverBoxOptions
  inline def `content-box`: `content-box` = "content-box".asInstanceOf[`content-box`]
  
  @js.native
  sealed trait contextmenu extends StObject
  inline def contextmenu: contextmenu = "contextmenu".asInstanceOf[contextmenu]
  
  @js.native
  sealed trait controlled
    extends StObject
       with RTCIceRole
  inline def controlled: controlled = "controlled".asInstanceOf[controlled]
  
  @js.native
  sealed trait controllerchange extends StObject
  inline def controllerchange: controllerchange = "controllerchange".asInstanceOf[controllerchange]
  
  @js.native
  sealed trait controlling
    extends StObject
       with RTCIceRole
  inline def controlling: controlling = "controlling".asInstanceOf[controlling]
  
  @js.native
  sealed trait copy extends StObject
  inline def copy: copy = "copy".asInstanceOf[copy]
  
  @js.native
  sealed trait copyLink extends StObject
  inline def copyLink: copyLink = "copyLink".asInstanceOf[copyLink]
  
  @js.native
  sealed trait copyMove extends StObject
  inline def copyMove: copyMove = "copyMove".asInstanceOf[copyMove]
  
  @js.native
  sealed trait cors
    extends StObject
       with RequestMode
       with ResponseType
  inline def cors: cors = "cors".asInstanceOf[cors]
  
  @js.native
  sealed trait `cross-platform`
    extends StObject
       with AuthenticatorAttachment
  inline def `cross-platform`: `cross-platform` = "cross-platform".asInstanceOf[`cross-platform`]
  
  @js.native
  sealed trait csrc
    extends StObject
       with RTCStatsType
  inline def csrc: csrc = "csrc".asInstanceOf[csrc]
  
  @js.native
  sealed trait cuechange extends StObject
  inline def cuechange: cuechange = "cuechange".asInstanceOf[cuechange]
  
  @js.native
  sealed trait custom
    extends StObject
       with OscillatorType
  inline def custom: custom = "custom".asInstanceOf[custom]
  
  @js.native
  sealed trait cut extends StObject
  inline def cut: cut = "cut".asInstanceOf[cut]
  
  @js.native
  sealed trait data extends StObject
  inline def data: data = "data".asInstanceOf[data]
  
  @js.native
  sealed trait `data-channel`
    extends StObject
       with RTCStatsType
  inline def `data-channel`: `data-channel` = "data-channel".asInstanceOf[`data-channel`]
  
  @js.native
  sealed trait `data-channel-failure`
    extends StObject
       with RTCErrorDetailType
  inline def `data-channel-failure`: `data-channel-failure` = "data-channel-failure".asInstanceOf[`data-channel-failure`]
  
  @js.native
  sealed trait datachannel extends StObject
  inline def datachannel: datachannel = "datachannel".asInstanceOf[datachannel]
  
  @js.native
  sealed trait datalist extends StObject
  inline def datalist: datalist = "datalist".asInstanceOf[datalist]
  
  @js.native
  sealed trait dblclick extends StObject
  inline def dblclick: dblclick = "dblclick".asInstanceOf[dblclick]
  
  @js.native
  sealed trait dd extends StObject
  inline def dd: dd = "dd".asInstanceOf[dd]
  
  @js.native
  sealed trait decode
    extends StObject
       with EndOfStreamError
  inline def decode: decode = "decode".asInstanceOf[decode]
  
  @js.native
  sealed trait decrypt
    extends StObject
       with KeyUsage
  inline def decrypt: decrypt = "decrypt".asInstanceOf[decrypt]
  
  @js.native
  sealed trait defer
    extends StObject
       with MSWebViewPermissionState
  inline def defer: defer = "defer".asInstanceOf[defer]
  
  @js.native
  sealed trait defs extends StObject
  inline def defs: defs = "defs".asInstanceOf[defs]
  
  @js.native
  sealed trait del extends StObject
  inline def del: del = "del".asInstanceOf[del]
  
  @js.native
  sealed trait delivery
    extends StObject
       with PaymentShippingType
  inline def delivery: delivery = "delivery".asInstanceOf[delivery]
  
  @js.native
  sealed trait denied
    extends StObject
       with NotificationPermission
       with PermissionState
       with PushPermissionState
  inline def denied: denied = "denied".asInstanceOf[denied]
  
  @js.native
  sealed trait deny
    extends StObject
       with MSWebViewPermissionState
  inline def deny: deny = "deny".asInstanceOf[deny]
  
  @js.native
  sealed trait deriveBits
    extends StObject
       with KeyUsage
  inline def deriveBits: deriveBits = "deriveBits".asInstanceOf[deriveBits]
  
  @js.native
  sealed trait deriveKey
    extends StObject
       with KeyUsage
  inline def deriveKey: deriveKey = "deriveKey".asInstanceOf[deriveKey]
  
  @js.native
  sealed trait desc extends StObject
  inline def desc: desc = "desc".asInstanceOf[desc]
  
  @js.native
  sealed trait descriptions
    extends StObject
       with TextTrackKind
  inline def descriptions: descriptions = "descriptions".asInstanceOf[descriptions]
  
  @js.native
  sealed trait details extends StObject
  inline def details: details = "details".asInstanceOf[details]
  
  @js.native
  sealed trait `device-info`
    extends StObject
       with PermissionName
  inline def `device-info`: `device-info` = "device-info".asInstanceOf[`device-info`]
  
  @js.native
  sealed trait `device-pixel-content-box`
    extends StObject
       with ResizeObserverBoxOptions
  inline def `device-pixel-content-box`: `device-pixel-content-box` = "device-pixel-content-box".asInstanceOf[`device-pixel-content-box`]
  
  @js.native
  sealed trait devicechange extends StObject
  inline def devicechange: devicechange = "devicechange".asInstanceOf[devicechange]
  
  @js.native
  sealed trait devicemotion extends StObject
  inline def devicemotion: devicemotion = "devicemotion".asInstanceOf[devicemotion]
  
  @js.native
  sealed trait deviceorientation extends StObject
  inline def deviceorientation: deviceorientation = "deviceorientation".asInstanceOf[deviceorientation]
  
  @js.native
  sealed trait deviceorientationabsolute extends StObject
  inline def deviceorientationabsolute: deviceorientationabsolute = "deviceorientationabsolute".asInstanceOf[deviceorientationabsolute]
  
  @js.native
  sealed trait dfn extends StObject
  inline def dfn: dfn = "dfn".asInstanceOf[dfn]
  
  @js.native
  sealed trait dialog extends StObject
  inline def dialog: dialog = "dialog".asInstanceOf[dialog]
  
  @js.native
  sealed trait dir extends StObject
  inline def dir: dir = "dir".asInstanceOf[dir]
  
  @js.native
  sealed trait direct
    extends StObject
       with AttestationConveyancePreference
       with TouchType
  inline def direct: direct = "direct".asInstanceOf[direct]
  
  @js.native
  sealed trait disabled
    extends StObject
       with TextTrackMode
  inline def disabled: disabled = "disabled".asInstanceOf[disabled]
  
  @js.native
  sealed trait disambiguation
    extends StObject
       with ListeningState
  inline def disambiguation: disambiguation = "disambiguation".asInstanceOf[disambiguation]
  
  @js.native
  sealed trait disconnected
    extends StObject
       with RTCIceConnectionState
       with RTCIceTransportState
       with RTCPeerConnectionState
  inline def disconnected: disconnected = "disconnected".asInstanceOf[disconnected]
  
  @js.native
  sealed trait discouraged
    extends StObject
       with ResidentKeyRequirement
       with UserVerificationRequirement
  inline def discouraged: discouraged = "discouraged".asInstanceOf[discouraged]
  
  @js.native
  sealed trait discrete
    extends StObject
       with ChannelInterpretation
  inline def discrete: discrete = "discrete".asInstanceOf[discrete]
  
  @js.native
  sealed trait `display-capture`
    extends StObject
       with PermissionName
  inline def `display-capture`: `display-capture` = "display-capture".asInstanceOf[`display-capture`]
  
  @js.native
  sealed trait div extends StObject
  inline def div: div = "div".asInstanceOf[div]
  
  @js.native
  sealed trait dl extends StObject
  inline def dl: dl = "dl".asInstanceOf[dl]
  
  @js.native
  sealed trait document
    extends StObject
       with RequestDestination
       with XMLHttpRequestResponseType
  inline def document: document = "document".asInstanceOf[document]
  
  @js.native
  sealed trait done
    extends StObject
       with IDBRequestReadyState
  inline def done: done = "done".asInstanceOf[done]
  
  @js.native
  sealed trait down
    extends StObject
       with NavigationReason
  inline def down: down = "down".asInstanceOf[down]
  
  @js.native
  sealed trait downloading extends StObject
  inline def downloading: downloading = "downloading".asInstanceOf[downloading]
  
  @js.native
  sealed trait drag extends StObject
  inline def drag: drag = "drag".asInstanceOf[drag]
  
  @js.native
  sealed trait dragend extends StObject
  inline def dragend: dragend = "dragend".asInstanceOf[dragend]
  
  @js.native
  sealed trait dragenter extends StObject
  inline def dragenter: dragenter = "dragenter".asInstanceOf[dragenter]
  
  @js.native
  sealed trait dragexit extends StObject
  inline def dragexit: dragexit = "dragexit".asInstanceOf[dragexit]
  
  @js.native
  sealed trait dragleave extends StObject
  inline def dragleave: dragleave = "dragleave".asInstanceOf[dragleave]
  
  @js.native
  sealed trait dragover extends StObject
  inline def dragover: dragover = "dragover".asInstanceOf[dragover]
  
  @js.native
  sealed trait dragstart extends StObject
  inline def dragstart: dragstart = "dragstart".asInstanceOf[dragstart]
  
  @js.native
  sealed trait drop extends StObject
  inline def drop: drop = "drop".asInstanceOf[drop]
  
  @js.native
  sealed trait dt extends StObject
  inline def dt: dt = "dt".asInstanceOf[dt]
  
  @js.native
  sealed trait `dtls-failure`
    extends StObject
       with RTCErrorDetailType
  inline def `dtls-failure`: `dtls-failure` = "dtls-failure".asInstanceOf[`dtls-failure`]
  
  @js.native
  sealed trait durationchange extends StObject
  inline def durationchange: durationchange = "durationchange".asInstanceOf[durationchange]
  
  @js.native
  sealed trait ellipse extends StObject
  inline def ellipse: ellipse = "ellipse".asInstanceOf[ellipse]
  
  @js.native
  sealed trait em extends StObject
  inline def em: em = "em".asInstanceOf[em]
  
  @js.native
  sealed trait embed
    extends StObject
       with RequestDestination
  inline def embed: embed = "embed".asInstanceOf[embed]
  
  @js.native
  sealed trait emptied extends StObject
  inline def emptied: emptied = "emptied".asInstanceOf[emptied]
  
  @js.native
  sealed trait encrypt
    extends StObject
       with KeyUsage
  inline def encrypt: encrypt = "encrypt".asInstanceOf[encrypt]
  
  @js.native
  sealed trait encrypted extends StObject
  inline def encrypted: encrypted = "encrypted".asInstanceOf[encrypted]
  
  @js.native
  sealed trait end
    extends StObject
       with AlignSetting
       with CanvasTextAlign
       with LineAlignSetting
       with ScrollLogicalPosition
       with SelectionMode
  inline def end: end = "end".asInstanceOf[end]
  
  @js.native
  sealed trait ended
    extends StObject
       with MediaStreamTrackState
       with ReadyState
  inline def ended: ended = "ended".asInstanceOf[ended]
  
  @js.native
  sealed trait enter extends StObject
  inline def enter: enter = "enter".asInstanceOf[enter]
  
  @js.native
  sealed trait enterprise
    extends StObject
       with AttestationConveyancePreference
  inline def enterprise: enterprise = "enterprise".asInstanceOf[enterprise]
  
  @js.native
  sealed trait environment
    extends StObject
       with VideoFacingModeEnum
  inline def environment: environment = "environment".asInstanceOf[environment]
  
  @js.native
  sealed trait equalpower
    extends StObject
       with PanningModelType
  inline def equalpower: equalpower = "equalpower".asInstanceOf[equalpower]
  
  @js.native
  sealed trait error
    extends StObject
       with RequestRedirect
       with ResponseType
  inline def error: error = "error".asInstanceOf[error]
  
  @js.native
  sealed trait evenodd
    extends StObject
       with CanvasFillRule
  inline def evenodd: evenodd = "evenodd".asInstanceOf[evenodd]
  
  @js.native
  sealed trait exit extends StObject
  inline def exit: exit = "exit".asInstanceOf[exit]
  
  @js.native
  sealed trait expired
    extends StObject
       with MediaKeyStatus
  inline def expired: expired = "expired".asInstanceOf[expired]
  
  @js.native
  sealed trait explicit
    extends StObject
       with ChannelCountMode
  inline def explicit: explicit = "explicit".asInstanceOf[explicit]
  
  @js.native
  sealed trait exponential
    extends StObject
       with DistanceModelType
  inline def exponential: exponential = "exponential".asInstanceOf[exponential]
  
  @js.native
  sealed trait f32
    extends StObject
       with ValueType
  inline def f32: f32 = "f32".asInstanceOf[f32]
  
  @js.native
  sealed trait f64
    extends StObject
       with ValueType
  inline def f64: f64 = "f64".asInstanceOf[f64]
  
  @js.native
  sealed trait fail
    extends StObject
       with PaymentComplete
  inline def fail: fail = "fail".asInstanceOf[fail]
  
  @js.native
  sealed trait failed
    extends StObject
       with RTCDtlsTransportState
       with RTCIceConnectionState
       with RTCIceTransportState
       with RTCPeerConnectionState
       with RTCStatsIceCandidatePairState
  inline def failed: failed = "failed".asInstanceOf[failed]
  
  @js.native
  sealed trait feBlend extends StObject
  inline def feBlend: feBlend = "feBlend".asInstanceOf[feBlend]
  
  @js.native
  sealed trait feColorMatrix extends StObject
  inline def feColorMatrix: feColorMatrix = "feColorMatrix".asInstanceOf[feColorMatrix]
  
  @js.native
  sealed trait feComponentTransfer extends StObject
  inline def feComponentTransfer: feComponentTransfer = "feComponentTransfer".asInstanceOf[feComponentTransfer]
  
  @js.native
  sealed trait feComposite extends StObject
  inline def feComposite: feComposite = "feComposite".asInstanceOf[feComposite]
  
  @js.native
  sealed trait feConvolveMatrix extends StObject
  inline def feConvolveMatrix: feConvolveMatrix = "feConvolveMatrix".asInstanceOf[feConvolveMatrix]
  
  @js.native
  sealed trait feDiffuseLighting extends StObject
  inline def feDiffuseLighting: feDiffuseLighting = "feDiffuseLighting".asInstanceOf[feDiffuseLighting]
  
  @js.native
  sealed trait feDisplacementMap extends StObject
  inline def feDisplacementMap: feDisplacementMap = "feDisplacementMap".asInstanceOf[feDisplacementMap]
  
  @js.native
  sealed trait feDistantLight extends StObject
  inline def feDistantLight: feDistantLight = "feDistantLight".asInstanceOf[feDistantLight]
  
  @js.native
  sealed trait feFlood extends StObject
  inline def feFlood: feFlood = "feFlood".asInstanceOf[feFlood]
  
  @js.native
  sealed trait feFuncA extends StObject
  inline def feFuncA: feFuncA = "feFuncA".asInstanceOf[feFuncA]
  
  @js.native
  sealed trait feFuncB extends StObject
  inline def feFuncB: feFuncB = "feFuncB".asInstanceOf[feFuncB]
  
  @js.native
  sealed trait feFuncG extends StObject
  inline def feFuncG: feFuncG = "feFuncG".asInstanceOf[feFuncG]
  
  @js.native
  sealed trait feFuncR extends StObject
  inline def feFuncR: feFuncR = "feFuncR".asInstanceOf[feFuncR]
  
  @js.native
  sealed trait feGaussianBlur extends StObject
  inline def feGaussianBlur: feGaussianBlur = "feGaussianBlur".asInstanceOf[feGaussianBlur]
  
  @js.native
  sealed trait feImage extends StObject
  inline def feImage: feImage = "feImage".asInstanceOf[feImage]
  
  @js.native
  sealed trait feMerge extends StObject
  inline def feMerge: feMerge = "feMerge".asInstanceOf[feMerge]
  
  @js.native
  sealed trait feMergeNode extends StObject
  inline def feMergeNode: feMergeNode = "feMergeNode".asInstanceOf[feMergeNode]
  
  @js.native
  sealed trait feMorphology extends StObject
  inline def feMorphology: feMorphology = "feMorphology".asInstanceOf[feMorphology]
  
  @js.native
  sealed trait feOffset extends StObject
  inline def feOffset: feOffset = "feOffset".asInstanceOf[feOffset]
  
  @js.native
  sealed trait fePointLight extends StObject
  inline def fePointLight: fePointLight = "fePointLight".asInstanceOf[fePointLight]
  
  @js.native
  sealed trait feSpecularLighting extends StObject
  inline def feSpecularLighting: feSpecularLighting = "feSpecularLighting".asInstanceOf[feSpecularLighting]
  
  @js.native
  sealed trait feSpotLight extends StObject
  inline def feSpotLight: feSpotLight = "feSpotLight".asInstanceOf[feSpotLight]
  
  @js.native
  sealed trait feTile extends StObject
  inline def feTile: feTile = "feTile".asInstanceOf[feTile]
  
  @js.native
  sealed trait feTurbulence extends StObject
  inline def feTurbulence: feTurbulence = "feTurbulence".asInstanceOf[feTurbulence]
  
  @js.native
  sealed trait fieldset extends StObject
  inline def fieldset: fieldset = "fieldset".asInstanceOf[fieldset]
  
  @js.native
  sealed trait figcaption extends StObject
  inline def figcaption: figcaption = "figcaption".asInstanceOf[figcaption]
  
  @js.native
  sealed trait figure extends StObject
  inline def figure: figure = "figure".asInstanceOf[figure]
  
  @js.native
  sealed trait filter extends StObject
  inline def filter: filter = "filter".asInstanceOf[filter]
  
  @js.native
  sealed trait `fingerprint-failure`
    extends StObject
       with RTCErrorDetailType
  inline def `fingerprint-failure`: `fingerprint-failure` = "fingerprint-failure".asInstanceOf[`fingerprint-failure`]
  
  @js.native
  sealed trait finish extends StObject
  inline def finish: finish = "finish".asInstanceOf[finish]
  
  @js.native
  sealed trait finished
    extends StObject
       with AnimationPlayState
  inline def finished: finished = "finished".asInstanceOf[finished]
  
  @js.native
  sealed trait flipY
    extends StObject
       with ImageOrientation
  inline def flipY: flipY = "flipY".asInstanceOf[flipY]
  
  @js.native
  sealed trait focus extends StObject
  inline def focus: focus = "focus".asInstanceOf[focus]
  
  @js.native
  sealed trait focusin extends StObject
  inline def focusin: focusin = "focusin".asInstanceOf[focusin]
  
  @js.native
  sealed trait focusout extends StObject
  inline def focusout: focusout = "focusout".asInstanceOf[focusout]
  
  @js.native
  sealed trait follow
    extends StObject
       with RequestRedirect
  inline def follow: follow = "follow".asInstanceOf[follow]
  
  @js.native
  sealed trait font
    extends StObject
       with RequestDestination
  inline def font: font = "font".asInstanceOf[font]
  
  @js.native
  sealed trait footer extends StObject
  inline def footer: footer = "footer".asInstanceOf[footer]
  
  @js.native
  sealed trait `force-cache`
    extends StObject
       with RequestCache
  inline def `force-cache`: `force-cache` = "force-cache".asInstanceOf[`force-cache`]
  
  @js.native
  sealed trait foreignObject extends StObject
  inline def foreignObject: foreignObject = "foreignObject".asInstanceOf[foreignObject]
  
  @js.native
  sealed trait form extends StObject
  inline def form: form = "form".asInstanceOf[form]
  
  @js.native
  sealed trait forward extends StObject
  inline def forward: forward = "forward".asInstanceOf[forward]
  
  @js.native
  sealed trait forwards
    extends StObject
       with FillMode
  inline def forwards: forwards = "forwards".asInstanceOf[forwards]
  
  @js.native
  sealed trait frame extends StObject
  inline def frame: frame = "frame".asInstanceOf[frame]
  
  @js.native
  sealed trait frameset extends StObject
  inline def frameset: frameset = "frameset".asInstanceOf[frameset]
  
  @js.native
  sealed trait frozen
    extends StObject
       with RTCStatsIceCandidatePairState
  inline def frozen: frozen = "frozen".asInstanceOf[frozen]
  
  @js.native
  sealed trait fullscreenchange extends StObject
  inline def fullscreenchange: fullscreenchange = "fullscreenchange".asInstanceOf[fullscreenchange]
  
  @js.native
  sealed trait fullscreenerror extends StObject
  inline def fullscreenerror: fullscreenerror = "fullscreenerror".asInstanceOf[fullscreenerror]
  
  @js.native
  sealed trait function
    extends StObject
       with ImportExportKind
  inline def function: function = "function".asInstanceOf[function]
  
  @js.native
  sealed trait g extends StObject
  inline def g: g = "g".asInstanceOf[g]
  
  @js.native
  sealed trait gamepadconnected extends StObject
  inline def gamepadconnected: gamepadconnected = "gamepadconnected".asInstanceOf[gamepadconnected]
  
  @js.native
  sealed trait gamepaddisconnected extends StObject
  inline def gamepaddisconnected: gamepaddisconnected = "gamepaddisconnected".asInstanceOf[gamepaddisconnected]
  
  @js.native
  sealed trait gathering
    extends StObject
       with RTCIceGathererState
       with RTCIceGatheringState
  inline def gathering: gathering = "gathering".asInstanceOf[gathering]
  
  @js.native
  sealed trait gatheringstatechange extends StObject
  inline def gatheringstatechange: gatheringstatechange = "gatheringstatechange".asInstanceOf[gatheringstatechange]
  
  @js.native
  sealed trait geolocation
    extends StObject
       with MSWebViewPermissionType
       with PermissionName
  inline def geolocation: geolocation = "geolocation".asInstanceOf[geolocation]
  
  @js.native
  sealed trait global
    extends StObject
       with ImportExportKind
  inline def global: global = "global".asInstanceOf[global]
  
  @js.native
  sealed trait gotpointercapture extends StObject
  inline def gotpointercapture: gotpointercapture = "gotpointercapture".asInstanceOf[gotpointercapture]
  
  @js.native
  sealed trait granted
    extends StObject
       with NotificationPermission
       with PermissionState
       with PushPermissionState
  inline def granted: granted = "granted".asInstanceOf[granted]
  
  @js.native
  sealed trait gyroscope
    extends StObject
       with PermissionName
  inline def gyroscope: gyroscope = "gyroscope".asInstanceOf[gyroscope]
  
  @js.native
  sealed trait h1 extends StObject
  inline def h1: h1 = "h1".asInstanceOf[h1]
  
  @js.native
  sealed trait h2 extends StObject
  inline def h2: h2 = "h2".asInstanceOf[h2]
  
  @js.native
  sealed trait h3 extends StObject
  inline def h3: h3 = "h3".asInstanceOf[h3]
  
  @js.native
  sealed trait h4 extends StObject
  inline def h4: h4 = "h4".asInstanceOf[h4]
  
  @js.native
  sealed trait h5 extends StObject
  inline def h5: h5 = "h5".asInstanceOf[h5]
  
  @js.native
  sealed trait h6 extends StObject
  inline def h6: h6 = "h6".asInstanceOf[h6]
  
  @js.native
  sealed trait hanging
    extends StObject
       with CanvasTextBaseline
  inline def hanging: hanging = "hanging".asInstanceOf[hanging]
  
  @js.native
  sealed trait `hardware-encoder-error`
    extends StObject
       with RTCErrorDetailType
  inline def `hardware-encoder-error`: `hardware-encoder-error` = "hardware-encoder-error".asInstanceOf[`hardware-encoder-error`]
  
  @js.native
  sealed trait `hardware-encoder-not-available`
    extends StObject
       with RTCErrorDetailType
  inline def `hardware-encoder-not-available`: `hardware-encoder-not-available` = "hardware-encoder-not-available".asInstanceOf[`hardware-encoder-not-available`]
  
  @js.native
  sealed trait hashchange extends StObject
  inline def hashchange: hashchange = "hashchange".asInstanceOf[hashchange]
  
  @js.native
  sealed trait `have-local-offer`
    extends StObject
       with RTCSignalingState
  inline def `have-local-offer`: `have-local-offer` = "have-local-offer".asInstanceOf[`have-local-offer`]
  
  @js.native
  sealed trait `have-local-pranswer`
    extends StObject
       with RTCSignalingState
  inline def `have-local-pranswer`: `have-local-pranswer` = "have-local-pranswer".asInstanceOf[`have-local-pranswer`]
  
  @js.native
  sealed trait `have-remote-offer`
    extends StObject
       with RTCSignalingState
  inline def `have-remote-offer`: `have-remote-offer` = "have-remote-offer".asInstanceOf[`have-remote-offer`]
  
  @js.native
  sealed trait `have-remote-pranswer`
    extends StObject
       with RTCSignalingState
  inline def `have-remote-pranswer`: `have-remote-pranswer` = "have-remote-pranswer".asInstanceOf[`have-remote-pranswer`]
  
  @js.native
  sealed trait head extends StObject
  inline def head: head = "head".asInstanceOf[head]
  
  @js.native
  sealed trait header extends StObject
  inline def header: header = "header".asInstanceOf[header]
  
  @js.native
  sealed trait hgroup extends StObject
  inline def hgroup: hgroup = "hgroup".asInstanceOf[hgroup]
  
  @js.native
  sealed trait hidden
    extends StObject
       with TextTrackMode
       with VisibilityState
  inline def hidden: hidden = "hidden".asInstanceOf[hidden]
  
  @js.native
  sealed trait hide
    extends StObject
       with FullscreenNavigationUI
  inline def hide: hide = "hide".asInstanceOf[hide]
  
  @js.native
  sealed trait high
    extends StObject
       with ImageSmoothingQuality
       with ResizeQuality
  inline def high: high = "high".asInstanceOf[high]
  
  @js.native
  sealed trait `high-performance`
    extends StObject
       with WebGLPowerPreference
  inline def `high-performance`: `high-performance` = "high-performance".asInstanceOf[`high-performance`]
  
  @js.native
  sealed trait highpass
    extends StObject
       with BiquadFilterType
  inline def highpass: highpass = "highpass".asInstanceOf[highpass]
  
  @js.native
  sealed trait highshelf
    extends StObject
       with BiquadFilterType
  inline def highshelf: highshelf = "highshelf".asInstanceOf[highshelf]
  
  @js.native
  sealed trait host
    extends StObject
       with RTCIceCandidateType
       with RTCStatsIceCandidateType
  inline def host: host = "host".asInstanceOf[host]
  
  @js.native
  sealed trait hr extends StObject
  inline def hr: hr = "hr".asInstanceOf[hr]
  
  @js.native
  sealed trait html extends StObject
  inline def html: html = "html".asInstanceOf[html]
  
  @js.native
  sealed trait httpColonSlashSlashwwwDotw3DotorgSlash1999Slashxhtml extends StObject
  inline def httpColonSlashSlashwwwDotw3DotorgSlash1999Slashxhtml: httpColonSlashSlashwwwDotw3DotorgSlash1999Slashxhtml = "http://www.w3.org/1999/xhtml".asInstanceOf[httpColonSlashSlashwwwDotw3DotorgSlash1999Slashxhtml]
  
  @js.native
  sealed trait httpColonSlashSlashwwwDotw3DotorgSlash2000Slashsvg extends StObject
  inline def httpColonSlashSlashwwwDotw3DotorgSlash2000Slashsvg: httpColonSlashSlashwwwDotw3DotorgSlash2000Slashsvg = "http://www.w3.org/2000/svg".asInstanceOf[httpColonSlashSlashwwwDotw3DotorgSlash2000Slashsvg]
  
  @js.native
  sealed trait i extends StObject
  inline def i: i = "i".asInstanceOf[i]
  
  @js.native
  sealed trait i32
    extends StObject
       with ValueType
  inline def i32: i32 = "i32".asInstanceOf[i32]
  
  @js.native
  sealed trait i64
    extends StObject
       with ValueType
  inline def i64: i64 = "i64".asInstanceOf[i64]
  
  @js.native
  sealed trait `ice-server`
    extends StObject
       with RTCStatsType
  inline def `ice-server`: `ice-server` = "ice-server".asInstanceOf[`ice-server`]
  
  @js.native
  sealed trait icecandidate extends StObject
  inline def icecandidate: icecandidate = "icecandidate".asInstanceOf[icecandidate]
  
  @js.native
  sealed trait icecandidateerror extends StObject
  inline def icecandidateerror: icecandidateerror = "icecandidateerror".asInstanceOf[icecandidateerror]
  
  @js.native
  sealed trait iceconnectionstatechange extends StObject
  inline def iceconnectionstatechange: iceconnectionstatechange = "iceconnectionstatechange".asInstanceOf[iceconnectionstatechange]
  
  @js.native
  sealed trait icegatheringstatechange extends StObject
  inline def icegatheringstatechange: icegatheringstatechange = "icegatheringstatechange".asInstanceOf[icegatheringstatechange]
  
  @js.native
  sealed trait ideographic
    extends StObject
       with CanvasTextBaseline
  inline def ideographic: ideographic = "ideographic".asInstanceOf[ideographic]
  
  @js.native
  sealed trait idle
    extends StObject
       with AnimationPlayState
  inline def idle: idle = "idle".asInstanceOf[idle]
  
  @js.native
  sealed trait iframe extends StObject
  inline def iframe: iframe = "iframe".asInstanceOf[iframe]
  
  @js.native
  sealed trait image
    extends StObject
       with RequestDestination
  inline def image: image = "image".asInstanceOf[image]
  
  @js.native
  sealed trait imageSlashsvgPlussignxml
    extends StObject
       with DOMParserSupportedType
  inline def imageSlashsvgPlussignxml: imageSlashsvgPlussignxml = "image/svg+xml".asInstanceOf[imageSlashsvgPlussignxml]
  
  @js.native
  sealed trait img extends StObject
  inline def img: img = "img".asInstanceOf[img]
  
  @js.native
  sealed trait imports
    extends StObject
       with ServiceWorkerUpdateViaCache
  inline def imports: imports = "imports".asInstanceOf[imports]
  
  @js.native
  sealed trait `in-progress`
    extends StObject
       with RTCStatsIceCandidatePairState
  inline def `in-progress`: `in-progress` = "in-progress".asInstanceOf[`in-progress`]
  
  @js.native
  sealed trait inactive
    extends StObject
       with ListeningState
       with RTCRtpTransceiverDirection
  inline def inactive: inactive = "inactive".asInstanceOf[inactive]
  
  @js.native
  sealed trait `inbound-rtp`
    extends StObject
       with RTCStatsType
  inline def `inbound-rtp`: `inbound-rtp` = "inbound-rtp".asInstanceOf[`inbound-rtp`]
  
  @js.native
  sealed trait include
    extends StObject
       with RequestCredentials
  inline def include: include = "include".asInstanceOf[include]
  
  @js.native
  sealed trait indirect
    extends StObject
       with AttestationConveyancePreference
  inline def indirect: indirect = "indirect".asInstanceOf[indirect]
  
  @js.native
  sealed trait `individualization-request`
    extends StObject
       with MediaKeyMessageType
  inline def `individualization-request`: `individualization-request` = "individualization-request".asInstanceOf[`individualization-request`]
  
  @js.native
  sealed trait inherit
    extends StObject
       with CanvasDirection
  inline def inherit: inherit = "inherit".asInstanceOf[inherit]
  
  @js.native
  sealed trait input extends StObject
  inline def input: input = "input".asInstanceOf[input]
  
  @js.native
  sealed trait ins extends StObject
  inline def ins: ins = "ins".asInstanceOf[ins]
  
  @js.native
  sealed trait installed
    extends StObject
       with ServiceWorkerState
  inline def installed: installed = "installed".asInstanceOf[installed]
  
  @js.native
  sealed trait installing
    extends StObject
       with ServiceWorkerState
  inline def installing: installing = "installing".asInstanceOf[installing]
  
  @js.native
  sealed trait interactive
    extends StObject
       with AudioContextLatencyCategory
       with DocumentReadyState
  inline def interactive: interactive = "interactive".asInstanceOf[interactive]
  
  @js.native
  sealed trait internal
    extends StObject
       with AuthenticatorTransport
  inline def internal: internal = "internal".asInstanceOf[internal]
  
  @js.native
  sealed trait `internal-error`
    extends StObject
       with MediaKeyStatus
  inline def `internal-error`: `internal-error` = "internal-error".asInstanceOf[`internal-error`]
  
  @js.native
  sealed trait interrupted
    extends StObject
       with SpeechSynthesisErrorCode
  inline def interrupted: interrupted = "interrupted".asInstanceOf[interrupted]
  
  @js.native
  sealed trait invalid extends StObject
  inline def invalid: invalid = "invalid".asInstanceOf[invalid]
  
  @js.native
  sealed trait `invalid-argument`
    extends StObject
       with SpeechSynthesisErrorCode
  inline def `invalid-argument`: `invalid-argument` = "invalid-argument".asInstanceOf[`invalid-argument`]
  
  @js.native
  sealed trait inverse
    extends StObject
       with DistanceModelType
  inline def inverse: inverse = "inverse".asInstanceOf[inverse]
  
  @js.native
  sealed trait json
    extends StObject
       with XMLHttpRequestResponseType
  inline def json: json = "json".asInstanceOf[json]
  
  @js.native
  sealed trait jwk
    extends StObject
       with KeyFormat
  inline def jwk: jwk = "jwk".asInstanceOf[jwk]
  
  @js.native
  sealed trait `k-rate`
    extends StObject
       with AutomationRate
  inline def `k-rate`: `k-rate` = "k-rate".asInstanceOf[`k-rate`]
  
  @js.native
  sealed trait kbd extends StObject
  inline def kbd: kbd = "kbd".asInstanceOf[kbd]
  
  @js.native
  sealed trait keydown extends StObject
  inline def keydown: keydown = "keydown".asInstanceOf[keydown]
  
  @js.native
  sealed trait keypress extends StObject
  inline def keypress: keypress = "keypress".asInstanceOf[keypress]
  
  @js.native
  sealed trait keystatuseschange extends StObject
  inline def keystatuseschange: keystatuseschange = "keystatuseschange".asInstanceOf[keystatuseschange]
  
  @js.native
  sealed trait keyup extends StObject
  inline def keyup: keyup = "keyup".asInstanceOf[keyup]
  
  @js.native
  sealed trait label extends StObject
  inline def label: label = "label".asInstanceOf[label]
  
  @js.native
  sealed trait landscape
    extends StObject
       with OrientationLockType
  inline def landscape: landscape = "landscape".asInstanceOf[landscape]
  
  @js.native
  sealed trait `landscape-primary`
    extends StObject
       with OrientationLockType
       with OrientationType
  inline def `landscape-primary`: `landscape-primary` = "landscape-primary".asInstanceOf[`landscape-primary`]
  
  @js.native
  sealed trait `landscape-secondary`
    extends StObject
       with OrientationLockType
       with OrientationType
  inline def `landscape-secondary`: `landscape-secondary` = "landscape-secondary".asInstanceOf[`landscape-secondary`]
  
  @js.native
  sealed trait `language-not-supported`
    extends StObject
       with SpeechRecognitionErrorCode
  inline def `language-not-supported`: `language-not-supported` = "language-not-supported".asInstanceOf[`language-not-supported`]
  
  @js.native
  sealed trait `language-unavailable`
    extends StObject
       with SpeechSynthesisErrorCode
  inline def `language-unavailable`: `language-unavailable` = "language-unavailable".asInstanceOf[`language-unavailable`]
  
  @js.native
  sealed trait languagechange extends StObject
  inline def languagechange: languagechange = "languagechange".asInstanceOf[languagechange]
  
  @js.native
  sealed trait left
    extends StObject
       with AlignSetting
       with CanvasTextAlign
       with GamepadHand
       with NavigationReason
       with VideoFacingModeEnum
  inline def left: left = "left".asInstanceOf[left]
  
  @js.native
  sealed trait legend extends StObject
  inline def legend: legend = "legend".asInstanceOf[legend]
  
  @js.native
  sealed trait li extends StObject
  inline def li: li = "li".asInstanceOf[li]
  
  @js.native
  sealed trait `license-release`
    extends StObject
       with MediaKeyMessageType
  inline def `license-release`: `license-release` = "license-release".asInstanceOf[`license-release`]
  
  @js.native
  sealed trait `license-renewal`
    extends StObject
       with MediaKeyMessageType
  inline def `license-renewal`: `license-renewal` = "license-renewal".asInstanceOf[`license-renewal`]
  
  @js.native
  sealed trait `license-request`
    extends StObject
       with MediaKeyMessageType
  inline def `license-request`: `license-request` = "license-request".asInstanceOf[`license-request`]
  
  @js.native
  sealed trait line extends StObject
  inline def line: line = "line".asInstanceOf[line]
  
  @js.native
  sealed trait `line-left`
    extends StObject
       with PositionAlignSetting
  inline def `line-left`: `line-left` = "line-left".asInstanceOf[`line-left`]
  
  @js.native
  sealed trait `line-right`
    extends StObject
       with PositionAlignSetting
  inline def `line-right`: `line-right` = "line-right".asInstanceOf[`line-right`]
  
  @js.native
  sealed trait linear
    extends StObject
       with DistanceModelType
  inline def linear: linear = "linear".asInstanceOf[linear]
  
  @js.native
  sealed trait linearGradient extends StObject
  inline def linearGradient: linearGradient = "linearGradient".asInstanceOf[linearGradient]
  
  @js.native
  sealed trait link extends StObject
  inline def link: link = "link".asInstanceOf[link]
  
  @js.native
  sealed trait linkMove extends StObject
  inline def linkMove: linkMove = "linkMove".asInstanceOf[linkMove]
  
  @js.native
  sealed trait listing extends StObject
  inline def listing: listing = "listing".asInstanceOf[listing]
  
  @js.native
  sealed trait live
    extends StObject
       with MediaStreamTrackState
  inline def live: live = "live".asInstanceOf[live]
  
  @js.native
  sealed trait load extends StObject
  inline def load: load = "load".asInstanceOf[load]
  
  @js.native
  sealed trait loadeddata extends StObject
  inline def loadeddata: loadeddata = "loadeddata".asInstanceOf[loadeddata]
  
  @js.native
  sealed trait loadedmetadata extends StObject
  inline def loadedmetadata: loadedmetadata = "loadedmetadata".asInstanceOf[loadedmetadata]
  
  @js.native
  sealed trait loadend extends StObject
  inline def loadend: loadend = "loadend".asInstanceOf[loadend]
  
  @js.native
  sealed trait loading
    extends StObject
       with DocumentReadyState
  inline def loading: loading = "loading".asInstanceOf[loading]
  
  @js.native
  sealed trait loadstart extends StObject
  inline def loadstart: loadstart = "loadstart".asInstanceOf[loadstart]
  
  @js.native
  sealed trait `local-candidate`
    extends StObject
       with RTCStatsType
  inline def `local-candidate`: `local-candidate` = "local-candidate".asInstanceOf[`local-candidate`]
  
  @js.native
  sealed trait localcandidate extends StObject
  inline def localcandidate: localcandidate = "localcandidate".asInstanceOf[localcandidate]
  
  @js.native
  sealed trait long extends StObject
  inline def long: long = "long".asInstanceOf[long]
  
  @js.native
  sealed trait lookup extends StObject
  inline def lookup: lookup = "lookup".asInstanceOf[lookup]
  
  @js.native
  sealed trait lostpointercapture extends StObject
  inline def lostpointercapture: lostpointercapture = "lostpointercapture".asInstanceOf[lostpointercapture]
  
  @js.native
  sealed trait low
    extends StObject
       with ImageSmoothingQuality
       with ResizeQuality
  inline def low: low = "low".asInstanceOf[low]
  
  @js.native
  sealed trait `low-power`
    extends StObject
       with WebGLPowerPreference
  inline def `low-power`: `low-power` = "low-power".asInstanceOf[`low-power`]
  
  @js.native
  sealed trait lowpass
    extends StObject
       with BiquadFilterType
  inline def lowpass: lowpass = "lowpass".asInstanceOf[lowpass]
  
  @js.native
  sealed trait lowshelf
    extends StObject
       with BiquadFilterType
  inline def lowshelf: lowshelf = "lowshelf".asInstanceOf[lowshelf]
  
  @js.native
  sealed trait lr
    extends StObject
       with DirectionSetting
  inline def lr: lr = "lr".asInstanceOf[lr]
  
  @js.native
  sealed trait ltr
    extends StObject
       with CanvasDirection
       with NotificationDirection
  inline def ltr: ltr = "ltr".asInstanceOf[ltr]
  
  @js.native
  sealed trait magnetometer
    extends StObject
       with PermissionName
  inline def magnetometer: magnetometer = "magnetometer".asInstanceOf[magnetometer]
  
  @js.native
  sealed trait main extends StObject
  inline def main: main = "main".asInstanceOf[main]
  
  @js.native
  sealed trait manifest
    extends StObject
       with RequestDestination
  inline def manifest: manifest = "manifest".asInstanceOf[manifest]
  
  @js.native
  sealed trait manual
    extends StObject
       with RequestRedirect
       with ScrollRestoration
  inline def manual: manual = "manual".asInstanceOf[manual]
  
  @js.native
  sealed trait map extends StObject
  inline def map: map = "map".asInstanceOf[map]
  
  @js.native
  sealed trait mark extends StObject
  inline def mark: mark = "mark".asInstanceOf[mark]
  
  @js.native
  sealed trait marker extends StObject
  inline def marker: marker = "marker".asInstanceOf[marker]
  
  @js.native
  sealed trait marquee extends StObject
  inline def marquee: marquee = "marquee".asInstanceOf[marquee]
  
  @js.native
  sealed trait mask extends StObject
  inline def mask: mask = "mask".asInstanceOf[mask]
  
  @js.native
  sealed trait max
    extends StObject
       with ChannelCountMode
  inline def max: max = "max".asInstanceOf[max]
  
  @js.native
  sealed trait `max-bundle`
    extends StObject
       with RTCBundlePolicy
  inline def `max-bundle`: `max-bundle` = "max-bundle".asInstanceOf[`max-bundle`]
  
  @js.native
  sealed trait `max-compat`
    extends StObject
       with RTCBundlePolicy
  inline def `max-compat`: `max-compat` = "max-compat".asInstanceOf[`max-compat`]
  
  @js.native
  sealed trait maybe
    extends StObject
       with CanPlayTypeResult
  inline def maybe: maybe = "maybe".asInstanceOf[maybe]
  
  @js.native
  sealed trait media
    extends StObject
       with MSWebViewPermissionType
  inline def media: media = "media".asInstanceOf[media]
  
  @js.native
  sealed trait `media-source`
    extends StObject
       with RTCStatsType
  inline def `media-source`: `media-source` = "media-source".asInstanceOf[`media-source`]
  
  @js.native
  sealed trait medium
    extends StObject
       with ImageSmoothingQuality
       with ResizeQuality
  inline def medium: medium = "medium".asInstanceOf[medium]
  
  @js.native
  sealed trait memory
    extends StObject
       with ImportExportKind
  inline def memory: memory = "memory".asInstanceOf[memory]
  
  @js.native
  sealed trait menu extends StObject
  inline def menu: menu = "menu".asInstanceOf[menu]
  
  @js.native
  sealed trait message extends StObject
  inline def message: message = "message".asInstanceOf[message]
  
  @js.native
  sealed trait messageerror extends StObject
  inline def messageerror: messageerror = "messageerror".asInstanceOf[messageerror]
  
  @js.native
  sealed trait meta extends StObject
  inline def meta: meta = "meta".asInstanceOf[meta]
  
  @js.native
  sealed trait metadata
    extends StObject
       with TextTrackKind
  inline def metadata: metadata = "metadata".asInstanceOf[metadata]
  
  @js.native
  sealed trait meter extends StObject
  inline def meter: meter = "meter".asInstanceOf[meter]
  
  @js.native
  sealed trait microphone
    extends StObject
       with PermissionName
  inline def microphone: microphone = "microphone".asInstanceOf[microphone]
  
  @js.native
  sealed trait middle
    extends StObject
       with CanvasTextBaseline
  inline def middle: middle = "middle".asInstanceOf[middle]
  
  @js.native
  sealed trait midi
    extends StObject
       with PermissionName
  inline def midi: midi = "midi".asInstanceOf[midi]
  
  @js.native
  sealed trait miter
    extends StObject
       with CanvasLineJoin
  inline def miter: miter = "miter".asInstanceOf[miter]
  
  @js.native
  sealed trait module
    extends StObject
       with WorkerType
  inline def module: module = "module".asInstanceOf[module]
  
  @js.native
  sealed trait monitor
    extends StObject
       with DisplayCaptureSurfaceType
  inline def monitor: monitor = "monitor".asInstanceOf[monitor]
  
  @js.native
  sealed trait mounted
    extends StObject
       with VRDisplayEventReason
  inline def mounted: mounted = "mounted".asInstanceOf[mounted]
  
  @js.native
  sealed trait mousedown extends StObject
  inline def mousedown: mousedown = "mousedown".asInstanceOf[mousedown]
  
  @js.native
  sealed trait mouseenter extends StObject
  inline def mouseenter: mouseenter = "mouseenter".asInstanceOf[mouseenter]
  
  @js.native
  sealed trait mouseleave extends StObject
  inline def mouseleave: mouseleave = "mouseleave".asInstanceOf[mouseleave]
  
  @js.native
  sealed trait mousemove extends StObject
  inline def mousemove: mousemove = "mousemove".asInstanceOf[mousemove]
  
  @js.native
  sealed trait mouseout extends StObject
  inline def mouseout: mouseout = "mouseout".asInstanceOf[mouseout]
  
  @js.native
  sealed trait mouseover extends StObject
  inline def mouseover: mouseover = "mouseover".asInstanceOf[mouseover]
  
  @js.native
  sealed trait mouseup extends StObject
  inline def mouseup: mouseup = "mouseup".asInstanceOf[mouseup]
  
  @js.native
  sealed trait mousewheel extends StObject
  inline def mousewheel: mousewheel = "mousewheel".asInstanceOf[mousewheel]
  
  @js.native
  sealed trait move extends StObject
  inline def move: move = "move".asInstanceOf[move]
  
  @js.native
  sealed trait mute extends StObject
  inline def mute: mute = "mute".asInstanceOf[mute]
  
  @js.native
  sealed trait narrow extends StObject
  inline def narrow: narrow = "narrow".asInstanceOf[narrow]
  
  @js.native
  sealed trait native
    extends StObject
       with EndingType
  inline def native: native = "native".asInstanceOf[native]
  
  @js.native
  sealed trait natural
    extends StObject
       with OrientationLockType
  inline def natural: natural = "natural".asInstanceOf[natural]
  
  @js.native
  sealed trait nav extends StObject
  inline def nav: nav = "nav".asInstanceOf[nav]
  
  @js.native
  sealed trait navigate
    extends StObject
       with NavigationType
       with RequestMode
  inline def navigate: navigate = "navigate".asInstanceOf[navigate]
  
  @js.native
  sealed trait navigation
    extends StObject
       with VRDisplayEventReason
  inline def navigation: navigation = "navigation".asInstanceOf[navigation]
  
  @js.native
  sealed trait nearest
    extends StObject
       with ScrollLogicalPosition
  inline def nearest: nearest = "nearest".asInstanceOf[nearest]
  
  @js.native
  sealed trait negotiationneeded extends StObject
  inline def negotiationneeded: negotiationneeded = "negotiationneeded".asInstanceOf[negotiationneeded]
  
  @js.native
  sealed trait network
    extends StObject
       with EndOfStreamError
       with SpeechRecognitionErrorCode
       with SpeechSynthesisErrorCode
  inline def network: network = "network".asInstanceOf[network]
  
  @js.native
  sealed trait `new`
    extends StObject
       with RTCDtlsTransportState
       with RTCIceConnectionState
       with RTCIceGathererState
       with RTCIceGatheringState
       with RTCIceTransportState
       with RTCPeerConnectionState
  inline def `new`: `new` = "new".asInstanceOf[`new`]
  
  @js.native
  sealed trait next
    extends StObject
       with IDBCursorDirection
  inline def next: next = "next".asInstanceOf[next]
  
  @js.native
  sealed trait nextunique
    extends StObject
       with IDBCursorDirection
  inline def nextunique: nextunique = "nextunique".asInstanceOf[nextunique]
  
  @js.native
  sealed trait nfc_
    extends StObject
       with AuthenticatorTransport
       with PermissionName
  inline def nfc_ : nfc_ = "nfc".asInstanceOf[nfc_]
  
  @js.native
  sealed trait `no-cache`
    extends StObject
       with RequestCache
  inline def `no-cache`: `no-cache` = "no-cache".asInstanceOf[`no-cache`]
  
  @js.native
  sealed trait `no-cors`
    extends StObject
       with RequestMode
  inline def `no-cors`: `no-cors` = "no-cors".asInstanceOf[`no-cors`]
  
  @js.native
  sealed trait `no-referrer`
    extends StObject
       with ReferrerPolicy
  inline def `no-referrer`: `no-referrer` = "no-referrer".asInstanceOf[`no-referrer`]
  
  @js.native
  sealed trait `no-referrer-when-downgrade`
    extends StObject
       with ReferrerPolicy
  inline def `no-referrer-when-downgrade`: `no-referrer-when-downgrade` = "no-referrer-when-downgrade".asInstanceOf[`no-referrer-when-downgrade`]
  
  @js.native
  sealed trait `no-speech`
    extends StObject
       with SpeechRecognitionErrorCode
  inline def `no-speech`: `no-speech` = "no-speech".asInstanceOf[`no-speech`]
  
  @js.native
  sealed trait `no-store`
    extends StObject
       with RequestCache
  inline def `no-store`: `no-store` = "no-store".asInstanceOf[`no-store`]
  
  @js.native
  sealed trait nohost
    extends StObject
       with RTCIceGatherPolicy
  inline def nohost: nohost = "nohost".asInstanceOf[nohost]
  
  @js.native
  sealed trait nomatch extends StObject
  inline def nomatch: nomatch = "nomatch".asInstanceOf[nomatch]
  
  @js.native
  sealed trait none
    extends StObject
       with AttestationConveyancePreference
       with ColorSpaceConversion
       with FillMode
       with ImageOrientation
       with OverSampleType
       with PremultiplyAlpha
       with ServiceWorkerUpdateViaCache
  inline def none: none = "none".asInstanceOf[none]
  
  @js.native
  sealed trait nonzero
    extends StObject
       with CanvasFillRule
  inline def nonzero: nonzero = "nonzero".asInstanceOf[nonzero]
  
  @js.native
  sealed trait normal
    extends StObject
       with PlaybackDirection
  inline def normal: normal = "normal".asInstanceOf[normal]
  
  @js.native
  sealed trait noscript extends StObject
  inline def noscript: noscript = "noscript".asInstanceOf[noscript]
  
  @js.native
  sealed trait `not-allowed`
    extends StObject
       with MediaKeysRequirement
       with SpeechRecognitionErrorCode
       with SpeechSynthesisErrorCode
  inline def `not-allowed`: `not-allowed` = "not-allowed".asInstanceOf[`not-allowed`]
  
  @js.native
  sealed trait notch
    extends StObject
       with BiquadFilterType
  inline def notch: notch = "notch".asInstanceOf[notch]
  
  @js.native
  sealed trait notifications
    extends StObject
       with PermissionName
  inline def notifications: notifications = "notifications".asInstanceOf[notifications]
  
  @js.native
  sealed trait noupdate extends StObject
  inline def noupdate: noupdate = "noupdate".asInstanceOf[noupdate]
  
  @js.native
  sealed trait number extends StObject
  inline def number: number = "number".asInstanceOf[number]
  
  @js.native
  sealed trait numeric extends StObject
  inline def numeric: numeric = "numeric".asInstanceOf[numeric]
  
  @js.native
  sealed trait `object`
    extends StObject
       with RequestDestination
  inline def `object`: `object` = "object".asInstanceOf[`object`]
  
  @js.native
  sealed trait obsolete extends StObject
  inline def obsolete: obsolete = "obsolete".asInstanceOf[obsolete]
  
  @js.native
  sealed trait offer
    extends StObject
       with RTCSdpType
  inline def offer: offer = "offer".asInstanceOf[offer]
  
  @js.native
  sealed trait offline extends StObject
  inline def offline: offline = "offline".asInstanceOf[offline]
  
  @js.native
  sealed trait ol extends StObject
  inline def ol: ol = "ol".asInstanceOf[ol]
  
  @js.native
  sealed trait omit
    extends StObject
       with RequestCredentials
  inline def omit: omit = "omit".asInstanceOf[omit]
  
  @js.native
  sealed trait online extends StObject
  inline def online: online = "online".asInstanceOf[online]
  
  @js.native
  sealed trait `only-if-cached`
    extends StObject
       with RequestCache
  inline def `only-if-cached`: `only-if-cached` = "only-if-cached".asInstanceOf[`only-if-cached`]
  
  @js.native
  sealed trait opaque
    extends StObject
       with ResponseType
  inline def opaque: opaque = "opaque".asInstanceOf[opaque]
  
  @js.native
  sealed trait opaqueredirect
    extends StObject
       with ResponseType
  inline def opaqueredirect: opaqueredirect = "opaqueredirect".asInstanceOf[opaqueredirect]
  
  @js.native
  sealed trait open
    extends StObject
       with RTCDataChannelState
       with ReadyState
       with ShadowRootMode
  inline def open: open = "open".asInstanceOf[open]
  
  @js.native
  sealed trait optgroup extends StObject
  inline def optgroup: optgroup = "optgroup".asInstanceOf[optgroup]
  
  @js.native
  sealed trait option extends StObject
  inline def option: option = "option".asInstanceOf[option]
  
  @js.native
  sealed trait optional
    extends StObject
       with CredentialMediationRequirement
       with MediaKeysRequirement
  inline def optional: optional = "optional".asInstanceOf[optional]
  
  @js.native
  sealed trait orientationchange extends StObject
  inline def orientationchange: orientationchange = "orientationchange".asInstanceOf[orientationchange]
  
  @js.native
  sealed trait origin
    extends StObject
       with ReferrerPolicy
  inline def origin: origin = "origin".asInstanceOf[origin]
  
  @js.native
  sealed trait `origin-when-cross-origin`
    extends StObject
       with ReferrerPolicy
  inline def `origin-when-cross-origin`: `origin-when-cross-origin` = "origin-when-cross-origin".asInstanceOf[`origin-when-cross-origin`]
  
  @js.native
  sealed trait `outbound-rtp`
    extends StObject
       with RTCStatsType
  inline def `outbound-rtp`: `outbound-rtp` = "outbound-rtp".asInstanceOf[`outbound-rtp`]
  
  @js.native
  sealed trait output extends StObject
  inline def output: output = "output".asInstanceOf[output]
  
  @js.native
  sealed trait `output-downscaled`
    extends StObject
       with MediaKeyStatus
  inline def `output-downscaled`: `output-downscaled` = "output-downscaled".asInstanceOf[`output-downscaled`]
  
  @js.native
  sealed trait `output-restricted`
    extends StObject
       with MediaKeyStatus
  inline def `output-restricted`: `output-restricted` = "output-restricted".asInstanceOf[`output-restricted`]
  
  @js.native
  sealed trait p extends StObject
  inline def p: p = "p".asInstanceOf[p]
  
  @js.native
  sealed trait p256dh
    extends StObject
       with PushEncryptionKeyName
  inline def p256dh: p256dh = "p256dh".asInstanceOf[p256dh]
  
  @js.native
  sealed trait pagehide extends StObject
  inline def pagehide: pagehide = "pagehide".asInstanceOf[pagehide]
  
  @js.native
  sealed trait pageshow extends StObject
  inline def pageshow: pageshow = "pageshow".asInstanceOf[pageshow]
  
  @js.native
  sealed trait paintworklet
    extends StObject
       with RequestDestination
  inline def paintworklet: paintworklet = "paintworklet".asInstanceOf[paintworklet]
  
  @js.native
  sealed trait param extends StObject
  inline def param: param = "param".asInstanceOf[param]
  
  @js.native
  sealed trait parsed
    extends StObject
       with ServiceWorkerState
  inline def parsed: parsed = "parsed".asInstanceOf[parsed]
  
  @js.native
  sealed trait passive
    extends StObject
       with RTCIceTcpCandidateType
  inline def passive: passive = "passive".asInstanceOf[passive]
  
  @js.native
  sealed trait password extends StObject
  inline def password: password = "password".asInstanceOf[password]
  
  @js.native
  sealed trait paste extends StObject
  inline def paste: paste = "paste".asInstanceOf[paste]
  
  @js.native
  sealed trait path extends StObject
  inline def path: path = "path".asInstanceOf[path]
  
  @js.native
  sealed trait pattern extends StObject
  inline def pattern: pattern = "pattern".asInstanceOf[pattern]
  
  @js.native
  sealed trait pause extends StObject
  inline def pause: pause = "pause".asInstanceOf[pause]
  
  @js.native
  sealed trait paused
    extends StObject
       with AnimationPlayState
  inline def paused: paused = "paused".asInstanceOf[paused]
  
  @js.native
  sealed trait payerdetailchange extends StObject
  inline def payerdetailchange: payerdetailchange = "payerdetailchange".asInstanceOf[payerdetailchange]
  
  @js.native
  sealed trait paymentmethodchange extends StObject
  inline def paymentmethodchange: paymentmethodchange = "paymentmethodchange".asInstanceOf[paymentmethodchange]
  
  @js.native
  sealed trait peaking
    extends StObject
       with BiquadFilterType
  inline def peaking: peaking = "peaking".asInstanceOf[peaking]
  
  @js.native
  sealed trait `peer-connection`
    extends StObject
       with RTCStatsType
  inline def `peer-connection`: `peer-connection` = "peer-connection".asInstanceOf[`peer-connection`]
  
  @js.native
  sealed trait peerreflexive
    extends StObject
       with RTCStatsIceCandidateType
  inline def peerreflexive: peerreflexive = "peerreflexive".asInstanceOf[peerreflexive]
  
  @js.native
  sealed trait pending
    extends StObject
       with IDBRequestReadyState
  inline def pending: pending = "pending".asInstanceOf[pending]
  
  @js.native
  sealed trait `persistent-license`
    extends StObject
       with MediaKeySessionType
  inline def `persistent-license`: `persistent-license` = "persistent-license".asInstanceOf[`persistent-license`]
  
  @js.native
  sealed trait `persistent-storage`
    extends StObject
       with PermissionName
  inline def `persistent-storage`: `persistent-storage` = "persistent-storage".asInstanceOf[`persistent-storage`]
  
  @js.native
  sealed trait pickup
    extends StObject
       with PaymentShippingType
  inline def pickup: pickup = "pickup".asInstanceOf[pickup]
  
  @js.native
  sealed trait picture extends StObject
  inline def picture: picture = "picture".asInstanceOf[picture]
  
  @js.native
  sealed trait pixelated
    extends StObject
       with ResizeQuality
  inline def pixelated: pixelated = "pixelated".asInstanceOf[pixelated]
  
  @js.native
  sealed trait pkcs8
    extends StObject
       with KeyFormat
  inline def pkcs8: pkcs8 = "pkcs8".asInstanceOf[pkcs8]
  
  @js.native
  sealed trait platform
    extends StObject
       with AuthenticatorAttachment
  inline def platform: platform = "platform".asInstanceOf[platform]
  
  @js.native
  sealed trait play extends StObject
  inline def play: play = "play".asInstanceOf[play]
  
  @js.native
  sealed trait playback
    extends StObject
       with AudioContextLatencyCategory
  inline def playback: playback = "playback".asInstanceOf[playback]
  
  @js.native
  sealed trait playing extends StObject
  inline def playing: playing = "playing".asInstanceOf[playing]
  
  @js.native
  sealed trait pointercancel extends StObject
  inline def pointercancel: pointercancel = "pointercancel".asInstanceOf[pointercancel]
  
  @js.native
  sealed trait pointerdown extends StObject
  inline def pointerdown: pointerdown = "pointerdown".asInstanceOf[pointerdown]
  
  @js.native
  sealed trait pointerenter extends StObject
  inline def pointerenter: pointerenter = "pointerenter".asInstanceOf[pointerenter]
  
  @js.native
  sealed trait pointerleave extends StObject
  inline def pointerleave: pointerleave = "pointerleave".asInstanceOf[pointerleave]
  
  @js.native
  sealed trait pointerlock
    extends StObject
       with MSWebViewPermissionType
  inline def pointerlock: pointerlock = "pointerlock".asInstanceOf[pointerlock]
  
  @js.native
  sealed trait pointerlockchange extends StObject
  inline def pointerlockchange: pointerlockchange = "pointerlockchange".asInstanceOf[pointerlockchange]
  
  @js.native
  sealed trait pointerlockerror extends StObject
  inline def pointerlockerror: pointerlockerror = "pointerlockerror".asInstanceOf[pointerlockerror]
  
  @js.native
  sealed trait pointermove extends StObject
  inline def pointermove: pointermove = "pointermove".asInstanceOf[pointermove]
  
  @js.native
  sealed trait pointerout extends StObject
  inline def pointerout: pointerout = "pointerout".asInstanceOf[pointerout]
  
  @js.native
  sealed trait pointerover extends StObject
  inline def pointerover: pointerover = "pointerover".asInstanceOf[pointerover]
  
  @js.native
  sealed trait pointerup extends StObject
  inline def pointerup: pointerup = "pointerup".asInstanceOf[pointerup]
  
  @js.native
  sealed trait polygon extends StObject
  inline def polygon: polygon = "polygon".asInstanceOf[polygon]
  
  @js.native
  sealed trait polyline extends StObject
  inline def polyline: polyline = "polyline".asInstanceOf[polyline]
  
  @js.native
  sealed trait popstate extends StObject
  inline def popstate: popstate = "popstate".asInstanceOf[popstate]
  
  @js.native
  sealed trait portrait
    extends StObject
       with OrientationLockType
  inline def portrait: portrait = "portrait".asInstanceOf[portrait]
  
  @js.native
  sealed trait `portrait-primary`
    extends StObject
       with OrientationLockType
       with OrientationType
  inline def `portrait-primary`: `portrait-primary` = "portrait-primary".asInstanceOf[`portrait-primary`]
  
  @js.native
  sealed trait `portrait-secondary`
    extends StObject
       with OrientationLockType
       with OrientationType
  inline def `portrait-secondary`: `portrait-secondary` = "portrait-secondary".asInstanceOf[`portrait-secondary`]
  
  @js.native
  sealed trait pranswer
    extends StObject
       with RTCSdpType
  inline def pranswer: pranswer = "pranswer".asInstanceOf[pranswer]
  
  @js.native
  sealed trait pre extends StObject
  inline def pre: pre = "pre".asInstanceOf[pre]
  
  @js.native
  sealed trait preferred
    extends StObject
       with ResidentKeyRequirement
       with UserVerificationRequirement
  inline def preferred: preferred = "preferred".asInstanceOf[preferred]
  
  @js.native
  sealed trait premultiply
    extends StObject
       with PremultiplyAlpha
  inline def premultiply: premultiply = "premultiply".asInstanceOf[premultiply]
  
  @js.native
  sealed trait prerender
    extends StObject
       with NavigationType
  inline def prerender: prerender = "prerender".asInstanceOf[prerender]
  
  @js.native
  sealed trait preserve
    extends StObject
       with SelectionMode
  inline def preserve: preserve = "preserve".asInstanceOf[preserve]
  
  @js.native
  sealed trait prev
    extends StObject
       with IDBCursorDirection
  inline def prev: prev = "prev".asInstanceOf[prev]
  
  @js.native
  sealed trait prevunique
    extends StObject
       with IDBCursorDirection
  inline def prevunique: prevunique = "prevunique".asInstanceOf[prevunique]
  
  @js.native
  sealed trait prflx
    extends StObject
       with RTCIceCandidateType
  inline def prflx: prflx = "prflx".asInstanceOf[prflx]
  
  @js.native
  sealed trait `private`
    extends StObject
       with KeyType
  inline def `private`: `private` = "private".asInstanceOf[`private`]
  
  @js.native
  sealed trait probably
    extends StObject
       with CanPlayTypeResult
  inline def probably: probably = "probably".asInstanceOf[probably]
  
  @js.native
  sealed trait processorerror extends StObject
  inline def processorerror: processorerror = "processorerror".asInstanceOf[processorerror]
  
  @js.native
  sealed trait progress extends StObject
  inline def progress: progress = "progress".asInstanceOf[progress]
  
  @js.native
  sealed trait prompt
    extends StObject
       with PermissionState
       with PushPermissionState
  inline def prompt: prompt = "prompt".asInstanceOf[prompt]
  
  @js.native
  sealed trait public
    extends StObject
       with KeyType
  inline def public: public = "public".asInstanceOf[public]
  
  @js.native
  sealed trait `public-key` extends StObject
  inline def `public-key`: `public-key` = "public-key".asInstanceOf[`public-key`]
  
  @js.native
  sealed trait push
    extends StObject
       with PermissionName
  inline def push: push = "push".asInstanceOf[push]
  
  @js.native
  sealed trait q extends StObject
  inline def q: q = "q".asInstanceOf[q]
  
  @js.native
  sealed trait radialGradient extends StObject
  inline def radialGradient: radialGradient = "radialGradient".asInstanceOf[radialGradient]
  
  @js.native
  sealed trait ratechange extends StObject
  inline def ratechange: ratechange = "ratechange".asInstanceOf[ratechange]
  
  @js.native
  sealed trait raw
    extends StObject
       with KeyFormat
  inline def raw: raw = "raw".asInstanceOf[raw]
  
  @js.native
  sealed trait readonly
    extends StObject
       with IDBTransactionMode
  inline def readonly: readonly = "readonly".asInstanceOf[readonly]
  
  @js.native
  sealed trait readwrite
    extends StObject
       with IDBTransactionMode
  inline def readwrite: readwrite = "readwrite".asInstanceOf[readwrite]
  
  @js.native
  sealed trait readystatechange extends StObject
  inline def readystatechange: readystatechange = "readystatechange".asInstanceOf[readystatechange]
  
  @js.native
  sealed trait receiver
    extends StObject
       with RTCStatsType
  inline def receiver: receiver = "receiver".asInstanceOf[receiver]
  
  @js.native
  sealed trait rect extends StObject
  inline def rect: rect = "rect".asInstanceOf[rect]
  
  @js.native
  sealed trait recvonly
    extends StObject
       with RTCRtpTransceiverDirection
  inline def recvonly: recvonly = "recvonly".asInstanceOf[recvonly]
  
  @js.native
  sealed trait redundant
    extends StObject
       with ServiceWorkerState
  inline def redundant: redundant = "redundant".asInstanceOf[redundant]
  
  @js.native
  sealed trait rejectionhandled extends StObject
  inline def rejectionhandled: rejectionhandled = "rejectionhandled".asInstanceOf[rejectionhandled]
  
  @js.native
  sealed trait relay
    extends StObject
       with RTCIceCandidateType
       with RTCIceGatherPolicy
       with RTCIceTransportPolicy
  inline def relay: relay = "relay".asInstanceOf[relay]
  
  @js.native
  sealed trait relayed
    extends StObject
       with RTCStatsIceCandidateType
  inline def relayed: relayed = "relayed".asInstanceOf[relayed]
  
  @js.native
  sealed trait released
    extends StObject
       with MediaKeyStatus
  inline def released: released = "released".asInstanceOf[released]
  
  @js.native
  sealed trait reload
    extends StObject
       with NavigationType
       with RequestCache
  inline def reload: reload = "reload".asInstanceOf[reload]
  
  @js.native
  sealed trait `remote-candidate`
    extends StObject
       with RTCStatsType
  inline def `remote-candidate`: `remote-candidate` = "remote-candidate".asInstanceOf[`remote-candidate`]
  
  @js.native
  sealed trait `remote-inbound-rtp`
    extends StObject
       with RTCStatsType
  inline def `remote-inbound-rtp`: `remote-inbound-rtp` = "remote-inbound-rtp".asInstanceOf[`remote-inbound-rtp`]
  
  @js.native
  sealed trait `remote-outbound-rtp`
    extends StObject
       with RTCStatsType
  inline def `remote-outbound-rtp`: `remote-outbound-rtp` = "remote-outbound-rtp".asInstanceOf[`remote-outbound-rtp`]
  
  @js.native
  sealed trait removesourcebuffer extends StObject
  inline def removesourcebuffer: removesourcebuffer = "removesourcebuffer".asInstanceOf[removesourcebuffer]
  
  @js.native
  sealed trait removetrack extends StObject
  inline def removetrack: removetrack = "removetrack".asInstanceOf[removetrack]
  
  @js.native
  sealed trait replace
    extends StObject
       with CompositeOperation
       with CompositeOperationOrAuto
       with IterationCompositeOperation
  inline def replace: replace = "replace".asInstanceOf[replace]
  
  @js.native
  sealed trait report
    extends StObject
       with RequestDestination
  inline def report: report = "report".asInstanceOf[report]
  
  @js.native
  sealed trait requested
    extends StObject
       with VRDisplayEventReason
  inline def requested: requested = "requested".asInstanceOf[requested]
  
  @js.native
  sealed trait require extends StObject
  inline def require: require = "require".asInstanceOf[require]
  
  @js.native
  sealed trait required
    extends StObject
       with CredentialMediationRequirement
       with MediaKeysRequirement
       with ResidentKeyRequirement
       with UserVerificationRequirement
  inline def required: required = "required".asInstanceOf[required]
  
  @js.native
  sealed trait reset extends StObject
  inline def reset: reset = "reset".asInstanceOf[reset]
  
  @js.native
  sealed trait resize extends StObject
  inline def resize: resize = "resize".asInstanceOf[resize]
  
  @js.native
  sealed trait resourcetimingbufferfull extends StObject
  inline def resourcetimingbufferfull: resourcetimingbufferfull = "resourcetimingbufferfull".asInstanceOf[resourcetimingbufferfull]
  
  @js.native
  sealed trait result extends StObject
  inline def result: result = "result".asInstanceOf[result]
  
  @js.native
  sealed trait resume extends StObject
  inline def resume: resume = "resume".asInstanceOf[resume]
  
  @js.native
  sealed trait reverse
    extends StObject
       with PlaybackDirection
  inline def reverse: reverse = "reverse".asInstanceOf[reverse]
  
  @js.native
  sealed trait right
    extends StObject
       with AlignSetting
       with CanvasTextAlign
       with GamepadHand
       with NavigationReason
       with VideoFacingModeEnum
  inline def right: right = "right".asInstanceOf[right]
  
  @js.native
  sealed trait rl
    extends StObject
       with DirectionSetting
  inline def rl: rl = "rl".asInstanceOf[rl]
  
  @js.native
  sealed trait rollback
    extends StObject
       with RTCSdpType
  inline def rollback: rollback = "rollback".asInstanceOf[rollback]
  
  @js.native
  sealed trait round
    extends StObject
       with CanvasLineCap
       with CanvasLineJoin
  inline def round: round = "round".asInstanceOf[round]
  
  @js.native
  sealed trait rp extends StObject
  inline def rp: rp = "rp".asInstanceOf[rp]
  
  @js.native
  sealed trait rt extends StObject
  inline def rt: rt = "rt".asInstanceOf[rt]
  
  @js.native
  sealed trait rtcp
    extends StObject
       with RTCIceComponent
  inline def rtcp: rtcp = "rtcp".asInstanceOf[rtcp]
  
  @js.native
  sealed trait rtl
    extends StObject
       with CanvasDirection
       with NotificationDirection
  inline def rtl: rtl = "rtl".asInstanceOf[rtl]
  
  @js.native
  sealed trait rtp
    extends StObject
       with RTCIceComponent
  inline def rtp: rtp = "rtp".asInstanceOf[rtp]
  
  @js.native
  sealed trait ruby extends StObject
  inline def ruby: ruby = "ruby".asInstanceOf[ruby]
  
  @js.native
  sealed trait running
    extends StObject
       with AnimationPlayState
       with AudioContextState
  inline def running: running = "running".asInstanceOf[running]
  
  @js.native
  sealed trait s extends StObject
  inline def s: s = "s".asInstanceOf[s]
  
  @js.native
  sealed trait `same-origin`
    extends StObject
       with ReferrerPolicy
       with RequestCredentials
       with RequestMode
  inline def `same-origin`: `same-origin` = "same-origin".asInstanceOf[`same-origin`]
  
  @js.native
  sealed trait samp extends StObject
  inline def samp: samp = "samp".asInstanceOf[samp]
  
  @js.native
  sealed trait sawtooth
    extends StObject
       with OscillatorType
  inline def sawtooth: sawtooth = "sawtooth".asInstanceOf[sawtooth]
  
  @js.native
  sealed trait script
    extends StObject
       with RequestDestination
  inline def script: script = "script".asInstanceOf[script]
  
  @js.native
  sealed trait scroll extends StObject
  inline def scroll: scroll = "scroll".asInstanceOf[scroll]
  
  @js.native
  sealed trait `sctp-failure`
    extends StObject
       with RTCErrorDetailType
  inline def `sctp-failure`: `sctp-failure` = "sctp-failure".asInstanceOf[`sctp-failure`]
  
  @js.native
  sealed trait `sctp-transport`
    extends StObject
       with RTCStatsType
  inline def `sctp-transport`: `sctp-transport` = "sctp-transport".asInstanceOf[`sctp-transport`]
  
  @js.native
  sealed trait `sdp-syntax-error`
    extends StObject
       with RTCErrorDetailType
  inline def `sdp-syntax-error`: `sdp-syntax-error` = "sdp-syntax-error".asInstanceOf[`sdp-syntax-error`]
  
  @js.native
  sealed trait secret
    extends StObject
       with KeyType
  inline def secret: secret = "secret".asInstanceOf[secret]
  
  @js.native
  sealed trait section extends StObject
  inline def section: section = "section".asInstanceOf[section]
  
  @js.native
  sealed trait securitypolicyviolation extends StObject
  inline def securitypolicyviolation: securitypolicyviolation = "securitypolicyviolation".asInstanceOf[securitypolicyviolation]
  
  @js.native
  sealed trait seeked extends StObject
  inline def seeked: seeked = "seeked".asInstanceOf[seeked]
  
  @js.native
  sealed trait seeking extends StObject
  inline def seeking: seeking = "seeking".asInstanceOf[seeking]
  
  @js.native
  sealed trait segments
    extends StObject
       with AppendMode
  inline def segments: segments = "segments".asInstanceOf[segments]
  
  @js.native
  sealed trait select
    extends StObject
       with SelectionMode
  inline def select: select = "select".asInstanceOf[select]
  
  @js.native
  sealed trait selectedcandidatepairchange extends StObject
  inline def selectedcandidatepairchange: selectedcandidatepairchange = "selectedcandidatepairchange".asInstanceOf[selectedcandidatepairchange]
  
  @js.native
  sealed trait selectionchange extends StObject
  inline def selectionchange: selectionchange = "selectionchange".asInstanceOf[selectionchange]
  
  @js.native
  sealed trait selectstart extends StObject
  inline def selectstart: selectstart = "selectstart".asInstanceOf[selectstart]
  
  @js.native
  sealed trait sender
    extends StObject
       with RTCStatsType
  inline def sender: sender = "sender".asInstanceOf[sender]
  
  @js.native
  sealed trait sendonly
    extends StObject
       with RTCRtpTransceiverDirection
  inline def sendonly: sendonly = "sendonly".asInstanceOf[sendonly]
  
  @js.native
  sealed trait sendrecv
    extends StObject
       with RTCRtpTransceiverDirection
  inline def sendrecv: sendrecv = "sendrecv".asInstanceOf[sendrecv]
  
  @js.native
  sealed trait sequence
    extends StObject
       with AppendMode
  inline def sequence: sequence = "sequence".asInstanceOf[sequence]
  
  @js.native
  sealed trait server
    extends StObject
       with RTCDtlsRole
  inline def server: server = "server".asInstanceOf[server]
  
  @js.native
  sealed trait serverreflexive
    extends StObject
       with RTCStatsIceCandidateType
  inline def serverreflexive: serverreflexive = "serverreflexive".asInstanceOf[serverreflexive]
  
  @js.native
  sealed trait `service-not-allowed`
    extends StObject
       with SpeechRecognitionErrorCode
  inline def `service-not-allowed`: `service-not-allowed` = "service-not-allowed".asInstanceOf[`service-not-allowed`]
  
  @js.native
  sealed trait sharedworker
    extends StObject
       with ClientTypes
       with RequestDestination
  inline def sharedworker: sharedworker = "sharedworker".asInstanceOf[sharedworker]
  
  @js.native
  sealed trait shipping
    extends StObject
       with PaymentShippingType
  inline def shipping: shipping = "shipping".asInstanceOf[shipping]
  
  @js.native
  sealed trait shippingaddresschange extends StObject
  inline def shippingaddresschange: shippingaddresschange = "shippingaddresschange".asInstanceOf[shippingaddresschange]
  
  @js.native
  sealed trait shippingoptionchange extends StObject
  inline def shippingoptionchange: shippingoptionchange = "shippingoptionchange".asInstanceOf[shippingoptionchange]
  
  @js.native
  sealed trait short extends StObject
  inline def short: short = "short".asInstanceOf[short]
  
  @js.native
  sealed trait show
    extends StObject
       with FullscreenNavigationUI
  inline def show: show = "show".asInstanceOf[show]
  
  @js.native
  sealed trait showing
    extends StObject
       with TextTrackMode
  inline def showing: showing = "showing".asInstanceOf[showing]
  
  @js.native
  sealed trait sign
    extends StObject
       with KeyUsage
  inline def sign: sign = "sign".asInstanceOf[sign]
  
  @js.native
  sealed trait signalingstatechange extends StObject
  inline def signalingstatechange: signalingstatechange = "signalingstatechange".asInstanceOf[signalingstatechange]
  
  @js.native
  sealed trait silent
    extends StObject
       with CredentialMediationRequirement
  inline def silent: silent = "silent".asInstanceOf[silent]
  
  @js.native
  sealed trait sine
    extends StObject
       with OscillatorType
  inline def sine: sine = "sine".asInstanceOf[sine]
  
  @js.native
  sealed trait slot extends StObject
  inline def slot: slot = "slot".asInstanceOf[slot]
  
  @js.native
  sealed trait small extends StObject
  inline def small: small = "small".asInstanceOf[small]
  
  @js.native
  sealed trait smooth
    extends StObject
       with ScrollBehavior
  inline def smooth: smooth = "smooth".asInstanceOf[smooth]
  
  @js.native
  sealed trait so
    extends StObject
       with RTCIceTcpCandidateType
  inline def so: so = "so".asInstanceOf[so]
  
  @js.native
  sealed trait soundend extends StObject
  inline def soundend: soundend = "soundend".asInstanceOf[soundend]
  
  @js.native
  sealed trait soundstart extends StObject
  inline def soundstart: soundstart = "soundstart".asInstanceOf[soundstart]
  
  @js.native
  sealed trait source extends StObject
  inline def source: source = "source".asInstanceOf[source]
  
  @js.native
  sealed trait sourceclose extends StObject
  inline def sourceclose: sourceclose = "sourceclose".asInstanceOf[sourceclose]
  
  @js.native
  sealed trait sourceended extends StObject
  inline def sourceended: sourceended = "sourceended".asInstanceOf[sourceended]
  
  @js.native
  sealed trait sourceopen extends StObject
  inline def sourceopen: sourceopen = "sourceopen".asInstanceOf[sourceopen]
  
  @js.native
  sealed trait span extends StObject
  inline def span: span = "span".asInstanceOf[span]
  
  @js.native
  sealed trait speaker
    extends StObject
       with PermissionName
  inline def speaker: speaker = "speaker".asInstanceOf[speaker]
  
  @js.native
  sealed trait speakers
    extends StObject
       with ChannelInterpretation
  inline def speakers: speakers = "speakers".asInstanceOf[speakers]
  
  @js.native
  sealed trait speechend extends StObject
  inline def speechend: speechend = "speechend".asInstanceOf[speechend]
  
  @js.native
  sealed trait speechstart extends StObject
  inline def speechstart: speechstart = "speechstart".asInstanceOf[speechstart]
  
  @js.native
  sealed trait spki
    extends StObject
       with KeyFormat
  inline def spki: spki = "spki".asInstanceOf[spki]
  
  @js.native
  sealed trait square
    extends StObject
       with CanvasLineCap
       with OscillatorType
  inline def square: square = "square".asInstanceOf[square]
  
  @js.native
  sealed trait srflx
    extends StObject
       with RTCIceCandidateType
  inline def srflx: srflx = "srflx".asInstanceOf[srflx]
  
  @js.native
  sealed trait stable
    extends StObject
       with RTCSignalingState
  inline def stable: stable = "stable".asInstanceOf[stable]
  
  @js.native
  sealed trait stalled extends StObject
  inline def stalled: stalled = "stalled".asInstanceOf[stalled]
  
  @js.native
  sealed trait standard
    extends StObject
       with GamepadMappingType
  inline def standard: standard = "standard".asInstanceOf[standard]
  
  @js.native
  sealed trait start
    extends StObject
       with AlignSetting
       with CanvasTextAlign
       with LineAlignSetting
       with ScrollLogicalPosition
       with SelectionMode
  inline def start: start = "start".asInstanceOf[start]
  
  @js.native
  sealed trait statechange extends StObject
  inline def statechange: statechange = "statechange".asInstanceOf[statechange]
  
  @js.native
  sealed trait `status-pending`
    extends StObject
       with MediaKeyStatus
  inline def `status-pending`: `status-pending` = "status-pending".asInstanceOf[`status-pending`]
  
  @js.native
  sealed trait stop extends StObject
  inline def stop: stop = "stop".asInstanceOf[stop]
  
  @js.native
  sealed trait stopped
    extends StObject
       with RTCRtpTransceiverDirection
  inline def stopped: stopped = "stopped".asInstanceOf[stopped]
  
  @js.native
  sealed trait storage extends StObject
  inline def storage: storage = "storage".asInstanceOf[storage]
  
  @js.native
  sealed trait stream
    extends StObject
       with RTCStatsType
  inline def stream: stream = "stream".asInstanceOf[stream]
  
  @js.native
  sealed trait `strict-origin`
    extends StObject
       with ReferrerPolicy
  inline def `strict-origin`: `strict-origin` = "strict-origin".asInstanceOf[`strict-origin`]
  
  @js.native
  sealed trait `strict-origin-when-cross-origin`
    extends StObject
       with ReferrerPolicy
  inline def `strict-origin-when-cross-origin`: `strict-origin-when-cross-origin` = "strict-origin-when-cross-origin".asInstanceOf[`strict-origin-when-cross-origin`]
  
  @js.native
  sealed trait string extends StObject
  inline def string: string = "string".asInstanceOf[string]
  
  @js.native
  sealed trait strong extends StObject
  inline def strong: strong = "strong".asInstanceOf[strong]
  
  @js.native
  sealed trait style
    extends StObject
       with RequestDestination
  inline def style: style = "style".asInstanceOf[style]
  
  @js.native
  sealed trait stylus
    extends StObject
       with TouchType
  inline def stylus: stylus = "stylus".asInstanceOf[stylus]
  
  @js.native
  sealed trait sub extends StObject
  inline def sub: sub = "sub".asInstanceOf[sub]
  
  @js.native
  sealed trait submit extends StObject
  inline def submit: submit = "submit".asInstanceOf[submit]
  
  @js.native
  sealed trait subtitles
    extends StObject
       with TextTrackKind
  inline def subtitles: subtitles = "subtitles".asInstanceOf[subtitles]
  
  @js.native
  sealed trait succeeded
    extends StObject
       with RTCStatsIceCandidatePairState
  inline def succeeded: succeeded = "succeeded".asInstanceOf[succeeded]
  
  @js.native
  sealed trait success
    extends StObject
       with PaymentComplete
  inline def success: success = "success".asInstanceOf[success]
  
  @js.native
  sealed trait summary extends StObject
  inline def summary: summary = "summary".asInstanceOf[summary]
  
  @js.native
  sealed trait sup extends StObject
  inline def sup: sup = "sup".asInstanceOf[sup]
  
  @js.native
  sealed trait suspend extends StObject
  inline def suspend: suspend = "suspend".asInstanceOf[suspend]
  
  @js.native
  sealed trait suspended
    extends StObject
       with AudioContextState
  inline def suspended: suspended = "suspended".asInstanceOf[suspended]
  
  @js.native
  sealed trait svg extends StObject
  inline def svg: svg = "svg".asInstanceOf[svg]
  
  @js.native
  sealed trait switch extends StObject
  inline def switch: switch = "switch".asInstanceOf[switch]
  
  @js.native
  sealed trait symbol extends StObject
  inline def symbol: symbol = "symbol".asInstanceOf[symbol]
  
  @js.native
  sealed trait sync extends StObject
  inline def sync: sync = "sync".asInstanceOf[sync]
  
  @js.native
  sealed trait `synthesis-failed`
    extends StObject
       with SpeechSynthesisErrorCode
  inline def `synthesis-failed`: `synthesis-failed` = "synthesis-failed".asInstanceOf[`synthesis-failed`]
  
  @js.native
  sealed trait `synthesis-unavailable`
    extends StObject
       with SpeechSynthesisErrorCode
  inline def `synthesis-unavailable`: `synthesis-unavailable` = "synthesis-unavailable".asInstanceOf[`synthesis-unavailable`]
  
  @js.native
  sealed trait table
    extends StObject
       with ImportExportKind
  inline def table: table = "table".asInstanceOf[table]
  
  @js.native
  sealed trait tbody extends StObject
  inline def tbody: tbody = "tbody".asInstanceOf[tbody]
  
  @js.native
  sealed trait tcp
    extends StObject
       with RTCIceProtocol
  inline def tcp: tcp = "tcp".asInstanceOf[tcp]
  
  @js.native
  sealed trait td extends StObject
  inline def td: td = "td".asInstanceOf[td]
  
  @js.native
  sealed trait template extends StObject
  inline def template: template = "template".asInstanceOf[template]
  
  @js.native
  sealed trait temporary
    extends StObject
       with MediaKeySessionType
  inline def temporary: temporary = "temporary".asInstanceOf[temporary]
  
  @js.native
  sealed trait text
    extends StObject
       with XMLHttpRequestResponseType
  inline def text: text = "text".asInstanceOf[text]
  
  @js.native
  sealed trait `text-too-long`
    extends StObject
       with SpeechSynthesisErrorCode
  inline def `text-too-long`: `text-too-long` = "text-too-long".asInstanceOf[`text-too-long`]
  
  @js.native
  sealed trait textPath extends StObject
  inline def textPath: textPath = "textPath".asInstanceOf[textPath]
  
  @js.native
  sealed trait textSlashhtml
    extends StObject
       with DOMParserSupportedType
  inline def textSlashhtml: textSlashhtml = "text/html".asInstanceOf[textSlashhtml]
  
  @js.native
  sealed trait textSlashxml
    extends StObject
       with DOMParserSupportedType
  inline def textSlashxml: textSlashxml = "text/xml".asInstanceOf[textSlashxml]
  
  @js.native
  sealed trait textarea extends StObject
  inline def textarea: textarea = "textarea".asInstanceOf[textarea]
  
  @js.native
  sealed trait tfoot extends StObject
  inline def tfoot: tfoot = "tfoot".asInstanceOf[tfoot]
  
  @js.native
  sealed trait th extends StObject
  inline def th: th = "th".asInstanceOf[th]
  
  @js.native
  sealed trait thead extends StObject
  inline def thead: thead = "thead".asInstanceOf[thead]
  
  @js.native
  sealed trait time extends StObject
  inline def time: time = "time".asInstanceOf[time]
  
  @js.native
  sealed trait timeout extends StObject
  inline def timeout: timeout = "timeout".asInstanceOf[timeout]
  
  @js.native
  sealed trait timeupdate extends StObject
  inline def timeupdate: timeupdate = "timeupdate".asInstanceOf[timeupdate]
  
  @js.native
  sealed trait title extends StObject
  inline def title: title = "title".asInstanceOf[title]
  
  @js.native
  sealed trait toggle extends StObject
  inline def toggle: toggle = "toggle".asInstanceOf[toggle]
  
  @js.native
  sealed trait tonechange extends StObject
  inline def tonechange: tonechange = "tonechange".asInstanceOf[tonechange]
  
  @js.native
  sealed trait top
    extends StObject
       with CanvasTextBaseline
  inline def top: top = "top".asInstanceOf[top]
  
  @js.native
  sealed trait touchcancel extends StObject
  inline def touchcancel: touchcancel = "touchcancel".asInstanceOf[touchcancel]
  
  @js.native
  sealed trait touchend extends StObject
  inline def touchend: touchend = "touchend".asInstanceOf[touchend]
  
  @js.native
  sealed trait touchmove extends StObject
  inline def touchmove: touchmove = "touchmove".asInstanceOf[touchmove]
  
  @js.native
  sealed trait touchstart extends StObject
  inline def touchstart: touchstart = "touchstart".asInstanceOf[touchstart]
  
  @js.native
  sealed trait tr extends StObject
  inline def tr: tr = "tr".asInstanceOf[tr]
  
  @js.native
  sealed trait track
    extends StObject
       with RTCStatsType
       with RequestDestination
  inline def track: track = "track".asInstanceOf[track]
  
  @js.native
  sealed trait transceiver
    extends StObject
       with RTCStatsType
  inline def transceiver: transceiver = "transceiver".asInstanceOf[transceiver]
  
  @js.native
  sealed trait transitioncancel extends StObject
  inline def transitioncancel: transitioncancel = "transitioncancel".asInstanceOf[transitioncancel]
  
  @js.native
  sealed trait transitionend extends StObject
  inline def transitionend: transitionend = "transitionend".asInstanceOf[transitionend]
  
  @js.native
  sealed trait transitionrun extends StObject
  inline def transitionrun: transitionrun = "transitionrun".asInstanceOf[transitionrun]
  
  @js.native
  sealed trait transitionstart extends StObject
  inline def transitionstart: transitionstart = "transitionstart".asInstanceOf[transitionstart]
  
  @js.native
  sealed trait transparent
    extends StObject
       with EndingType
  inline def transparent: transparent = "transparent".asInstanceOf[transparent]
  
  @js.native
  sealed trait transport
    extends StObject
       with RTCStatsType
  inline def transport: transport = "transport".asInstanceOf[transport]
  
  @js.native
  sealed trait triangle
    extends StObject
       with OscillatorType
  inline def triangle: triangle = "triangle".asInstanceOf[triangle]
  
  @js.native
  sealed trait tspan extends StObject
  inline def tspan: tspan = "tspan".asInstanceOf[tspan]
  
  @js.native
  sealed trait u extends StObject
  inline def u: u = "u".asInstanceOf[u]
  
  @js.native
  sealed trait udp
    extends StObject
       with RTCIceProtocol
  inline def udp: udp = "udp".asInstanceOf[udp]
  
  @js.native
  sealed trait ul extends StObject
  inline def ul: ul = "ul".asInstanceOf[ul]
  
  @js.native
  sealed trait unhandledrejection extends StObject
  inline def unhandledrejection: unhandledrejection = "unhandledrejection".asInstanceOf[unhandledrejection]
  
  @js.native
  sealed trait uninitialized extends StObject
  inline def uninitialized: uninitialized = "uninitialized".asInstanceOf[uninitialized]
  
  @js.native
  sealed trait unknown
    extends StObject
       with MSWebViewPermissionState
       with PaymentComplete
       with RTCIceRole
  inline def unknown: unknown = "unknown".asInstanceOf[unknown]
  
  @js.native
  sealed trait unlimitedIndexedDBQuota
    extends StObject
       with MSWebViewPermissionType
  inline def unlimitedIndexedDBQuota: unlimitedIndexedDBQuota = "unlimitedIndexedDBQuota".asInstanceOf[unlimitedIndexedDBQuota]
  
  @js.native
  sealed trait unload extends StObject
  inline def unload: unload = "unload".asInstanceOf[unload]
  
  @js.native
  sealed trait unmounted
    extends StObject
       with VRDisplayEventReason
  inline def unmounted: unmounted = "unmounted".asInstanceOf[unmounted]
  
  @js.native
  sealed trait unmute extends StObject
  inline def unmute: unmute = "unmute".asInstanceOf[unmute]
  
  @js.native
  sealed trait `unsafe-url`
    extends StObject
       with ReferrerPolicy
  inline def `unsafe-url`: `unsafe-url` = "unsafe-url".asInstanceOf[`unsafe-url`]
  
  @js.native
  sealed trait unwrapKey
    extends StObject
       with KeyUsage
  inline def unwrapKey: unwrapKey = "unwrapKey".asInstanceOf[unwrapKey]
  
  @js.native
  sealed trait up
    extends StObject
       with NavigationReason
       with ScrollSetting
  inline def up: up = "up".asInstanceOf[up]
  
  @js.native
  sealed trait update extends StObject
  inline def update: update = "update".asInstanceOf[update]
  
  @js.native
  sealed trait updateend extends StObject
  inline def updateend: updateend = "updateend".asInstanceOf[updateend]
  
  @js.native
  sealed trait updatefound extends StObject
  inline def updatefound: updatefound = "updatefound".asInstanceOf[updatefound]
  
  @js.native
  sealed trait updateready extends StObject
  inline def updateready: updateready = "updateready".asInstanceOf[updateready]
  
  @js.native
  sealed trait updatestart extends StObject
  inline def updatestart: updatestart = "updatestart".asInstanceOf[updatestart]
  
  @js.native
  sealed trait upgradeneeded extends StObject
  inline def upgradeneeded: upgradeneeded = "upgradeneeded".asInstanceOf[upgradeneeded]
  
  @js.native
  sealed trait usable
    extends StObject
       with MediaKeyStatus
  inline def usable: usable = "usable".asInstanceOf[usable]
  
  @js.native
  sealed trait usb_
    extends StObject
       with AuthenticatorTransport
  inline def usb_ : usb_ = "usb".asInstanceOf[usb_]
  
  @js.native
  sealed trait use extends StObject
  inline def use: use = "use".asInstanceOf[use]
  
  @js.native
  sealed trait user
    extends StObject
       with VideoFacingModeEnum
  inline def user: user = "user".asInstanceOf[user]
  
  @js.native
  sealed trait `var` extends StObject
  inline def `var`: `var` = "var".asInstanceOf[`var`]
  
  @js.native
  sealed trait verify
    extends StObject
       with KeyUsage
  inline def verify: verify = "verify".asInstanceOf[verify]
  
  @js.native
  sealed trait versionchange
    extends StObject
       with IDBTransactionMode
  inline def versionchange: versionchange = "versionchange".asInstanceOf[versionchange]
  
  @js.native
  sealed trait vibration extends StObject
  inline def vibration: vibration = "vibration".asInstanceOf[vibration]
  
  @js.native
  sealed trait video
    extends StObject
       with RequestDestination
  inline def video: video = "video".asInstanceOf[video]
  
  @js.native
  sealed trait videoinput
    extends StObject
       with MediaDeviceKind
  inline def videoinput: videoinput = "videoinput".asInstanceOf[videoinput]
  
  @js.native
  sealed trait view extends StObject
  inline def view: view = "view".asInstanceOf[view]
  
  @js.native
  sealed trait visibilitychange extends StObject
  inline def visibilitychange: visibilitychange = "visibilitychange".asInstanceOf[visibilitychange]
  
  @js.native
  sealed trait visible
    extends StObject
       with VisibilityState
  inline def visible: visible = "visible".asInstanceOf[visible]
  
  @js.native
  sealed trait `voice-unavailable`
    extends StObject
       with SpeechSynthesisErrorCode
  inline def `voice-unavailable`: `voice-unavailable` = "voice-unavailable".asInstanceOf[`voice-unavailable`]
  
  @js.native
  sealed trait voiceschanged extends StObject
  inline def voiceschanged: voiceschanged = "voiceschanged".asInstanceOf[voiceschanged]
  
  @js.native
  sealed trait volumechange extends StObject
  inline def volumechange: volumechange = "volumechange".asInstanceOf[volumechange]
  
  @js.native
  sealed trait vrdisplayactivate extends StObject
  inline def vrdisplayactivate: vrdisplayactivate = "vrdisplayactivate".asInstanceOf[vrdisplayactivate]
  
  @js.native
  sealed trait vrdisplayblur extends StObject
  inline def vrdisplayblur: vrdisplayblur = "vrdisplayblur".asInstanceOf[vrdisplayblur]
  
  @js.native
  sealed trait vrdisplayconnect extends StObject
  inline def vrdisplayconnect: vrdisplayconnect = "vrdisplayconnect".asInstanceOf[vrdisplayconnect]
  
  @js.native
  sealed trait vrdisplaydeactivate extends StObject
  inline def vrdisplaydeactivate: vrdisplaydeactivate = "vrdisplaydeactivate".asInstanceOf[vrdisplaydeactivate]
  
  @js.native
  sealed trait vrdisplaydisconnect extends StObject
  inline def vrdisplaydisconnect: vrdisplaydisconnect = "vrdisplaydisconnect".asInstanceOf[vrdisplaydisconnect]
  
  @js.native
  sealed trait vrdisplaypresentchange extends StObject
  inline def vrdisplaypresentchange: vrdisplaypresentchange = "vrdisplaypresentchange".asInstanceOf[vrdisplaypresentchange]
  
  @js.native
  sealed trait waiting
    extends StObject
       with RTCStatsIceCandidatePairState
  inline def waiting: waiting = "waiting".asInstanceOf[waiting]
  
  @js.native
  sealed trait waitingforkey extends StObject
  inline def waitingforkey: waitingforkey = "waitingforkey".asInstanceOf[waitingforkey]
  
  @js.native
  sealed trait wbr extends StObject
  inline def wbr: wbr = "wbr".asInstanceOf[wbr]
  
  @js.native
  sealed trait webgl
    extends StObject
       with OffscreenRenderingContextId
  inline def webgl: webgl = "webgl".asInstanceOf[webgl]
  
  @js.native
  sealed trait webgl2
    extends StObject
       with OffscreenRenderingContextId
  inline def webgl2: webgl2 = "webgl2".asInstanceOf[webgl2]
  
  @js.native
  sealed trait webnotifications
    extends StObject
       with MSWebViewPermissionType
  inline def webnotifications: webnotifications = "webnotifications".asInstanceOf[webnotifications]
  
  @js.native
  sealed trait wheel extends StObject
  inline def wheel: wheel = "wheel".asInstanceOf[wheel]
  
  @js.native
  sealed trait window
    extends StObject
       with ClientTypes
       with DisplayCaptureSurfaceType
  inline def window: window = "window".asInstanceOf[window]
  
  @js.native
  sealed trait worker
    extends StObject
       with ClientTypes
       with RequestDestination
  inline def worker: worker = "worker".asInstanceOf[worker]
  
  @js.native
  sealed trait wrapKey
    extends StObject
       with KeyUsage
  inline def wrapKey: wrapKey = "wrapKey".asInstanceOf[wrapKey]
  
  @js.native
  sealed trait xmp extends StObject
  inline def xmp: xmp = "xmp".asInstanceOf[xmp]
  
  @js.native
  sealed trait xslt
    extends StObject
       with RequestDestination
  inline def xslt: xslt = "xslt".asInstanceOf[xslt]
}
