package typings.node.cryptoMod

import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

/* Rewritten from type alias, can be one of: 
  - typings.node.nodeStrings.rsa
  - typings.node.nodeStrings.dsa
  - typings.node.nodeStrings.ec
*/
trait KeyType extends StObject
object KeyType {
  
  inline def dsa: typings.node.nodeStrings.dsa = "dsa".asInstanceOf[typings.node.nodeStrings.dsa]
  
  inline def ec: typings.node.nodeStrings.ec = "ec".asInstanceOf[typings.node.nodeStrings.ec]
  
  inline def rsa: typings.node.nodeStrings.rsa = "rsa".asInstanceOf[typings.node.nodeStrings.rsa]
}
