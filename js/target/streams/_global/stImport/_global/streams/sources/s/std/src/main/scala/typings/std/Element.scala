package typings.std

import typings.std.stdStrings.`object`
import typings.std.stdStrings.`var`
import typings.std.stdStrings.a
import typings.std.stdStrings.abbr
import typings.std.stdStrings.address
import typings.std.stdStrings.applet
import typings.std.stdStrings.area
import typings.std.stdStrings.article
import typings.std.stdStrings.aside
import typings.std.stdStrings.audio
import typings.std.stdStrings.b
import typings.std.stdStrings.base
import typings.std.stdStrings.bdi
import typings.std.stdStrings.bdo
import typings.std.stdStrings.blockquote
import typings.std.stdStrings.body
import typings.std.stdStrings.br
import typings.std.stdStrings.button
import typings.std.stdStrings.canvas
import typings.std.stdStrings.caption
import typings.std.stdStrings.circle
import typings.std.stdStrings.cite
import typings.std.stdStrings.clipPath
import typings.std.stdStrings.code
import typings.std.stdStrings.col
import typings.std.stdStrings.colgroup
import typings.std.stdStrings.data
import typings.std.stdStrings.datalist
import typings.std.stdStrings.dd
import typings.std.stdStrings.defs
import typings.std.stdStrings.del
import typings.std.stdStrings.desc
import typings.std.stdStrings.details
import typings.std.stdStrings.dfn
import typings.std.stdStrings.dialog
import typings.std.stdStrings.dir
import typings.std.stdStrings.div
import typings.std.stdStrings.dl
import typings.std.stdStrings.dt
import typings.std.stdStrings.ellipse
import typings.std.stdStrings.em
import typings.std.stdStrings.embed
import typings.std.stdStrings.feBlend
import typings.std.stdStrings.feColorMatrix
import typings.std.stdStrings.feComponentTransfer
import typings.std.stdStrings.feComposite
import typings.std.stdStrings.feConvolveMatrix
import typings.std.stdStrings.feDiffuseLighting
import typings.std.stdStrings.feDisplacementMap
import typings.std.stdStrings.feDistantLight
import typings.std.stdStrings.feFlood
import typings.std.stdStrings.feFuncA
import typings.std.stdStrings.feFuncB
import typings.std.stdStrings.feFuncG
import typings.std.stdStrings.feFuncR
import typings.std.stdStrings.feGaussianBlur
import typings.std.stdStrings.feImage
import typings.std.stdStrings.feMerge
import typings.std.stdStrings.feMergeNode
import typings.std.stdStrings.feMorphology
import typings.std.stdStrings.feOffset
import typings.std.stdStrings.fePointLight
import typings.std.stdStrings.feSpecularLighting
import typings.std.stdStrings.feSpotLight
import typings.std.stdStrings.feTile
import typings.std.stdStrings.feTurbulence
import typings.std.stdStrings.fieldset
import typings.std.stdStrings.figcaption
import typings.std.stdStrings.figure
import typings.std.stdStrings.filter
import typings.std.stdStrings.font
import typings.std.stdStrings.footer
import typings.std.stdStrings.foreignObject
import typings.std.stdStrings.form
import typings.std.stdStrings.frame
import typings.std.stdStrings.frameset
import typings.std.stdStrings.fullscreenchange
import typings.std.stdStrings.fullscreenerror
import typings.std.stdStrings.g
import typings.std.stdStrings.h1
import typings.std.stdStrings.h2
import typings.std.stdStrings.h3
import typings.std.stdStrings.h4
import typings.std.stdStrings.h5
import typings.std.stdStrings.h6
import typings.std.stdStrings.head
import typings.std.stdStrings.header
import typings.std.stdStrings.hgroup
import typings.std.stdStrings.hr
import typings.std.stdStrings.html
import typings.std.stdStrings.httpColonSlashSlashwwwDotw3DotorgSlash1999Slashxhtml
import typings.std.stdStrings.httpColonSlashSlashwwwDotw3DotorgSlash2000Slashsvg
import typings.std.stdStrings.i
import typings.std.stdStrings.iframe
import typings.std.stdStrings.image
import typings.std.stdStrings.img
import typings.std.stdStrings.input
import typings.std.stdStrings.ins
import typings.std.stdStrings.kbd
import typings.std.stdStrings.label
import typings.std.stdStrings.legend
import typings.std.stdStrings.li
import typings.std.stdStrings.line
import typings.std.stdStrings.linearGradient
import typings.std.stdStrings.link
import typings.std.stdStrings.main
import typings.std.stdStrings.map
import typings.std.stdStrings.mark
import typings.std.stdStrings.marker
import typings.std.stdStrings.marquee
import typings.std.stdStrings.mask
import typings.std.stdStrings.menu
import typings.std.stdStrings.meta
import typings.std.stdStrings.metadata
import typings.std.stdStrings.meter
import typings.std.stdStrings.nav
import typings.std.stdStrings.noscript
import typings.std.stdStrings.ol
import typings.std.stdStrings.optgroup
import typings.std.stdStrings.option
import typings.std.stdStrings.output
import typings.std.stdStrings.p
import typings.std.stdStrings.param
import typings.std.stdStrings.path
import typings.std.stdStrings.pattern
import typings.std.stdStrings.picture
import typings.std.stdStrings.polygon
import typings.std.stdStrings.polyline
import typings.std.stdStrings.pre
import typings.std.stdStrings.progress
import typings.std.stdStrings.q
import typings.std.stdStrings.radialGradient
import typings.std.stdStrings.rect
import typings.std.stdStrings.rp
import typings.std.stdStrings.rt
import typings.std.stdStrings.ruby
import typings.std.stdStrings.s
import typings.std.stdStrings.samp
import typings.std.stdStrings.script
import typings.std.stdStrings.section
import typings.std.stdStrings.select
import typings.std.stdStrings.slot
import typings.std.stdStrings.small
import typings.std.stdStrings.source
import typings.std.stdStrings.span
import typings.std.stdStrings.stop
import typings.std.stdStrings.strong
import typings.std.stdStrings.style
import typings.std.stdStrings.sub
import typings.std.stdStrings.summary
import typings.std.stdStrings.sup
import typings.std.stdStrings.svg
import typings.std.stdStrings.switch
import typings.std.stdStrings.symbol
import typings.std.stdStrings.table
import typings.std.stdStrings.tbody
import typings.std.stdStrings.td
import typings.std.stdStrings.template
import typings.std.stdStrings.text
import typings.std.stdStrings.textPath
import typings.std.stdStrings.textarea
import typings.std.stdStrings.tfoot
import typings.std.stdStrings.th
import typings.std.stdStrings.thead
import typings.std.stdStrings.time
import typings.std.stdStrings.title
import typings.std.stdStrings.tr
import typings.std.stdStrings.track
import typings.std.stdStrings.tspan
import typings.std.stdStrings.u
import typings.std.stdStrings.ul
import typings.std.stdStrings.use
import typings.std.stdStrings.video
import typings.std.stdStrings.view
import typings.std.stdStrings.wbr
import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

/** Element is the most general base class from which all objects in a Document inherit. It only has methods and properties common to all kinds of elements. More specific classes inherit from Element. */
@js.native
trait Element
  extends StObject
     with Animatable
     with ChildNode
     with InnerHTML
     with NonDocumentTypeChildNode
     with ParentNode
     with Slottable {
  
  /* standard dom */
  @JSName("addEventListener")
  def addEventListener_fullscreenchange(
    `type`: fullscreenchange,
    listener: js.ThisFunction1[/* this */ this.type, /* ev */ org.scalajs.dom.Event, Any]
  ): Unit = js.native
  @JSName("addEventListener")
  def addEventListener_fullscreenchange(
    `type`: fullscreenchange,
    listener: js.ThisFunction1[/* this */ this.type, /* ev */ org.scalajs.dom.Event, Any],
    options: scala.Boolean
  ): Unit = js.native
  @JSName("addEventListener")
  def addEventListener_fullscreenchange(
    `type`: fullscreenchange,
    listener: js.ThisFunction1[/* this */ this.type, /* ev */ org.scalajs.dom.Event, Any],
    options: AddEventListenerOptions
  ): Unit = js.native
  @JSName("addEventListener")
  def addEventListener_fullscreenerror(
    `type`: fullscreenerror,
    listener: js.ThisFunction1[/* this */ this.type, /* ev */ org.scalajs.dom.Event, Any]
  ): Unit = js.native
  @JSName("addEventListener")
  def addEventListener_fullscreenerror(
    `type`: fullscreenerror,
    listener: js.ThisFunction1[/* this */ this.type, /* ev */ org.scalajs.dom.Event, Any],
    options: scala.Boolean
  ): Unit = js.native
  @JSName("addEventListener")
  def addEventListener_fullscreenerror(
    `type`: fullscreenerror,
    listener: js.ThisFunction1[/* this */ this.type, /* ev */ org.scalajs.dom.Event, Any],
    options: AddEventListenerOptions
  ): Unit = js.native
  
  /**
    * Creates a shadow root for element and returns it.
    */
  /* standard dom */
  def attachShadow(init: ShadowRootInit): ShadowRoot = js.native
  
  /* standard dom */
  val attributes: org.scalajs.dom.NamedNodeMap = js.native
  
  /**
    * Allows for manipulation of element's class content attribute as a set of whitespace-separated tokens through a DOMTokenList object.
    */
  /* standard dom */
  val classList: org.scalajs.dom.DOMTokenList = js.native
  
  /**
    * Returns the value of element's class content attribute. Can be set to change it.
    */
  /* standard dom */
  var className: java.lang.String = js.native
  
  /* standard dom */
  val clientHeight: Double = js.native
  
  /* standard dom */
  val clientLeft: Double = js.native
  
  /* standard dom */
  val clientTop: Double = js.native
  
  /* standard dom */
  val clientWidth: Double = js.native
  
  /* standard dom */
  def closest[E /* <: org.scalajs.dom.Element */](selector: java.lang.String): E | Null = js.native
  /**
    * Returns the first (starting at element) inclusive ancestor that matches selectors, and null otherwise.
    */
  /* standard dom */
  @JSName("closest")
  def closest_a(selector: a): org.scalajs.dom.HTMLAnchorElement | Null = js.native
  @JSName("closest")
  def closest_abbr(selector: abbr): org.scalajs.dom.HTMLElement | Null = js.native
  @JSName("closest")
  def closest_address(selector: address): org.scalajs.dom.HTMLElement | Null = js.native
  @JSName("closest")
  def closest_applet(selector: applet): HTMLAppletElement | Null = js.native
  @JSName("closest")
  def closest_area(selector: area): org.scalajs.dom.HTMLAreaElement | Null = js.native
  @JSName("closest")
  def closest_article(selector: article): org.scalajs.dom.HTMLElement | Null = js.native
  @JSName("closest")
  def closest_aside(selector: aside): org.scalajs.dom.HTMLElement | Null = js.native
  @JSName("closest")
  def closest_audio(selector: audio): org.scalajs.dom.HTMLAudioElement | Null = js.native
  @JSName("closest")
  def closest_b(selector: b): org.scalajs.dom.HTMLElement | Null = js.native
  @JSName("closest")
  def closest_base(selector: base): org.scalajs.dom.HTMLBaseElement | Null = js.native
  @JSName("closest")
  def closest_bdi(selector: bdi): org.scalajs.dom.HTMLElement | Null = js.native
  @JSName("closest")
  def closest_bdo(selector: bdo): org.scalajs.dom.HTMLElement | Null = js.native
  @JSName("closest")
  def closest_blockquote(selector: blockquote): org.scalajs.dom.HTMLQuoteElement | Null = js.native
  @JSName("closest")
  def closest_body(selector: body): org.scalajs.dom.HTMLBodyElement | Null = js.native
  @JSName("closest")
  def closest_br(selector: br): org.scalajs.dom.HTMLBRElement | Null = js.native
  @JSName("closest")
  def closest_button(selector: button): org.scalajs.dom.HTMLButtonElement | Null = js.native
  @JSName("closest")
  def closest_canvas(selector: canvas): org.scalajs.dom.HTMLCanvasElement | Null = js.native
  @JSName("closest")
  def closest_caption(selector: caption): org.scalajs.dom.HTMLTableCaptionElement | Null = js.native
  @JSName("closest")
  def closest_circle(selector: circle): org.scalajs.dom.SVGCircleElement | Null = js.native
  @JSName("closest")
  def closest_cite(selector: cite): org.scalajs.dom.HTMLElement | Null = js.native
  @JSName("closest")
  def closest_clipPath(selector: clipPath): org.scalajs.dom.SVGClipPathElement | Null = js.native
  @JSName("closest")
  def closest_code(selector: code): org.scalajs.dom.HTMLElement | Null = js.native
  @JSName("closest")
  def closest_col(selector: col): org.scalajs.dom.HTMLTableColElement | Null = js.native
  @JSName("closest")
  def closest_colgroup(selector: colgroup): org.scalajs.dom.HTMLTableColElement | Null = js.native
  @JSName("closest")
  def closest_data(selector: data): HTMLDataElement | Null = js.native
  @JSName("closest")
  def closest_datalist(selector: datalist): org.scalajs.dom.HTMLDataListElement | Null = js.native
  @JSName("closest")
  def closest_dd(selector: dd): org.scalajs.dom.HTMLElement | Null = js.native
  @JSName("closest")
  def closest_defs(selector: defs): org.scalajs.dom.SVGDefsElement | Null = js.native
  @JSName("closest")
  def closest_del(selector: del): org.scalajs.dom.HTMLModElement | Null = js.native
  @JSName("closest")
  def closest_desc(selector: desc): org.scalajs.dom.SVGDescElement | Null = js.native
  @JSName("closest")
  def closest_details(selector: details): HTMLDetailsElement | Null = js.native
  @JSName("closest")
  def closest_dfn(selector: dfn): org.scalajs.dom.HTMLElement | Null = js.native
  @JSName("closest")
  def closest_dialog(selector: dialog): HTMLDialogElement | Null = js.native
  @JSName("closest")
  def closest_dir(selector: dir): HTMLDirectoryElement | Null = js.native
  @JSName("closest")
  def closest_div(selector: div): org.scalajs.dom.HTMLDivElement | Null = js.native
  @JSName("closest")
  def closest_dl(selector: dl): org.scalajs.dom.HTMLDListElement | Null = js.native
  @JSName("closest")
  def closest_dt(selector: dt): org.scalajs.dom.HTMLElement | Null = js.native
  @JSName("closest")
  def closest_ellipse(selector: ellipse): org.scalajs.dom.SVGEllipseElement | Null = js.native
  @JSName("closest")
  def closest_em(selector: em): org.scalajs.dom.HTMLElement | Null = js.native
  @JSName("closest")
  def closest_embed(selector: embed): org.scalajs.dom.HTMLEmbedElement | Null = js.native
  @JSName("closest")
  def closest_feBlend(selector: feBlend): org.scalajs.dom.SVGFEBlendElement | Null = js.native
  @JSName("closest")
  def closest_feColorMatrix(selector: feColorMatrix): org.scalajs.dom.SVGFEColorMatrixElement | Null = js.native
  @JSName("closest")
  def closest_feComponentTransfer(selector: feComponentTransfer): org.scalajs.dom.SVGFEComponentTransferElement | Null = js.native
  @JSName("closest")
  def closest_feComposite(selector: feComposite): org.scalajs.dom.SVGFECompositeElement | Null = js.native
  @JSName("closest")
  def closest_feConvolveMatrix(selector: feConvolveMatrix): org.scalajs.dom.SVGFEConvolveMatrixElement | Null = js.native
  @JSName("closest")
  def closest_feDiffuseLighting(selector: feDiffuseLighting): org.scalajs.dom.SVGFEDiffuseLightingElement | Null = js.native
  @JSName("closest")
  def closest_feDisplacementMap(selector: feDisplacementMap): org.scalajs.dom.SVGFEDisplacementMapElement | Null = js.native
  @JSName("closest")
  def closest_feDistantLight(selector: feDistantLight): org.scalajs.dom.SVGFEDistantLightElement | Null = js.native
  @JSName("closest")
  def closest_feFlood(selector: feFlood): org.scalajs.dom.SVGFEFloodElement | Null = js.native
  @JSName("closest")
  def closest_feFuncA(selector: feFuncA): org.scalajs.dom.SVGFEFuncAElement | Null = js.native
  @JSName("closest")
  def closest_feFuncB(selector: feFuncB): org.scalajs.dom.SVGFEFuncBElement | Null = js.native
  @JSName("closest")
  def closest_feFuncG(selector: feFuncG): org.scalajs.dom.SVGFEFuncGElement | Null = js.native
  @JSName("closest")
  def closest_feFuncR(selector: feFuncR): org.scalajs.dom.SVGFEFuncRElement | Null = js.native
  @JSName("closest")
  def closest_feGaussianBlur(selector: feGaussianBlur): org.scalajs.dom.SVGFEGaussianBlurElement | Null = js.native
  @JSName("closest")
  def closest_feImage(selector: feImage): org.scalajs.dom.SVGFEImageElement | Null = js.native
  @JSName("closest")
  def closest_feMerge(selector: feMerge): org.scalajs.dom.SVGFEMergeElement | Null = js.native
  @JSName("closest")
  def closest_feMergeNode(selector: feMergeNode): org.scalajs.dom.SVGFEMergeNodeElement | Null = js.native
  @JSName("closest")
  def closest_feMorphology(selector: feMorphology): org.scalajs.dom.SVGFEMorphologyElement | Null = js.native
  @JSName("closest")
  def closest_feOffset(selector: feOffset): org.scalajs.dom.SVGFEOffsetElement | Null = js.native
  @JSName("closest")
  def closest_fePointLight(selector: fePointLight): org.scalajs.dom.SVGFEPointLightElement | Null = js.native
  @JSName("closest")
  def closest_feSpecularLighting(selector: feSpecularLighting): org.scalajs.dom.SVGFESpecularLightingElement | Null = js.native
  @JSName("closest")
  def closest_feSpotLight(selector: feSpotLight): org.scalajs.dom.SVGFESpotLightElement | Null = js.native
  @JSName("closest")
  def closest_feTile(selector: feTile): org.scalajs.dom.SVGFETileElement | Null = js.native
  @JSName("closest")
  def closest_feTurbulence(selector: feTurbulence): org.scalajs.dom.SVGFETurbulenceElement | Null = js.native
  @JSName("closest")
  def closest_fieldset(selector: fieldset): org.scalajs.dom.HTMLFieldSetElement | Null = js.native
  @JSName("closest")
  def closest_figcaption(selector: figcaption): org.scalajs.dom.HTMLElement | Null = js.native
  @JSName("closest")
  def closest_figure(selector: figure): org.scalajs.dom.HTMLElement | Null = js.native
  @JSName("closest")
  def closest_filter(selector: filter): org.scalajs.dom.SVGFilterElement | Null = js.native
  @JSName("closest")
  def closest_font(selector: font): HTMLFontElement | Null = js.native
  @JSName("closest")
  def closest_footer(selector: footer): org.scalajs.dom.HTMLElement | Null = js.native
  @JSName("closest")
  def closest_foreignObject(selector: foreignObject): SVGForeignObjectElement | Null = js.native
  @JSName("closest")
  def closest_form(selector: form): org.scalajs.dom.HTMLFormElement | Null = js.native
  @JSName("closest")
  def closest_frame(selector: frame): HTMLFrameElement | Null = js.native
  @JSName("closest")
  def closest_frameset(selector: frameset): HTMLFrameSetElement | Null = js.native
  @JSName("closest")
  def closest_g(selector: g): org.scalajs.dom.SVGGElement | Null = js.native
  @JSName("closest")
  def closest_h1(selector: h1): org.scalajs.dom.HTMLHeadingElement | Null = js.native
  @JSName("closest")
  def closest_h2(selector: h2): org.scalajs.dom.HTMLHeadingElement | Null = js.native
  @JSName("closest")
  def closest_h3(selector: h3): org.scalajs.dom.HTMLHeadingElement | Null = js.native
  @JSName("closest")
  def closest_h4(selector: h4): org.scalajs.dom.HTMLHeadingElement | Null = js.native
  @JSName("closest")
  def closest_h5(selector: h5): org.scalajs.dom.HTMLHeadingElement | Null = js.native
  @JSName("closest")
  def closest_h6(selector: h6): org.scalajs.dom.HTMLHeadingElement | Null = js.native
  @JSName("closest")
  def closest_head(selector: head): org.scalajs.dom.HTMLHeadElement | Null = js.native
  @JSName("closest")
  def closest_header(selector: header): org.scalajs.dom.HTMLElement | Null = js.native
  @JSName("closest")
  def closest_hgroup(selector: hgroup): org.scalajs.dom.HTMLElement | Null = js.native
  @JSName("closest")
  def closest_hr(selector: hr): org.scalajs.dom.HTMLHRElement | Null = js.native
  @JSName("closest")
  def closest_html(selector: html): org.scalajs.dom.HTMLHtmlElement | Null = js.native
  @JSName("closest")
  def closest_i(selector: i): org.scalajs.dom.HTMLElement | Null = js.native
  @JSName("closest")
  def closest_iframe(selector: iframe): org.scalajs.dom.HTMLIFrameElement | Null = js.native
  @JSName("closest")
  def closest_image(selector: image): org.scalajs.dom.SVGImageElement | Null = js.native
  @JSName("closest")
  def closest_img(selector: img): org.scalajs.dom.HTMLImageElement | Null = js.native
  @JSName("closest")
  def closest_input(selector: input): org.scalajs.dom.HTMLInputElement | Null = js.native
  @JSName("closest")
  def closest_ins(selector: ins): org.scalajs.dom.HTMLModElement | Null = js.native
  @JSName("closest")
  def closest_kbd(selector: kbd): org.scalajs.dom.HTMLElement | Null = js.native
  @JSName("closest")
  def closest_label(selector: label): org.scalajs.dom.HTMLLabelElement | Null = js.native
  @JSName("closest")
  def closest_legend(selector: legend): org.scalajs.dom.HTMLLegendElement | Null = js.native
  @JSName("closest")
  def closest_li(selector: li): org.scalajs.dom.HTMLLIElement | Null = js.native
  @JSName("closest")
  def closest_line(selector: line): org.scalajs.dom.SVGLineElement | Null = js.native
  @JSName("closest")
  def closest_linearGradient(selector: linearGradient): org.scalajs.dom.SVGLinearGradientElement | Null = js.native
  @JSName("closest")
  def closest_link(selector: link): org.scalajs.dom.HTMLLinkElement | Null = js.native
  @JSName("closest")
  def closest_main(selector: main): org.scalajs.dom.HTMLElement | Null = js.native
  @JSName("closest")
  def closest_map(selector: map): org.scalajs.dom.HTMLMapElement | Null = js.native
  @JSName("closest")
  def closest_mark(selector: mark): org.scalajs.dom.HTMLElement | Null = js.native
  @JSName("closest")
  def closest_marker(selector: marker): org.scalajs.dom.SVGMarkerElement | Null = js.native
  @JSName("closest")
  def closest_marquee(selector: marquee): HTMLMarqueeElement | Null = js.native
  @JSName("closest")
  def closest_mask(selector: mask): org.scalajs.dom.SVGMaskElement | Null = js.native
  @JSName("closest")
  def closest_menu(selector: menu): org.scalajs.dom.HTMLMenuElement | Null = js.native
  @JSName("closest")
  def closest_meta(selector: meta): org.scalajs.dom.HTMLMetaElement | Null = js.native
  @JSName("closest")
  def closest_metadata(selector: metadata): org.scalajs.dom.SVGMetadataElement | Null = js.native
  @JSName("closest")
  def closest_meter(selector: meter): HTMLMeterElement | Null = js.native
  @JSName("closest")
  def closest_nav(selector: nav): org.scalajs.dom.HTMLElement | Null = js.native
  @JSName("closest")
  def closest_noscript(selector: noscript): org.scalajs.dom.HTMLElement | Null = js.native
  @JSName("closest")
  def closest_object(selector: `object`): org.scalajs.dom.HTMLObjectElement | Null = js.native
  @JSName("closest")
  def closest_ol(selector: ol): org.scalajs.dom.HTMLOListElement | Null = js.native
  @JSName("closest")
  def closest_optgroup(selector: optgroup): org.scalajs.dom.HTMLOptGroupElement | Null = js.native
  @JSName("closest")
  def closest_option(selector: option): org.scalajs.dom.HTMLOptionElement | Null = js.native
  @JSName("closest")
  def closest_output(selector: output): HTMLOutputElement | Null = js.native
  @JSName("closest")
  def closest_p(selector: p): org.scalajs.dom.HTMLParagraphElement | Null = js.native
  @JSName("closest")
  def closest_param(selector: param): org.scalajs.dom.HTMLParamElement | Null = js.native
  @JSName("closest")
  def closest_path(selector: path): org.scalajs.dom.SVGPathElement | Null = js.native
  @JSName("closest")
  def closest_pattern(selector: pattern): org.scalajs.dom.SVGPatternElement | Null = js.native
  @JSName("closest")
  def closest_picture(selector: picture): HTMLPictureElement | Null = js.native
  @JSName("closest")
  def closest_polygon(selector: polygon): org.scalajs.dom.SVGPolygonElement | Null = js.native
  @JSName("closest")
  def closest_polyline(selector: polyline): org.scalajs.dom.SVGPolylineElement | Null = js.native
  @JSName("closest")
  def closest_pre(selector: pre): org.scalajs.dom.HTMLPreElement | Null = js.native
  @JSName("closest")
  def closest_progress(selector: progress): org.scalajs.dom.HTMLProgressElement | Null = js.native
  @JSName("closest")
  def closest_q(selector: q): org.scalajs.dom.HTMLQuoteElement | Null = js.native
  @JSName("closest")
  def closest_radialGradient(selector: radialGradient): org.scalajs.dom.SVGRadialGradientElement | Null = js.native
  @JSName("closest")
  def closest_rect(selector: rect): org.scalajs.dom.SVGRectElement | Null = js.native
  @JSName("closest")
  def closest_rp(selector: rp): org.scalajs.dom.HTMLElement | Null = js.native
  @JSName("closest")
  def closest_rt(selector: rt): org.scalajs.dom.HTMLElement | Null = js.native
  @JSName("closest")
  def closest_ruby(selector: ruby): org.scalajs.dom.HTMLElement | Null = js.native
  @JSName("closest")
  def closest_s(selector: s): org.scalajs.dom.HTMLElement | Null = js.native
  @JSName("closest")
  def closest_samp(selector: samp): org.scalajs.dom.HTMLElement | Null = js.native
  @JSName("closest")
  def closest_script(selector: script): org.scalajs.dom.HTMLScriptElement | Null = js.native
  @JSName("closest")
  def closest_section(selector: section): org.scalajs.dom.HTMLElement | Null = js.native
  @JSName("closest")
  def closest_select(selector: select): org.scalajs.dom.HTMLSelectElement | Null = js.native
  @JSName("closest")
  def closest_slot(selector: slot): HTMLSlotElement | Null = js.native
  @JSName("closest")
  def closest_small(selector: small): org.scalajs.dom.HTMLElement | Null = js.native
  @JSName("closest")
  def closest_source(selector: source): org.scalajs.dom.HTMLSourceElement | Null = js.native
  @JSName("closest")
  def closest_span(selector: span): org.scalajs.dom.HTMLSpanElement | Null = js.native
  @JSName("closest")
  def closest_stop(selector: stop): org.scalajs.dom.SVGStopElement | Null = js.native
  @JSName("closest")
  def closest_strong(selector: strong): org.scalajs.dom.HTMLElement | Null = js.native
  @JSName("closest")
  def closest_style(selector: style): org.scalajs.dom.HTMLStyleElement | Null = js.native
  @JSName("closest")
  def closest_sub(selector: sub): org.scalajs.dom.HTMLElement | Null = js.native
  @JSName("closest")
  def closest_summary(selector: summary): org.scalajs.dom.HTMLElement | Null = js.native
  @JSName("closest")
  def closest_sup(selector: sup): org.scalajs.dom.HTMLElement | Null = js.native
  @JSName("closest")
  def closest_svg(selector: svg): org.scalajs.dom.SVGSVGElement | Null = js.native
  @JSName("closest")
  def closest_switch(selector: switch): org.scalajs.dom.SVGSwitchElement | Null = js.native
  @JSName("closest")
  def closest_symbol(selector: symbol): org.scalajs.dom.SVGSymbolElement | Null = js.native
  @JSName("closest")
  def closest_table(selector: table): org.scalajs.dom.HTMLTableElement | Null = js.native
  @JSName("closest")
  def closest_tbody(selector: tbody): org.scalajs.dom.HTMLTableSectionElement | Null = js.native
  @JSName("closest")
  def closest_td(selector: td): HTMLTableDataCellElement | Null = js.native
  @JSName("closest")
  def closest_template(selector: template): HTMLTemplateElement | Null = js.native
  @JSName("closest")
  def closest_text(selector: text): org.scalajs.dom.SVGTextElement | Null = js.native
  @JSName("closest")
  def closest_textPath(selector: textPath): org.scalajs.dom.SVGTextPathElement | Null = js.native
  @JSName("closest")
  def closest_textarea(selector: textarea): org.scalajs.dom.HTMLTextAreaElement | Null = js.native
  @JSName("closest")
  def closest_tfoot(selector: tfoot): org.scalajs.dom.HTMLTableSectionElement | Null = js.native
  @JSName("closest")
  def closest_th(selector: th): HTMLTableHeaderCellElement | Null = js.native
  @JSName("closest")
  def closest_thead(selector: thead): org.scalajs.dom.HTMLTableSectionElement | Null = js.native
  @JSName("closest")
  def closest_time(selector: time): HTMLTimeElement | Null = js.native
  @JSName("closest")
  def closest_title(selector: title): org.scalajs.dom.HTMLTitleElement | Null = js.native
  @JSName("closest")
  def closest_tr(selector: tr): org.scalajs.dom.HTMLTableRowElement | Null = js.native
  @JSName("closest")
  def closest_track(selector: track): org.scalajs.dom.HTMLTrackElement | Null = js.native
  @JSName("closest")
  def closest_tspan(selector: tspan): org.scalajs.dom.SVGTSpanElement | Null = js.native
  @JSName("closest")
  def closest_u(selector: u): org.scalajs.dom.HTMLElement | Null = js.native
  @JSName("closest")
  def closest_ul(selector: ul): org.scalajs.dom.HTMLUListElement | Null = js.native
  @JSName("closest")
  def closest_use(selector: use): org.scalajs.dom.SVGUseElement | Null = js.native
  @JSName("closest")
  def closest_var(selector: `var`): org.scalajs.dom.HTMLElement | Null = js.native
  @JSName("closest")
  def closest_video(selector: video): org.scalajs.dom.HTMLVideoElement | Null = js.native
  @JSName("closest")
  def closest_view(selector: view): org.scalajs.dom.SVGViewElement | Null = js.native
  @JSName("closest")
  def closest_wbr(selector: wbr): org.scalajs.dom.HTMLElement | Null = js.native
  
  /**
    * Returns element's first attribute whose qualified name is qualifiedName, and null if there is no such attribute otherwise.
    */
  /* standard dom */
  def getAttribute(qualifiedName: java.lang.String): java.lang.String | Null = js.native
  
  /**
    * Returns element's attribute whose namespace is namespace and local name is localName, and null if there is no such attribute otherwise.
    */
  /* standard dom */
  def getAttributeNS(namespace: java.lang.String, localName: java.lang.String): java.lang.String | Null = js.native
  def getAttributeNS(namespace: Null, localName: java.lang.String): java.lang.String | Null = js.native
  
  /**
    * Returns the qualified names of all element's attributes. Can contain duplicates.
    */
  /* standard dom */
  def getAttributeNames(): js.Array[java.lang.String] = js.native
  
  /* standard dom */
  def getAttributeNode(qualifiedName: java.lang.String): org.scalajs.dom.Attr | Null = js.native
  
  /* standard dom */
  def getAttributeNodeNS(namespace: java.lang.String, localName: java.lang.String): org.scalajs.dom.Attr | Null = js.native
  def getAttributeNodeNS(namespace: Null, localName: java.lang.String): org.scalajs.dom.Attr | Null = js.native
  
  /* standard dom */
  def getBoundingClientRect(): org.scalajs.dom.DOMRect = js.native
  
  /* standard dom */
  def getClientRects(): DOMRectList = js.native
  
  /**
    * Returns a HTMLCollection of the elements in the object on which the method was invoked (a document or an element) that have all the classes given by classNames. The classNames argument is interpreted as a space-separated list of classes.
    */
  /* standard dom */
  def getElementsByClassName(classNames: java.lang.String): HTMLCollectionOf[org.scalajs.dom.Element] = js.native
  
  /* standard dom */
  def getElementsByTagName(qualifiedName: java.lang.String): HTMLCollectionOf[org.scalajs.dom.Element] = js.native
  
  /* standard dom */
  def getElementsByTagNameNS(namespaceURI: java.lang.String, localName: java.lang.String): HTMLCollectionOf[org.scalajs.dom.Element] = js.native
  /* standard dom */
  @JSName("getElementsByTagNameNS")
  def getElementsByTagNameNS_httpwwww3org1999xhtml(namespaceURI: httpColonSlashSlashwwwDotw3DotorgSlash1999Slashxhtml, localName: java.lang.String): HTMLCollectionOf[org.scalajs.dom.HTMLElement] = js.native
  /* standard dom */
  @JSName("getElementsByTagNameNS")
  def getElementsByTagNameNS_httpwwww3org2000svg(namespaceURI: httpColonSlashSlashwwwDotw3DotorgSlash2000Slashsvg, localName: java.lang.String): HTMLCollectionOf[org.scalajs.dom.SVGElement] = js.native
  
  /* standard dom */
  @JSName("getElementsByTagName")
  def getElementsByTagName_a(qualifiedName: a): HTMLCollectionOf[org.scalajs.dom.HTMLAnchorElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_abbr(qualifiedName: abbr): HTMLCollectionOf[org.scalajs.dom.HTMLElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_address(qualifiedName: address): HTMLCollectionOf[org.scalajs.dom.HTMLElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_applet(qualifiedName: applet): HTMLCollectionOf[HTMLAppletElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_area(qualifiedName: area): HTMLCollectionOf[org.scalajs.dom.HTMLAreaElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_article(qualifiedName: article): HTMLCollectionOf[org.scalajs.dom.HTMLElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_aside(qualifiedName: aside): HTMLCollectionOf[org.scalajs.dom.HTMLElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_audio(qualifiedName: audio): HTMLCollectionOf[org.scalajs.dom.HTMLAudioElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_b(qualifiedName: b): HTMLCollectionOf[org.scalajs.dom.HTMLElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_base(qualifiedName: base): HTMLCollectionOf[org.scalajs.dom.HTMLBaseElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_bdi(qualifiedName: bdi): HTMLCollectionOf[org.scalajs.dom.HTMLElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_bdo(qualifiedName: bdo): HTMLCollectionOf[org.scalajs.dom.HTMLElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_blockquote(qualifiedName: blockquote): HTMLCollectionOf[org.scalajs.dom.HTMLQuoteElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_body(qualifiedName: body): HTMLCollectionOf[org.scalajs.dom.HTMLBodyElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_br(qualifiedName: br): HTMLCollectionOf[org.scalajs.dom.HTMLBRElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_button(qualifiedName: button): HTMLCollectionOf[org.scalajs.dom.HTMLButtonElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_canvas(qualifiedName: canvas): HTMLCollectionOf[org.scalajs.dom.HTMLCanvasElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_caption(qualifiedName: caption): HTMLCollectionOf[org.scalajs.dom.HTMLTableCaptionElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_circle(qualifiedName: circle): HTMLCollectionOf[org.scalajs.dom.SVGCircleElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_cite(qualifiedName: cite): HTMLCollectionOf[org.scalajs.dom.HTMLElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_clipPath(qualifiedName: clipPath): HTMLCollectionOf[org.scalajs.dom.SVGClipPathElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_code(qualifiedName: code): HTMLCollectionOf[org.scalajs.dom.HTMLElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_col(qualifiedName: col): HTMLCollectionOf[org.scalajs.dom.HTMLTableColElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_colgroup(qualifiedName: colgroup): HTMLCollectionOf[org.scalajs.dom.HTMLTableColElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_data(qualifiedName: data): HTMLCollectionOf[HTMLDataElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_datalist(qualifiedName: datalist): HTMLCollectionOf[org.scalajs.dom.HTMLDataListElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_dd(qualifiedName: dd): HTMLCollectionOf[org.scalajs.dom.HTMLElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_defs(qualifiedName: defs): HTMLCollectionOf[org.scalajs.dom.SVGDefsElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_del(qualifiedName: del): HTMLCollectionOf[org.scalajs.dom.HTMLModElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_desc(qualifiedName: desc): HTMLCollectionOf[org.scalajs.dom.SVGDescElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_details(qualifiedName: details): HTMLCollectionOf[HTMLDetailsElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_dfn(qualifiedName: dfn): HTMLCollectionOf[org.scalajs.dom.HTMLElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_dialog(qualifiedName: dialog): HTMLCollectionOf[HTMLDialogElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_dir(qualifiedName: dir): HTMLCollectionOf[HTMLDirectoryElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_div(qualifiedName: div): HTMLCollectionOf[org.scalajs.dom.HTMLDivElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_dl(qualifiedName: dl): HTMLCollectionOf[org.scalajs.dom.HTMLDListElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_dt(qualifiedName: dt): HTMLCollectionOf[org.scalajs.dom.HTMLElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_ellipse(qualifiedName: ellipse): HTMLCollectionOf[org.scalajs.dom.SVGEllipseElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_em(qualifiedName: em): HTMLCollectionOf[org.scalajs.dom.HTMLElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_embed(qualifiedName: embed): HTMLCollectionOf[org.scalajs.dom.HTMLEmbedElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_feBlend(qualifiedName: feBlend): HTMLCollectionOf[org.scalajs.dom.SVGFEBlendElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_feColorMatrix(qualifiedName: feColorMatrix): HTMLCollectionOf[org.scalajs.dom.SVGFEColorMatrixElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_feComponentTransfer(qualifiedName: feComponentTransfer): HTMLCollectionOf[org.scalajs.dom.SVGFEComponentTransferElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_feComposite(qualifiedName: feComposite): HTMLCollectionOf[org.scalajs.dom.SVGFECompositeElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_feConvolveMatrix(qualifiedName: feConvolveMatrix): HTMLCollectionOf[org.scalajs.dom.SVGFEConvolveMatrixElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_feDiffuseLighting(qualifiedName: feDiffuseLighting): HTMLCollectionOf[org.scalajs.dom.SVGFEDiffuseLightingElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_feDisplacementMap(qualifiedName: feDisplacementMap): HTMLCollectionOf[org.scalajs.dom.SVGFEDisplacementMapElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_feDistantLight(qualifiedName: feDistantLight): HTMLCollectionOf[org.scalajs.dom.SVGFEDistantLightElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_feFlood(qualifiedName: feFlood): HTMLCollectionOf[org.scalajs.dom.SVGFEFloodElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_feFuncA(qualifiedName: feFuncA): HTMLCollectionOf[org.scalajs.dom.SVGFEFuncAElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_feFuncB(qualifiedName: feFuncB): HTMLCollectionOf[org.scalajs.dom.SVGFEFuncBElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_feFuncG(qualifiedName: feFuncG): HTMLCollectionOf[org.scalajs.dom.SVGFEFuncGElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_feFuncR(qualifiedName: feFuncR): HTMLCollectionOf[org.scalajs.dom.SVGFEFuncRElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_feGaussianBlur(qualifiedName: feGaussianBlur): HTMLCollectionOf[org.scalajs.dom.SVGFEGaussianBlurElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_feImage(qualifiedName: feImage): HTMLCollectionOf[org.scalajs.dom.SVGFEImageElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_feMerge(qualifiedName: feMerge): HTMLCollectionOf[org.scalajs.dom.SVGFEMergeElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_feMergeNode(qualifiedName: feMergeNode): HTMLCollectionOf[org.scalajs.dom.SVGFEMergeNodeElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_feMorphology(qualifiedName: feMorphology): HTMLCollectionOf[org.scalajs.dom.SVGFEMorphologyElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_feOffset(qualifiedName: feOffset): HTMLCollectionOf[org.scalajs.dom.SVGFEOffsetElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_fePointLight(qualifiedName: fePointLight): HTMLCollectionOf[org.scalajs.dom.SVGFEPointLightElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_feSpecularLighting(qualifiedName: feSpecularLighting): HTMLCollectionOf[org.scalajs.dom.SVGFESpecularLightingElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_feSpotLight(qualifiedName: feSpotLight): HTMLCollectionOf[org.scalajs.dom.SVGFESpotLightElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_feTile(qualifiedName: feTile): HTMLCollectionOf[org.scalajs.dom.SVGFETileElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_feTurbulence(qualifiedName: feTurbulence): HTMLCollectionOf[org.scalajs.dom.SVGFETurbulenceElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_fieldset(qualifiedName: fieldset): HTMLCollectionOf[org.scalajs.dom.HTMLFieldSetElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_figcaption(qualifiedName: figcaption): HTMLCollectionOf[org.scalajs.dom.HTMLElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_figure(qualifiedName: figure): HTMLCollectionOf[org.scalajs.dom.HTMLElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_filter(qualifiedName: filter): HTMLCollectionOf[org.scalajs.dom.SVGFilterElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_font(qualifiedName: font): HTMLCollectionOf[HTMLFontElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_footer(qualifiedName: footer): HTMLCollectionOf[org.scalajs.dom.HTMLElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_foreignObject(qualifiedName: foreignObject): HTMLCollectionOf[SVGForeignObjectElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_form(qualifiedName: form): HTMLCollectionOf[org.scalajs.dom.HTMLFormElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_frame(qualifiedName: frame): HTMLCollectionOf[HTMLFrameElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_frameset(qualifiedName: frameset): HTMLCollectionOf[HTMLFrameSetElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_g(qualifiedName: g): HTMLCollectionOf[org.scalajs.dom.SVGGElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_h1(qualifiedName: h1): HTMLCollectionOf[org.scalajs.dom.HTMLHeadingElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_h2(qualifiedName: h2): HTMLCollectionOf[org.scalajs.dom.HTMLHeadingElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_h3(qualifiedName: h3): HTMLCollectionOf[org.scalajs.dom.HTMLHeadingElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_h4(qualifiedName: h4): HTMLCollectionOf[org.scalajs.dom.HTMLHeadingElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_h5(qualifiedName: h5): HTMLCollectionOf[org.scalajs.dom.HTMLHeadingElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_h6(qualifiedName: h6): HTMLCollectionOf[org.scalajs.dom.HTMLHeadingElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_head(qualifiedName: head): HTMLCollectionOf[org.scalajs.dom.HTMLHeadElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_header(qualifiedName: header): HTMLCollectionOf[org.scalajs.dom.HTMLElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_hgroup(qualifiedName: hgroup): HTMLCollectionOf[org.scalajs.dom.HTMLElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_hr(qualifiedName: hr): HTMLCollectionOf[org.scalajs.dom.HTMLHRElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_html(qualifiedName: html): HTMLCollectionOf[org.scalajs.dom.HTMLHtmlElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_i(qualifiedName: i): HTMLCollectionOf[org.scalajs.dom.HTMLElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_iframe(qualifiedName: iframe): HTMLCollectionOf[org.scalajs.dom.HTMLIFrameElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_image(qualifiedName: image): HTMLCollectionOf[org.scalajs.dom.SVGImageElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_img(qualifiedName: img): HTMLCollectionOf[org.scalajs.dom.HTMLImageElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_input(qualifiedName: input): HTMLCollectionOf[org.scalajs.dom.HTMLInputElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_ins(qualifiedName: ins): HTMLCollectionOf[org.scalajs.dom.HTMLModElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_kbd(qualifiedName: kbd): HTMLCollectionOf[org.scalajs.dom.HTMLElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_label(qualifiedName: label): HTMLCollectionOf[org.scalajs.dom.HTMLLabelElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_legend(qualifiedName: legend): HTMLCollectionOf[org.scalajs.dom.HTMLLegendElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_li(qualifiedName: li): HTMLCollectionOf[org.scalajs.dom.HTMLLIElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_line(qualifiedName: line): HTMLCollectionOf[org.scalajs.dom.SVGLineElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_linearGradient(qualifiedName: linearGradient): HTMLCollectionOf[org.scalajs.dom.SVGLinearGradientElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_link(qualifiedName: link): HTMLCollectionOf[org.scalajs.dom.HTMLLinkElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_main(qualifiedName: main): HTMLCollectionOf[org.scalajs.dom.HTMLElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_map(qualifiedName: map): HTMLCollectionOf[org.scalajs.dom.HTMLMapElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_mark(qualifiedName: mark): HTMLCollectionOf[org.scalajs.dom.HTMLElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_marker(qualifiedName: marker): HTMLCollectionOf[org.scalajs.dom.SVGMarkerElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_marquee(qualifiedName: marquee): HTMLCollectionOf[HTMLMarqueeElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_mask(qualifiedName: mask): HTMLCollectionOf[org.scalajs.dom.SVGMaskElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_menu(qualifiedName: menu): HTMLCollectionOf[org.scalajs.dom.HTMLMenuElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_meta(qualifiedName: meta): HTMLCollectionOf[org.scalajs.dom.HTMLMetaElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_metadata(qualifiedName: metadata): HTMLCollectionOf[org.scalajs.dom.SVGMetadataElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_meter(qualifiedName: meter): HTMLCollectionOf[HTMLMeterElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_nav(qualifiedName: nav): HTMLCollectionOf[org.scalajs.dom.HTMLElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_noscript(qualifiedName: noscript): HTMLCollectionOf[org.scalajs.dom.HTMLElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_object(qualifiedName: `object`): HTMLCollectionOf[org.scalajs.dom.HTMLObjectElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_ol(qualifiedName: ol): HTMLCollectionOf[org.scalajs.dom.HTMLOListElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_optgroup(qualifiedName: optgroup): HTMLCollectionOf[org.scalajs.dom.HTMLOptGroupElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_option(qualifiedName: option): HTMLCollectionOf[org.scalajs.dom.HTMLOptionElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_output(qualifiedName: output): HTMLCollectionOf[HTMLOutputElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_p(qualifiedName: p): HTMLCollectionOf[org.scalajs.dom.HTMLParagraphElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_param(qualifiedName: param): HTMLCollectionOf[org.scalajs.dom.HTMLParamElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_path(qualifiedName: path): HTMLCollectionOf[org.scalajs.dom.SVGPathElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_pattern(qualifiedName: pattern): HTMLCollectionOf[org.scalajs.dom.SVGPatternElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_picture(qualifiedName: picture): HTMLCollectionOf[HTMLPictureElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_polygon(qualifiedName: polygon): HTMLCollectionOf[org.scalajs.dom.SVGPolygonElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_polyline(qualifiedName: polyline): HTMLCollectionOf[org.scalajs.dom.SVGPolylineElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_pre(qualifiedName: pre): HTMLCollectionOf[org.scalajs.dom.HTMLPreElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_progress(qualifiedName: progress): HTMLCollectionOf[org.scalajs.dom.HTMLProgressElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_q(qualifiedName: q): HTMLCollectionOf[org.scalajs.dom.HTMLQuoteElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_radialGradient(qualifiedName: radialGradient): HTMLCollectionOf[org.scalajs.dom.SVGRadialGradientElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_rect(qualifiedName: rect): HTMLCollectionOf[org.scalajs.dom.SVGRectElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_rp(qualifiedName: rp): HTMLCollectionOf[org.scalajs.dom.HTMLElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_rt(qualifiedName: rt): HTMLCollectionOf[org.scalajs.dom.HTMLElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_ruby(qualifiedName: ruby): HTMLCollectionOf[org.scalajs.dom.HTMLElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_s(qualifiedName: s): HTMLCollectionOf[org.scalajs.dom.HTMLElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_samp(qualifiedName: samp): HTMLCollectionOf[org.scalajs.dom.HTMLElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_script(qualifiedName: script): HTMLCollectionOf[org.scalajs.dom.HTMLScriptElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_section(qualifiedName: section): HTMLCollectionOf[org.scalajs.dom.HTMLElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_select(qualifiedName: select): HTMLCollectionOf[org.scalajs.dom.HTMLSelectElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_slot(qualifiedName: slot): HTMLCollectionOf[HTMLSlotElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_small(qualifiedName: small): HTMLCollectionOf[org.scalajs.dom.HTMLElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_source(qualifiedName: source): HTMLCollectionOf[org.scalajs.dom.HTMLSourceElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_span(qualifiedName: span): HTMLCollectionOf[org.scalajs.dom.HTMLSpanElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_stop(qualifiedName: stop): HTMLCollectionOf[org.scalajs.dom.SVGStopElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_strong(qualifiedName: strong): HTMLCollectionOf[org.scalajs.dom.HTMLElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_style(qualifiedName: style): HTMLCollectionOf[org.scalajs.dom.HTMLStyleElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_sub(qualifiedName: sub): HTMLCollectionOf[org.scalajs.dom.HTMLElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_summary(qualifiedName: summary): HTMLCollectionOf[org.scalajs.dom.HTMLElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_sup(qualifiedName: sup): HTMLCollectionOf[org.scalajs.dom.HTMLElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_svg(qualifiedName: svg): HTMLCollectionOf[org.scalajs.dom.SVGSVGElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_switch(qualifiedName: switch): HTMLCollectionOf[org.scalajs.dom.SVGSwitchElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_symbol(qualifiedName: symbol): HTMLCollectionOf[org.scalajs.dom.SVGSymbolElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_table(qualifiedName: table): HTMLCollectionOf[org.scalajs.dom.HTMLTableElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_tbody(qualifiedName: tbody): HTMLCollectionOf[org.scalajs.dom.HTMLTableSectionElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_td(qualifiedName: td): HTMLCollectionOf[HTMLTableDataCellElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_template(qualifiedName: template): HTMLCollectionOf[HTMLTemplateElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_text(qualifiedName: text): HTMLCollectionOf[org.scalajs.dom.SVGTextElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_textPath(qualifiedName: textPath): HTMLCollectionOf[org.scalajs.dom.SVGTextPathElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_textarea(qualifiedName: textarea): HTMLCollectionOf[org.scalajs.dom.HTMLTextAreaElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_tfoot(qualifiedName: tfoot): HTMLCollectionOf[org.scalajs.dom.HTMLTableSectionElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_th(qualifiedName: th): HTMLCollectionOf[HTMLTableHeaderCellElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_thead(qualifiedName: thead): HTMLCollectionOf[org.scalajs.dom.HTMLTableSectionElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_time(qualifiedName: time): HTMLCollectionOf[HTMLTimeElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_title(qualifiedName: title): HTMLCollectionOf[org.scalajs.dom.HTMLTitleElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_tr(qualifiedName: tr): HTMLCollectionOf[org.scalajs.dom.HTMLTableRowElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_track(qualifiedName: track): HTMLCollectionOf[org.scalajs.dom.HTMLTrackElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_tspan(qualifiedName: tspan): HTMLCollectionOf[org.scalajs.dom.SVGTSpanElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_u(qualifiedName: u): HTMLCollectionOf[org.scalajs.dom.HTMLElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_ul(qualifiedName: ul): HTMLCollectionOf[org.scalajs.dom.HTMLUListElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_use(qualifiedName: use): HTMLCollectionOf[org.scalajs.dom.SVGUseElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_var(qualifiedName: `var`): HTMLCollectionOf[org.scalajs.dom.HTMLElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_video(qualifiedName: video): HTMLCollectionOf[org.scalajs.dom.HTMLVideoElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_view(qualifiedName: view): HTMLCollectionOf[org.scalajs.dom.SVGViewElement] = js.native
  @JSName("getElementsByTagName")
  def getElementsByTagName_wbr(qualifiedName: wbr): HTMLCollectionOf[org.scalajs.dom.HTMLElement] = js.native
  
  /**
    * Returns true if element has an attribute whose qualified name is qualifiedName, and false otherwise.
    */
  /* standard dom */
  def hasAttribute(qualifiedName: java.lang.String): scala.Boolean = js.native
  
  /**
    * Returns true if element has an attribute whose namespace is namespace and local name is localName.
    */
  /* standard dom */
  def hasAttributeNS(namespace: java.lang.String, localName: java.lang.String): scala.Boolean = js.native
  def hasAttributeNS(namespace: Null, localName: java.lang.String): scala.Boolean = js.native
  
  /**
    * Returns true if element has attributes, and false otherwise.
    */
  /* standard dom */
  def hasAttributes(): scala.Boolean = js.native
  
  /* standard dom */
  def hasPointerCapture(pointerId: Double): scala.Boolean = js.native
  
  /**
    * Returns the value of element's id content attribute. Can be set to change it.
    */
  /* standard dom */
  var id: java.lang.String = js.native
  
  /* standard dom */
  def insertAdjacentElement(position: InsertPosition, insertedElement: org.scalajs.dom.Element): org.scalajs.dom.Element | Null = js.native
  
  /* standard dom */
  def insertAdjacentHTML(where: InsertPosition, html: java.lang.String): Unit = js.native
  
  /* standard dom */
  def insertAdjacentText(where: InsertPosition, text: java.lang.String): Unit = js.native
  
  /**
    * Returns the local name.
    */
  /* standard dom */
  val localName: java.lang.String = js.native
  
  /**
    * Returns true if matching selectors against element's root yields element, and false otherwise.
    */
  /* standard dom */
  def matches(selectors: java.lang.String): scala.Boolean = js.native
  
  /* standard dom */
  def msGetRegionContent(): Any = js.native
  
  /* standard dom */
  var onfullscreenchange: (js.ThisFunction1[/* this */ this.type, /* ev */ org.scalajs.dom.Event, Any]) | Null = js.native
  
  /* standard dom */
  var onfullscreenerror: (js.ThisFunction1[/* this */ this.type, /* ev */ org.scalajs.dom.Event, Any]) | Null = js.native
  
  /* standard dom */
  var outerHTML: java.lang.String = js.native
  
  /* standard dom */
  @JSName("ownerDocument")
  val ownerDocument_Element: org.scalajs.dom.Document = js.native
  
  /**
    * Returns the namespace prefix.
    */
  /* standard dom */
  val prefix: java.lang.String | Null = js.native
  
  /* standard dom */
  def releasePointerCapture(pointerId: Double): Unit = js.native
  
  /**
    * Removes element's first attribute whose qualified name is qualifiedName.
    */
  /* standard dom */
  def removeAttribute(qualifiedName: java.lang.String): Unit = js.native
  
  /**
    * Removes element's attribute whose namespace is namespace and local name is localName.
    */
  /* standard dom */
  def removeAttributeNS(namespace: java.lang.String, localName: java.lang.String): Unit = js.native
  def removeAttributeNS(namespace: Null, localName: java.lang.String): Unit = js.native
  
  /* standard dom */
  def removeAttributeNode(attr: org.scalajs.dom.Attr): org.scalajs.dom.Attr = js.native
  
  /* standard dom */
  @JSName("removeEventListener")
  def removeEventListener_fullscreenchange(
    `type`: fullscreenchange,
    listener: js.ThisFunction1[/* this */ this.type, /* ev */ org.scalajs.dom.Event, Any]
  ): Unit = js.native
  @JSName("removeEventListener")
  def removeEventListener_fullscreenchange(
    `type`: fullscreenchange,
    listener: js.ThisFunction1[/* this */ this.type, /* ev */ org.scalajs.dom.Event, Any],
    options: org.scalajs.dom.EventListenerOptions
  ): Unit = js.native
  @JSName("removeEventListener")
  def removeEventListener_fullscreenchange(
    `type`: fullscreenchange,
    listener: js.ThisFunction1[/* this */ this.type, /* ev */ org.scalajs.dom.Event, Any],
    options: scala.Boolean
  ): Unit = js.native
  @JSName("removeEventListener")
  def removeEventListener_fullscreenerror(
    `type`: fullscreenerror,
    listener: js.ThisFunction1[/* this */ this.type, /* ev */ org.scalajs.dom.Event, Any]
  ): Unit = js.native
  @JSName("removeEventListener")
  def removeEventListener_fullscreenerror(
    `type`: fullscreenerror,
    listener: js.ThisFunction1[/* this */ this.type, /* ev */ org.scalajs.dom.Event, Any],
    options: org.scalajs.dom.EventListenerOptions
  ): Unit = js.native
  @JSName("removeEventListener")
  def removeEventListener_fullscreenerror(
    `type`: fullscreenerror,
    listener: js.ThisFunction1[/* this */ this.type, /* ev */ org.scalajs.dom.Event, Any],
    options: scala.Boolean
  ): Unit = js.native
  
  /**
    * Displays element fullscreen and resolves promise when done.
    *
    * When supplied, options's navigationUI member indicates whether showing navigation UI while in fullscreen is preferred or not. If set to "show", navigation simplicity is preferred over screen space, and if set to "hide", more screen space is preferred. User agents are always free to honor user preference over the application's. The default value "auto" indicates no application preference.
    */
  /* standard dom */
  def requestFullscreen(): js.Promise[Unit] = js.native
  def requestFullscreen(options: FullscreenOptions): js.Promise[Unit] = js.native
  
  /* standard dom */
  def requestPointerLock(): Unit = js.native
  
  /* standard dom */
  def scroll(): Unit = js.native
  def scroll(options: ScrollToOptions): Unit = js.native
  /* standard dom */
  def scroll(x: Double, y: Double): Unit = js.native
  
  /* standard dom */
  def scrollBy(): Unit = js.native
  def scrollBy(options: ScrollToOptions): Unit = js.native
  /* standard dom */
  def scrollBy(x: Double, y: Double): Unit = js.native
  
  /* standard dom */
  val scrollHeight: Double = js.native
  
  /* standard dom */
  def scrollIntoView(): Unit = js.native
  def scrollIntoView(arg: scala.Boolean): Unit = js.native
  def scrollIntoView(arg: ScrollIntoViewOptions): Unit = js.native
  
  /* standard dom */
  var scrollLeft: Double = js.native
  
  /* standard dom */
  def scrollTo(): Unit = js.native
  def scrollTo(options: ScrollToOptions): Unit = js.native
  /* standard dom */
  def scrollTo(x: Double, y: Double): Unit = js.native
  
  /* standard dom */
  var scrollTop: Double = js.native
  
  /* standard dom */
  val scrollWidth: Double = js.native
  
  /**
    * Sets the value of element's first attribute whose qualified name is qualifiedName to value.
    */
  /* standard dom */
  def setAttribute(qualifiedName: java.lang.String, value: java.lang.String): Unit = js.native
  
  /**
    * Sets the value of element's attribute whose namespace is namespace and local name is localName to value.
    */
  /* standard dom */
  def setAttributeNS(namespace: java.lang.String, qualifiedName: java.lang.String, value: java.lang.String): Unit = js.native
  def setAttributeNS(namespace: Null, qualifiedName: java.lang.String, value: java.lang.String): Unit = js.native
  
  /* standard dom */
  def setAttributeNode(attr: org.scalajs.dom.Attr): org.scalajs.dom.Attr | Null = js.native
  
  /* standard dom */
  def setAttributeNodeNS(attr: org.scalajs.dom.Attr): org.scalajs.dom.Attr | Null = js.native
  
  /* standard dom */
  def setPointerCapture(pointerId: Double): Unit = js.native
  
  /**
    * Returns element's shadow root, if any, and if shadow root's mode is "open", and null otherwise.
    */
  /* standard dom */
  val shadowRoot: ShadowRoot | Null = js.native
  
  /**
    * Returns the value of element's slot content attribute. Can be set to change it.
    */
  /* standard dom */
  var slot: java.lang.String = js.native
  
  /**
    * Returns the HTML-uppercased qualified name.
    */
  /* standard dom */
  val tagName: java.lang.String = js.native
  
  /**
    * If force is not given, "toggles" qualifiedName, removing it if it is present and adding it if it is not present. If force is true, adds qualifiedName. If force is false, removes qualifiedName.
    *
    * Returns true if qualifiedName is now present, and false otherwise.
    */
  /* standard dom */
  def toggleAttribute(qualifiedName: java.lang.String): scala.Boolean = js.native
  def toggleAttribute(qualifiedName: java.lang.String, force: scala.Boolean): scala.Boolean = js.native
  
  /* standard dom */
  def webkitMatchesSelector(selectors: java.lang.String): scala.Boolean = js.native
}
