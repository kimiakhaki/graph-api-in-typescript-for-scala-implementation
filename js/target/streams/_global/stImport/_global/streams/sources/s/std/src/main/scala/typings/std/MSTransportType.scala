package typings.std

import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

/* Rewritten from type alias, can be one of: 
  - typings.std.stdStrings.BT
  - typings.std.stdStrings.Embedded
  - typings.std.stdStrings.NFC
  - typings.std.stdStrings.USB
*/
trait MSTransportType extends StObject
object MSTransportType {
  
  inline def BT: typings.std.stdStrings.BT = "BT".asInstanceOf[typings.std.stdStrings.BT]
  
  inline def Embedded: typings.std.stdStrings.Embedded = "Embedded".asInstanceOf[typings.std.stdStrings.Embedded]
  
  inline def NFC: typings.std.stdStrings.NFC = "NFC".asInstanceOf[typings.std.stdStrings.NFC]
  
  inline def USB: typings.std.stdStrings.USB = "USB".asInstanceOf[typings.std.stdStrings.USB]
}
