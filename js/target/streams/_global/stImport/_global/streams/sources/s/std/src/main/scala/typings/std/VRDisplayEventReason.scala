package typings.std

import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

/* Rewritten from type alias, can be one of: 
  - typings.std.stdStrings.mounted
  - typings.std.stdStrings.navigation
  - typings.std.stdStrings.requested
  - typings.std.stdStrings.unmounted
*/
trait VRDisplayEventReason extends StObject
object VRDisplayEventReason {
  
  inline def mounted: typings.std.stdStrings.mounted = "mounted".asInstanceOf[typings.std.stdStrings.mounted]
  
  inline def navigation: typings.std.stdStrings.navigation = "navigation".asInstanceOf[typings.std.stdStrings.navigation]
  
  inline def requested: typings.std.stdStrings.requested = "requested".asInstanceOf[typings.std.stdStrings.requested]
  
  inline def unmounted: typings.std.stdStrings.unmounted = "unmounted".asInstanceOf[typings.std.stdStrings.unmounted]
}
