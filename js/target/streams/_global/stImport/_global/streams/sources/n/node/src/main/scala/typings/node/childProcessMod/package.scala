package typings.node.childProcessMod

import typings.node.Buffer
import typings.node.anon.encodingBufferEncodingExe
import typings.node.anon.encodingbuffernullExecOpt
import typings.node.anon.encodingstringExecOptions
import typings.node.anon.encodingstringnullundefin
import typings.node.anon.encodingstringnullundefinCwd
import typings.node.childProcessMod.^
import typings.node.nodeStrings.ignore
import typings.node.nodeStrings.inherit
import typings.node.nodeStrings.ipc
import typings.node.nodeStrings.pipe
import typings.node.streamMod.Readable
import typings.node.streamMod.Stream
import typings.node.streamMod.Writable
import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}


// no `options` definitely means stdout/stderr are `string`.
// NOTE: This namespace provides design-time support for util.promisify. Exported members do not exist at runtime.
// fallback if nothing else matches. Worst case is always `string | Buffer`.
inline def exec(command: String): ChildProcess = ^.asInstanceOf[js.Dynamic].applyDynamic("exec")(command.asInstanceOf[js.Any]).asInstanceOf[ChildProcess]
// NOTE: This namespace provides design-time support for util.promisify. Exported members do not exist at runtime.
inline def exec(
  command: String,
  callback: js.Function3[/* error */ ExecException | Null, /* stdout */ String, /* stderr */ String, Unit]
): ChildProcess = (^.asInstanceOf[js.Dynamic].applyDynamic("exec")(command.asInstanceOf[js.Any], callback.asInstanceOf[js.Any])).asInstanceOf[ChildProcess]
// NOTE: This namespace provides design-time support for util.promisify. Exported members do not exist at runtime.
inline def exec(
  command: String,
  options: Null,
  callback: js.Function3[
  /* error */ ExecException | Null, 
  /* stdout */ String | Buffer, 
  /* stderr */ String | Buffer, 
  Unit
]
): ChildProcess = (^.asInstanceOf[js.Dynamic].applyDynamic("exec")(command.asInstanceOf[js.Any], options.asInstanceOf[js.Any], callback.asInstanceOf[js.Any])).asInstanceOf[ChildProcess]
// NOTE: This namespace provides design-time support for util.promisify. Exported members do not exist at runtime.
inline def exec(
  command: String,
  options: Unit,
  callback: js.Function3[
  /* error */ ExecException | Null, 
  /* stdout */ String | Buffer, 
  /* stderr */ String | Buffer, 
  Unit
]
): ChildProcess = (^.asInstanceOf[js.Dynamic].applyDynamic("exec")(command.asInstanceOf[js.Any], options.asInstanceOf[js.Any], callback.asInstanceOf[js.Any])).asInstanceOf[ChildProcess]
// `options` with well known `encoding` means stdout/stderr are definitely `string`.
// NOTE: This namespace provides design-time support for util.promisify. Exported members do not exist at runtime.
inline def exec(command: String, options: encodingBufferEncodingExe): ChildProcess = (^.asInstanceOf[js.Dynamic].applyDynamic("exec")(command.asInstanceOf[js.Any], options.asInstanceOf[js.Any])).asInstanceOf[ChildProcess]
// NOTE: This namespace provides design-time support for util.promisify. Exported members do not exist at runtime.
inline def exec(
  command: String,
  options: encodingBufferEncodingExe,
  callback: js.Function3[/* error */ ExecException | Null, /* stdout */ String, /* stderr */ String, Unit]
): ChildProcess = (^.asInstanceOf[js.Dynamic].applyDynamic("exec")(command.asInstanceOf[js.Any], options.asInstanceOf[js.Any], callback.asInstanceOf[js.Any])).asInstanceOf[ChildProcess]
// `options` with `"buffer"` or `null` for `encoding` means stdout/stderr are definitely `Buffer`.
// NOTE: This namespace provides design-time support for util.promisify. Exported members do not exist at runtime.
inline def exec(command: String, options: encodingbuffernullExecOpt): ChildProcess = (^.asInstanceOf[js.Dynamic].applyDynamic("exec")(command.asInstanceOf[js.Any], options.asInstanceOf[js.Any])).asInstanceOf[ChildProcess]
// NOTE: This namespace provides design-time support for util.promisify. Exported members do not exist at runtime.
inline def exec(
  command: String,
  options: encodingbuffernullExecOpt,
  callback: js.Function3[/* error */ ExecException | Null, /* stdout */ Buffer, /* stderr */ Buffer, Unit]
): ChildProcess = (^.asInstanceOf[js.Dynamic].applyDynamic("exec")(command.asInstanceOf[js.Any], options.asInstanceOf[js.Any], callback.asInstanceOf[js.Any])).asInstanceOf[ChildProcess]
// `options` with an `encoding` whose type is `string` means stdout/stderr could either be `Buffer` or `string`.
// There is no guarantee the `encoding` is unknown as `string` is a superset of `BufferEncoding`.
// NOTE: This namespace provides design-time support for util.promisify. Exported members do not exist at runtime.
inline def exec(command: String, options: encodingstringExecOptions): ChildProcess = (^.asInstanceOf[js.Dynamic].applyDynamic("exec")(command.asInstanceOf[js.Any], options.asInstanceOf[js.Any])).asInstanceOf[ChildProcess]
// NOTE: This namespace provides design-time support for util.promisify. Exported members do not exist at runtime.
inline def exec(
  command: String,
  options: encodingstringExecOptions,
  callback: js.Function3[
  /* error */ ExecException | Null, 
  /* stdout */ String | Buffer, 
  /* stderr */ String | Buffer, 
  Unit
]
): ChildProcess = (^.asInstanceOf[js.Dynamic].applyDynamic("exec")(command.asInstanceOf[js.Any], options.asInstanceOf[js.Any], callback.asInstanceOf[js.Any])).asInstanceOf[ChildProcess]
// NOTE: This namespace provides design-time support for util.promisify. Exported members do not exist at runtime.
inline def exec(command: String, options: encodingstringnullundefin): ChildProcess = (^.asInstanceOf[js.Dynamic].applyDynamic("exec")(command.asInstanceOf[js.Any], options.asInstanceOf[js.Any])).asInstanceOf[ChildProcess]
// NOTE: This namespace provides design-time support for util.promisify. Exported members do not exist at runtime.
inline def exec(
  command: String,
  options: encodingstringnullundefin,
  callback: js.Function3[
  /* error */ ExecException | Null, 
  /* stdout */ String | Buffer, 
  /* stderr */ String | Buffer, 
  Unit
]
): ChildProcess = (^.asInstanceOf[js.Dynamic].applyDynamic("exec")(command.asInstanceOf[js.Any], options.asInstanceOf[js.Any], callback.asInstanceOf[js.Any])).asInstanceOf[ChildProcess]
// `options` without an `encoding` means stdout/stderr are definitely `string`.
// NOTE: This namespace provides design-time support for util.promisify. Exported members do not exist at runtime.
inline def exec(command: String, options: ExecOptions): ChildProcess = (^.asInstanceOf[js.Dynamic].applyDynamic("exec")(command.asInstanceOf[js.Any], options.asInstanceOf[js.Any])).asInstanceOf[ChildProcess]
// NOTE: This namespace provides design-time support for util.promisify. Exported members do not exist at runtime.
inline def exec(
  command: String,
  options: ExecOptions,
  callback: js.Function3[/* error */ ExecException | Null, /* stdout */ String, /* stderr */ String, Unit]
): ChildProcess = (^.asInstanceOf[js.Dynamic].applyDynamic("exec")(command.asInstanceOf[js.Any], options.asInstanceOf[js.Any], callback.asInstanceOf[js.Any])).asInstanceOf[ChildProcess]

// NOTE: This namespace provides design-time support for util.promisify. Exported members do not exist at runtime.
// fallback if nothing else matches. Worst case is always `string | Buffer`.
inline def execFile(file: String): ChildProcess = ^.asInstanceOf[js.Dynamic].applyDynamic("execFile")(file.asInstanceOf[js.Any]).asInstanceOf[ChildProcess]
// NOTE: This namespace provides design-time support for util.promisify. Exported members do not exist at runtime.
inline def execFile(file: String, args: js.Array[String]): ChildProcess = (^.asInstanceOf[js.Dynamic].applyDynamic("execFile")(file.asInstanceOf[js.Any], args.asInstanceOf[js.Any])).asInstanceOf[ChildProcess]
// NOTE: This namespace provides design-time support for util.promisify. Exported members do not exist at runtime.
inline def execFile(
  file: String,
  args: js.Array[String],
  callback: js.Function3[/* error */ ExecException | Null, /* stdout */ String, /* stderr */ String, Unit]
): ChildProcess = (^.asInstanceOf[js.Dynamic].applyDynamic("execFile")(file.asInstanceOf[js.Any], args.asInstanceOf[js.Any], callback.asInstanceOf[js.Any])).asInstanceOf[ChildProcess]
// NOTE: This namespace provides design-time support for util.promisify. Exported members do not exist at runtime.
inline def execFile(
  file: String,
  args: js.Array[String],
  options: Null,
  callback: js.Function3[
  /* error */ ExecException | Null, 
  /* stdout */ String | Buffer, 
  /* stderr */ String | Buffer, 
  Unit
]
): ChildProcess = (^.asInstanceOf[js.Dynamic].applyDynamic("execFile")(file.asInstanceOf[js.Any], args.asInstanceOf[js.Any], options.asInstanceOf[js.Any], callback.asInstanceOf[js.Any])).asInstanceOf[ChildProcess]
// NOTE: This namespace provides design-time support for util.promisify. Exported members do not exist at runtime.
inline def execFile(
  file: String,
  args: js.Array[String],
  options: Unit,
  callback: js.Function3[
  /* error */ ExecException | Null, 
  /* stdout */ String | Buffer, 
  /* stderr */ String | Buffer, 
  Unit
]
): ChildProcess = (^.asInstanceOf[js.Dynamic].applyDynamic("execFile")(file.asInstanceOf[js.Any], args.asInstanceOf[js.Any], options.asInstanceOf[js.Any], callback.asInstanceOf[js.Any])).asInstanceOf[ChildProcess]
// NOTE: This namespace provides design-time support for util.promisify. Exported members do not exist at runtime.
inline def execFile(file: String, args: js.Array[String], options: encodingstringnullundefinCwd): ChildProcess = (^.asInstanceOf[js.Dynamic].applyDynamic("execFile")(file.asInstanceOf[js.Any], args.asInstanceOf[js.Any], options.asInstanceOf[js.Any])).asInstanceOf[ChildProcess]
// NOTE: This namespace provides design-time support for util.promisify. Exported members do not exist at runtime.
inline def execFile(
  file: String,
  args: js.Array[String],
  options: encodingstringnullundefinCwd,
  callback: js.Function3[
  /* error */ ExecException | Null, 
  /* stdout */ String | Buffer, 
  /* stderr */ String | Buffer, 
  Unit
]
): ChildProcess = (^.asInstanceOf[js.Dynamic].applyDynamic("execFile")(file.asInstanceOf[js.Any], args.asInstanceOf[js.Any], options.asInstanceOf[js.Any], callback.asInstanceOf[js.Any])).asInstanceOf[ChildProcess]
// NOTE: This namespace provides design-time support for util.promisify. Exported members do not exist at runtime.
inline def execFile(
  file: String,
  args: js.Array[String],
  options: ExecFileOptionsWithBufferEncoding,
  callback: js.Function3[/* error */ ExecException | Null, /* stdout */ Buffer, /* stderr */ Buffer, Unit]
): ChildProcess = (^.asInstanceOf[js.Dynamic].applyDynamic("execFile")(file.asInstanceOf[js.Any], args.asInstanceOf[js.Any], options.asInstanceOf[js.Any], callback.asInstanceOf[js.Any])).asInstanceOf[ChildProcess]
// NOTE: This namespace provides design-time support for util.promisify. Exported members do not exist at runtime.
inline def execFile(
  file: String,
  args: js.Array[String],
  options: ExecFileOptionsWithOtherEncoding,
  callback: js.Function3[
  /* error */ ExecException | Null, 
  /* stdout */ String | Buffer, 
  /* stderr */ String | Buffer, 
  Unit
]
): ChildProcess = (^.asInstanceOf[js.Dynamic].applyDynamic("execFile")(file.asInstanceOf[js.Any], args.asInstanceOf[js.Any], options.asInstanceOf[js.Any], callback.asInstanceOf[js.Any])).asInstanceOf[ChildProcess]
// NOTE: This namespace provides design-time support for util.promisify. Exported members do not exist at runtime.
inline def execFile(
  file: String,
  args: js.Array[String],
  options: ExecFileOptionsWithStringEncoding,
  callback: js.Function3[/* error */ ExecException | Null, /* stdout */ String, /* stderr */ String, Unit]
): ChildProcess = (^.asInstanceOf[js.Dynamic].applyDynamic("execFile")(file.asInstanceOf[js.Any], args.asInstanceOf[js.Any], options.asInstanceOf[js.Any], callback.asInstanceOf[js.Any])).asInstanceOf[ChildProcess]
// NOTE: This namespace provides design-time support for util.promisify. Exported members do not exist at runtime.
inline def execFile(
  file: String,
  args: js.Array[String],
  options: ExecFileOptions,
  callback: js.Function3[/* error */ ExecException | Null, /* stdout */ String, /* stderr */ String, Unit]
): ChildProcess = (^.asInstanceOf[js.Dynamic].applyDynamic("execFile")(file.asInstanceOf[js.Any], args.asInstanceOf[js.Any], options.asInstanceOf[js.Any], callback.asInstanceOf[js.Any])).asInstanceOf[ChildProcess]
// NOTE: This namespace provides design-time support for util.promisify. Exported members do not exist at runtime.
inline def execFile(
  file: String,
  args: Null,
  callback: js.Function3[
  ExecException | Null, 
  Buffer | (/* stdout */ String), 
  Buffer | (/* stderr */ String), 
  Unit
]
): ChildProcess = (^.asInstanceOf[js.Dynamic].applyDynamic("execFile")(file.asInstanceOf[js.Any], args.asInstanceOf[js.Any], callback.asInstanceOf[js.Any])).asInstanceOf[ChildProcess]
// NOTE: This namespace provides design-time support for util.promisify. Exported members do not exist at runtime.
inline def execFile(
  file: String,
  args: Null,
  options: Null,
  callback: js.Function3[
  /* error */ ExecException | Null, 
  /* stdout */ String | Buffer, 
  /* stderr */ String | Buffer, 
  Unit
]
): ChildProcess = (^.asInstanceOf[js.Dynamic].applyDynamic("execFile")(file.asInstanceOf[js.Any], args.asInstanceOf[js.Any], options.asInstanceOf[js.Any], callback.asInstanceOf[js.Any])).asInstanceOf[ChildProcess]
// NOTE: This namespace provides design-time support for util.promisify. Exported members do not exist at runtime.
inline def execFile(
  file: String,
  args: Null,
  options: Unit,
  callback: js.Function3[
  /* error */ ExecException | Null, 
  /* stdout */ String | Buffer, 
  /* stderr */ String | Buffer, 
  Unit
]
): ChildProcess = (^.asInstanceOf[js.Dynamic].applyDynamic("execFile")(file.asInstanceOf[js.Any], args.asInstanceOf[js.Any], options.asInstanceOf[js.Any], callback.asInstanceOf[js.Any])).asInstanceOf[ChildProcess]
// NOTE: This namespace provides design-time support for util.promisify. Exported members do not exist at runtime.
inline def execFile(file: String, args: Null, options: encodingstringnullundefinCwd): ChildProcess = (^.asInstanceOf[js.Dynamic].applyDynamic("execFile")(file.asInstanceOf[js.Any], args.asInstanceOf[js.Any], options.asInstanceOf[js.Any])).asInstanceOf[ChildProcess]
// NOTE: This namespace provides design-time support for util.promisify. Exported members do not exist at runtime.
inline def execFile(
  file: String,
  args: Null,
  options: encodingstringnullundefinCwd,
  callback: js.Function3[
  /* error */ ExecException | Null, 
  /* stdout */ String | Buffer, 
  /* stderr */ String | Buffer, 
  Unit
]
): ChildProcess = (^.asInstanceOf[js.Dynamic].applyDynamic("execFile")(file.asInstanceOf[js.Any], args.asInstanceOf[js.Any], options.asInstanceOf[js.Any], callback.asInstanceOf[js.Any])).asInstanceOf[ChildProcess]
// NOTE: This namespace provides design-time support for util.promisify. Exported members do not exist at runtime.
inline def execFile(
  file: String,
  args: Null,
  options: ExecFileOptionsWithBufferEncoding,
  callback: js.Function3[/* error */ ExecException | Null, /* stdout */ Buffer, /* stderr */ Buffer, Unit]
): ChildProcess = (^.asInstanceOf[js.Dynamic].applyDynamic("execFile")(file.asInstanceOf[js.Any], args.asInstanceOf[js.Any], options.asInstanceOf[js.Any], callback.asInstanceOf[js.Any])).asInstanceOf[ChildProcess]
// NOTE: This namespace provides design-time support for util.promisify. Exported members do not exist at runtime.
inline def execFile(
  file: String,
  args: Null,
  options: ExecFileOptionsWithOtherEncoding,
  callback: js.Function3[
  /* error */ ExecException | Null, 
  /* stdout */ String | Buffer, 
  /* stderr */ String | Buffer, 
  Unit
]
): ChildProcess = (^.asInstanceOf[js.Dynamic].applyDynamic("execFile")(file.asInstanceOf[js.Any], args.asInstanceOf[js.Any], options.asInstanceOf[js.Any], callback.asInstanceOf[js.Any])).asInstanceOf[ChildProcess]
// NOTE: This namespace provides design-time support for util.promisify. Exported members do not exist at runtime.
inline def execFile(
  file: String,
  args: Null,
  options: ExecFileOptionsWithStringEncoding,
  callback: js.Function3[/* error */ ExecException | Null, /* stdout */ String, /* stderr */ String, Unit]
): ChildProcess = (^.asInstanceOf[js.Dynamic].applyDynamic("execFile")(file.asInstanceOf[js.Any], args.asInstanceOf[js.Any], options.asInstanceOf[js.Any], callback.asInstanceOf[js.Any])).asInstanceOf[ChildProcess]
// NOTE: This namespace provides design-time support for util.promisify. Exported members do not exist at runtime.
inline def execFile(
  file: String,
  args: Null,
  options: ExecFileOptions,
  callback: js.Function3[/* error */ ExecException | Null, /* stdout */ String, /* stderr */ String, Unit]
): ChildProcess = (^.asInstanceOf[js.Dynamic].applyDynamic("execFile")(file.asInstanceOf[js.Any], args.asInstanceOf[js.Any], options.asInstanceOf[js.Any], callback.asInstanceOf[js.Any])).asInstanceOf[ChildProcess]
// NOTE: This namespace provides design-time support for util.promisify. Exported members do not exist at runtime.
inline def execFile(
  file: String,
  args: Unit,
  callback: js.Function3[
  ExecException | Null, 
  Buffer | (/* stdout */ String), 
  Buffer | (/* stderr */ String), 
  Unit
]
): ChildProcess = (^.asInstanceOf[js.Dynamic].applyDynamic("execFile")(file.asInstanceOf[js.Any], args.asInstanceOf[js.Any], callback.asInstanceOf[js.Any])).asInstanceOf[ChildProcess]
// NOTE: This namespace provides design-time support for util.promisify. Exported members do not exist at runtime.
inline def execFile(
  file: String,
  args: Unit,
  options: Null,
  callback: js.Function3[
  /* error */ ExecException | Null, 
  /* stdout */ String | Buffer, 
  /* stderr */ String | Buffer, 
  Unit
]
): ChildProcess = (^.asInstanceOf[js.Dynamic].applyDynamic("execFile")(file.asInstanceOf[js.Any], args.asInstanceOf[js.Any], options.asInstanceOf[js.Any], callback.asInstanceOf[js.Any])).asInstanceOf[ChildProcess]
// NOTE: This namespace provides design-time support for util.promisify. Exported members do not exist at runtime.
inline def execFile(
  file: String,
  args: Unit,
  options: Unit,
  callback: js.Function3[
  /* error */ ExecException | Null, 
  /* stdout */ String | Buffer, 
  /* stderr */ String | Buffer, 
  Unit
]
): ChildProcess = (^.asInstanceOf[js.Dynamic].applyDynamic("execFile")(file.asInstanceOf[js.Any], args.asInstanceOf[js.Any], options.asInstanceOf[js.Any], callback.asInstanceOf[js.Any])).asInstanceOf[ChildProcess]
// NOTE: This namespace provides design-time support for util.promisify. Exported members do not exist at runtime.
inline def execFile(file: String, args: Unit, options: encodingstringnullundefinCwd): ChildProcess = (^.asInstanceOf[js.Dynamic].applyDynamic("execFile")(file.asInstanceOf[js.Any], args.asInstanceOf[js.Any], options.asInstanceOf[js.Any])).asInstanceOf[ChildProcess]
// NOTE: This namespace provides design-time support for util.promisify. Exported members do not exist at runtime.
inline def execFile(
  file: String,
  args: Unit,
  options: encodingstringnullundefinCwd,
  callback: js.Function3[
  /* error */ ExecException | Null, 
  /* stdout */ String | Buffer, 
  /* stderr */ String | Buffer, 
  Unit
]
): ChildProcess = (^.asInstanceOf[js.Dynamic].applyDynamic("execFile")(file.asInstanceOf[js.Any], args.asInstanceOf[js.Any], options.asInstanceOf[js.Any], callback.asInstanceOf[js.Any])).asInstanceOf[ChildProcess]
// NOTE: This namespace provides design-time support for util.promisify. Exported members do not exist at runtime.
inline def execFile(
  file: String,
  args: Unit,
  options: ExecFileOptionsWithBufferEncoding,
  callback: js.Function3[/* error */ ExecException | Null, /* stdout */ Buffer, /* stderr */ Buffer, Unit]
): ChildProcess = (^.asInstanceOf[js.Dynamic].applyDynamic("execFile")(file.asInstanceOf[js.Any], args.asInstanceOf[js.Any], options.asInstanceOf[js.Any], callback.asInstanceOf[js.Any])).asInstanceOf[ChildProcess]
// NOTE: This namespace provides design-time support for util.promisify. Exported members do not exist at runtime.
inline def execFile(
  file: String,
  args: Unit,
  options: ExecFileOptionsWithOtherEncoding,
  callback: js.Function3[
  /* error */ ExecException | Null, 
  /* stdout */ String | Buffer, 
  /* stderr */ String | Buffer, 
  Unit
]
): ChildProcess = (^.asInstanceOf[js.Dynamic].applyDynamic("execFile")(file.asInstanceOf[js.Any], args.asInstanceOf[js.Any], options.asInstanceOf[js.Any], callback.asInstanceOf[js.Any])).asInstanceOf[ChildProcess]
// NOTE: This namespace provides design-time support for util.promisify. Exported members do not exist at runtime.
inline def execFile(
  file: String,
  args: Unit,
  options: ExecFileOptionsWithStringEncoding,
  callback: js.Function3[/* error */ ExecException | Null, /* stdout */ String, /* stderr */ String, Unit]
): ChildProcess = (^.asInstanceOf[js.Dynamic].applyDynamic("execFile")(file.asInstanceOf[js.Any], args.asInstanceOf[js.Any], options.asInstanceOf[js.Any], callback.asInstanceOf[js.Any])).asInstanceOf[ChildProcess]
// NOTE: This namespace provides design-time support for util.promisify. Exported members do not exist at runtime.
inline def execFile(
  file: String,
  args: Unit,
  options: ExecFileOptions,
  callback: js.Function3[/* error */ ExecException | Null, /* stdout */ String, /* stderr */ String, Unit]
): ChildProcess = (^.asInstanceOf[js.Dynamic].applyDynamic("execFile")(file.asInstanceOf[js.Any], args.asInstanceOf[js.Any], options.asInstanceOf[js.Any], callback.asInstanceOf[js.Any])).asInstanceOf[ChildProcess]
// no `options` definitely means stdout/stderr are `string`.
// NOTE: This namespace provides design-time support for util.promisify. Exported members do not exist at runtime.
inline def execFile(
  file: String,
  callback: js.Function3[/* error */ ExecException | Null, /* stdout */ String, /* stderr */ String, Unit]
): ChildProcess = (^.asInstanceOf[js.Dynamic].applyDynamic("execFile")(file.asInstanceOf[js.Any], callback.asInstanceOf[js.Any])).asInstanceOf[ChildProcess]
// NOTE: This namespace provides design-time support for util.promisify. Exported members do not exist at runtime.
inline def execFile(file: String, options: encodingstringnullundefinCwd): ChildProcess = (^.asInstanceOf[js.Dynamic].applyDynamic("execFile")(file.asInstanceOf[js.Any], options.asInstanceOf[js.Any])).asInstanceOf[ChildProcess]
// NOTE: This namespace provides design-time support for util.promisify. Exported members do not exist at runtime.
inline def execFile(
  file: String,
  options: encodingstringnullundefinCwd,
  callback: js.Function3[
  /* error */ ExecException | Null, 
  /* stdout */ String | Buffer, 
  /* stderr */ String | Buffer, 
  Unit
]
): ChildProcess = (^.asInstanceOf[js.Dynamic].applyDynamic("execFile")(file.asInstanceOf[js.Any], options.asInstanceOf[js.Any], callback.asInstanceOf[js.Any])).asInstanceOf[ChildProcess]
// `options` with `"buffer"` or `null` for `encoding` means stdout/stderr are definitely `Buffer`.
// NOTE: This namespace provides design-time support for util.promisify. Exported members do not exist at runtime.
inline def execFile(
  file: String,
  options: ExecFileOptionsWithBufferEncoding,
  callback: js.Function3[/* error */ ExecException | Null, /* stdout */ Buffer, /* stderr */ Buffer, Unit]
): ChildProcess = (^.asInstanceOf[js.Dynamic].applyDynamic("execFile")(file.asInstanceOf[js.Any], options.asInstanceOf[js.Any], callback.asInstanceOf[js.Any])).asInstanceOf[ChildProcess]
// `options` with an `encoding` whose type is `string` means stdout/stderr could either be `Buffer` or `string`.
// There is no guarantee the `encoding` is unknown as `string` is a superset of `BufferEncoding`.
// NOTE: This namespace provides design-time support for util.promisify. Exported members do not exist at runtime.
inline def execFile(
  file: String,
  options: ExecFileOptionsWithOtherEncoding,
  callback: js.Function3[
  /* error */ ExecException | Null, 
  /* stdout */ String | Buffer, 
  /* stderr */ String | Buffer, 
  Unit
]
): ChildProcess = (^.asInstanceOf[js.Dynamic].applyDynamic("execFile")(file.asInstanceOf[js.Any], options.asInstanceOf[js.Any], callback.asInstanceOf[js.Any])).asInstanceOf[ChildProcess]
// `options` with well known `encoding` means stdout/stderr are definitely `string`.
// NOTE: This namespace provides design-time support for util.promisify. Exported members do not exist at runtime.
inline def execFile(
  file: String,
  options: ExecFileOptionsWithStringEncoding,
  callback: js.Function3[/* error */ ExecException | Null, /* stdout */ String, /* stderr */ String, Unit]
): ChildProcess = (^.asInstanceOf[js.Dynamic].applyDynamic("execFile")(file.asInstanceOf[js.Any], options.asInstanceOf[js.Any], callback.asInstanceOf[js.Any])).asInstanceOf[ChildProcess]
// `options` without an `encoding` means stdout/stderr are definitely `string`.
// NOTE: This namespace provides design-time support for util.promisify. Exported members do not exist at runtime.
inline def execFile(
  file: String,
  options: ExecFileOptions,
  callback: js.Function3[/* error */ ExecException | Null, /* stdout */ String, /* stderr */ String, Unit]
): ChildProcess = (^.asInstanceOf[js.Dynamic].applyDynamic("execFile")(file.asInstanceOf[js.Any], options.asInstanceOf[js.Any], callback.asInstanceOf[js.Any])).asInstanceOf[ChildProcess]

inline def execFileSync(command: String): String = ^.asInstanceOf[js.Dynamic].applyDynamic("execFileSync")(command.asInstanceOf[js.Any]).asInstanceOf[String]
inline def execFileSync(command: String, args: js.Array[String]): String = (^.asInstanceOf[js.Dynamic].applyDynamic("execFileSync")(command.asInstanceOf[js.Any], args.asInstanceOf[js.Any])).asInstanceOf[String]
inline def execFileSync(command: String, args: js.Array[String], options: ExecFileSyncOptions): Buffer = (^.asInstanceOf[js.Dynamic].applyDynamic("execFileSync")(command.asInstanceOf[js.Any], args.asInstanceOf[js.Any], options.asInstanceOf[js.Any])).asInstanceOf[Buffer]
inline def execFileSync(command: String, args: js.Array[String], options: ExecFileSyncOptionsWithBufferEncoding): Buffer = (^.asInstanceOf[js.Dynamic].applyDynamic("execFileSync")(command.asInstanceOf[js.Any], args.asInstanceOf[js.Any], options.asInstanceOf[js.Any])).asInstanceOf[Buffer]
inline def execFileSync(command: String, args: js.Array[String], options: ExecFileSyncOptionsWithStringEncoding): String = (^.asInstanceOf[js.Dynamic].applyDynamic("execFileSync")(command.asInstanceOf[js.Any], args.asInstanceOf[js.Any], options.asInstanceOf[js.Any])).asInstanceOf[String]
inline def execFileSync(command: String, args: Unit, options: ExecFileSyncOptions): Buffer = (^.asInstanceOf[js.Dynamic].applyDynamic("execFileSync")(command.asInstanceOf[js.Any], args.asInstanceOf[js.Any], options.asInstanceOf[js.Any])).asInstanceOf[Buffer]
inline def execFileSync(command: String, args: Unit, options: ExecFileSyncOptionsWithBufferEncoding): Buffer = (^.asInstanceOf[js.Dynamic].applyDynamic("execFileSync")(command.asInstanceOf[js.Any], args.asInstanceOf[js.Any], options.asInstanceOf[js.Any])).asInstanceOf[Buffer]
inline def execFileSync(command: String, args: Unit, options: ExecFileSyncOptionsWithStringEncoding): String = (^.asInstanceOf[js.Dynamic].applyDynamic("execFileSync")(command.asInstanceOf[js.Any], args.asInstanceOf[js.Any], options.asInstanceOf[js.Any])).asInstanceOf[String]
inline def execFileSync(command: String, options: ExecFileSyncOptions): Buffer = (^.asInstanceOf[js.Dynamic].applyDynamic("execFileSync")(command.asInstanceOf[js.Any], options.asInstanceOf[js.Any])).asInstanceOf[Buffer]
inline def execFileSync(command: String, options: ExecFileSyncOptionsWithBufferEncoding): Buffer = (^.asInstanceOf[js.Dynamic].applyDynamic("execFileSync")(command.asInstanceOf[js.Any], options.asInstanceOf[js.Any])).asInstanceOf[Buffer]
inline def execFileSync(command: String, options: ExecFileSyncOptionsWithStringEncoding): String = (^.asInstanceOf[js.Dynamic].applyDynamic("execFileSync")(command.asInstanceOf[js.Any], options.asInstanceOf[js.Any])).asInstanceOf[String]

inline def execFileSync_Buffer(command: String): Buffer = ^.asInstanceOf[js.Dynamic].applyDynamic("execFileSync")(command.asInstanceOf[js.Any]).asInstanceOf[Buffer]
inline def execFileSync_Buffer(command: String, args: js.Array[String]): Buffer = (^.asInstanceOf[js.Dynamic].applyDynamic("execFileSync")(command.asInstanceOf[js.Any], args.asInstanceOf[js.Any])).asInstanceOf[Buffer]

inline def execSync(command: String): String = ^.asInstanceOf[js.Dynamic].applyDynamic("execSync")(command.asInstanceOf[js.Any]).asInstanceOf[String]
inline def execSync(command: String, options: ExecSyncOptions): Buffer = (^.asInstanceOf[js.Dynamic].applyDynamic("execSync")(command.asInstanceOf[js.Any], options.asInstanceOf[js.Any])).asInstanceOf[Buffer]
inline def execSync(command: String, options: ExecSyncOptionsWithBufferEncoding): Buffer = (^.asInstanceOf[js.Dynamic].applyDynamic("execSync")(command.asInstanceOf[js.Any], options.asInstanceOf[js.Any])).asInstanceOf[Buffer]
inline def execSync(command: String, options: ExecSyncOptionsWithStringEncoding): String = (^.asInstanceOf[js.Dynamic].applyDynamic("execSync")(command.asInstanceOf[js.Any], options.asInstanceOf[js.Any])).asInstanceOf[String]

inline def execSync_Buffer(command: String): Buffer = ^.asInstanceOf[js.Dynamic].applyDynamic("execSync")(command.asInstanceOf[js.Any]).asInstanceOf[Buffer]

inline def fork(modulePath: String): ChildProcess = ^.asInstanceOf[js.Dynamic].applyDynamic("fork")(modulePath.asInstanceOf[js.Any]).asInstanceOf[ChildProcess]
inline def fork(modulePath: String, args: js.Array[String]): ChildProcess = (^.asInstanceOf[js.Dynamic].applyDynamic("fork")(modulePath.asInstanceOf[js.Any], args.asInstanceOf[js.Any])).asInstanceOf[ChildProcess]
inline def fork(modulePath: String, args: js.Array[String], options: ForkOptions): ChildProcess = (^.asInstanceOf[js.Dynamic].applyDynamic("fork")(modulePath.asInstanceOf[js.Any], args.asInstanceOf[js.Any], options.asInstanceOf[js.Any])).asInstanceOf[ChildProcess]
inline def fork(modulePath: String, args: Unit, options: ForkOptions): ChildProcess = (^.asInstanceOf[js.Dynamic].applyDynamic("fork")(modulePath.asInstanceOf[js.Any], args.asInstanceOf[js.Any], options.asInstanceOf[js.Any])).asInstanceOf[ChildProcess]

// overloads of spawn without 'args'
// overloads of spawn with 'args'
inline def spawn(command: String): ChildProcessWithoutNullStreams = ^.asInstanceOf[js.Dynamic].applyDynamic("spawn")(command.asInstanceOf[js.Any]).asInstanceOf[ChildProcessWithoutNullStreams]
inline def spawn(command: String, args: js.Array[String]): ChildProcessWithoutNullStreams = (^.asInstanceOf[js.Dynamic].applyDynamic("spawn")(command.asInstanceOf[js.Any], args.asInstanceOf[js.Any])).asInstanceOf[ChildProcessWithoutNullStreams]
inline def spawn(command: String, args: js.Array[String], options: SpawnOptions): ChildProcess = (^.asInstanceOf[js.Dynamic].applyDynamic("spawn")(command.asInstanceOf[js.Any], args.asInstanceOf[js.Any], options.asInstanceOf[js.Any])).asInstanceOf[ChildProcess]
inline def spawn(
  command: String,
  args: js.Array[String],
  options: SpawnOptionsWithStdioTuple[StdioNull | StdioPipe, StdioNull | StdioPipe, StdioNull | StdioPipe]
): ChildProcessByStdio[Writable, Readable, Readable] = (^.asInstanceOf[js.Dynamic].applyDynamic("spawn")(command.asInstanceOf[js.Any], args.asInstanceOf[js.Any], options.asInstanceOf[js.Any])).asInstanceOf[ChildProcessByStdio[Writable, Readable, Readable]]
inline def spawn(command: String, args: js.Array[String], options: SpawnOptionsWithoutStdio): ChildProcessWithoutNullStreams = (^.asInstanceOf[js.Dynamic].applyDynamic("spawn")(command.asInstanceOf[js.Any], args.asInstanceOf[js.Any], options.asInstanceOf[js.Any])).asInstanceOf[ChildProcessWithoutNullStreams]
inline def spawn(command: String, args: Unit, options: SpawnOptionsWithoutStdio): ChildProcessWithoutNullStreams = (^.asInstanceOf[js.Dynamic].applyDynamic("spawn")(command.asInstanceOf[js.Any], args.asInstanceOf[js.Any], options.asInstanceOf[js.Any])).asInstanceOf[ChildProcessWithoutNullStreams]
inline def spawn(command: String, options: SpawnOptions): ChildProcess = (^.asInstanceOf[js.Dynamic].applyDynamic("spawn")(command.asInstanceOf[js.Any], options.asInstanceOf[js.Any])).asInstanceOf[ChildProcess]
inline def spawn(
  command: String,
  options: SpawnOptionsWithStdioTuple[StdioNull | StdioPipe, StdioNull | StdioPipe, StdioNull | StdioPipe]
): ChildProcessByStdio[Writable, Readable, Readable] = (^.asInstanceOf[js.Dynamic].applyDynamic("spawn")(command.asInstanceOf[js.Any], options.asInstanceOf[js.Any])).asInstanceOf[ChildProcessByStdio[Writable, Readable, Readable]]
inline def spawn(command: String, options: SpawnOptionsWithoutStdio): ChildProcessWithoutNullStreams = (^.asInstanceOf[js.Dynamic].applyDynamic("spawn")(command.asInstanceOf[js.Any], options.asInstanceOf[js.Any])).asInstanceOf[ChildProcessWithoutNullStreams]

inline def spawnSync(command: String): SpawnSyncReturns[Buffer] = ^.asInstanceOf[js.Dynamic].applyDynamic("spawnSync")(command.asInstanceOf[js.Any]).asInstanceOf[SpawnSyncReturns[Buffer]]
inline def spawnSync(command: String, args: js.Array[String]): SpawnSyncReturns[String] = (^.asInstanceOf[js.Dynamic].applyDynamic("spawnSync")(command.asInstanceOf[js.Any], args.asInstanceOf[js.Any])).asInstanceOf[SpawnSyncReturns[String]]
inline def spawnSync(command: String, args: js.Array[String], options: SpawnSyncOptions): SpawnSyncReturns[Buffer] = (^.asInstanceOf[js.Dynamic].applyDynamic("spawnSync")(command.asInstanceOf[js.Any], args.asInstanceOf[js.Any], options.asInstanceOf[js.Any])).asInstanceOf[SpawnSyncReturns[Buffer]]
inline def spawnSync(command: String, args: js.Array[String], options: SpawnSyncOptionsWithBufferEncoding): SpawnSyncReturns[Buffer] = (^.asInstanceOf[js.Dynamic].applyDynamic("spawnSync")(command.asInstanceOf[js.Any], args.asInstanceOf[js.Any], options.asInstanceOf[js.Any])).asInstanceOf[SpawnSyncReturns[Buffer]]
inline def spawnSync(command: String, args: js.Array[String], options: SpawnSyncOptionsWithStringEncoding): SpawnSyncReturns[String] = (^.asInstanceOf[js.Dynamic].applyDynamic("spawnSync")(command.asInstanceOf[js.Any], args.asInstanceOf[js.Any], options.asInstanceOf[js.Any])).asInstanceOf[SpawnSyncReturns[String]]
inline def spawnSync(command: String, args: Unit, options: SpawnSyncOptions): SpawnSyncReturns[Buffer] = (^.asInstanceOf[js.Dynamic].applyDynamic("spawnSync")(command.asInstanceOf[js.Any], args.asInstanceOf[js.Any], options.asInstanceOf[js.Any])).asInstanceOf[SpawnSyncReturns[Buffer]]
inline def spawnSync(command: String, args: Unit, options: SpawnSyncOptionsWithBufferEncoding): SpawnSyncReturns[Buffer] = (^.asInstanceOf[js.Dynamic].applyDynamic("spawnSync")(command.asInstanceOf[js.Any], args.asInstanceOf[js.Any], options.asInstanceOf[js.Any])).asInstanceOf[SpawnSyncReturns[Buffer]]
inline def spawnSync(command: String, args: Unit, options: SpawnSyncOptionsWithStringEncoding): SpawnSyncReturns[String] = (^.asInstanceOf[js.Dynamic].applyDynamic("spawnSync")(command.asInstanceOf[js.Any], args.asInstanceOf[js.Any], options.asInstanceOf[js.Any])).asInstanceOf[SpawnSyncReturns[String]]
inline def spawnSync(command: String, options: SpawnSyncOptions): SpawnSyncReturns[Buffer] = (^.asInstanceOf[js.Dynamic].applyDynamic("spawnSync")(command.asInstanceOf[js.Any], options.asInstanceOf[js.Any])).asInstanceOf[SpawnSyncReturns[Buffer]]
inline def spawnSync(command: String, options: SpawnSyncOptionsWithBufferEncoding): SpawnSyncReturns[Buffer] = (^.asInstanceOf[js.Dynamic].applyDynamic("spawnSync")(command.asInstanceOf[js.Any], options.asInstanceOf[js.Any])).asInstanceOf[SpawnSyncReturns[Buffer]]
inline def spawnSync(command: String, options: SpawnSyncOptionsWithStringEncoding): SpawnSyncReturns[String] = (^.asInstanceOf[js.Dynamic].applyDynamic("spawnSync")(command.asInstanceOf[js.Any], options.asInstanceOf[js.Any])).asInstanceOf[SpawnSyncReturns[String]]

type Serializable = String | js.Object | Double | Boolean

/* Rewritten from type alias, can be one of: 
  - typings.node.nodeStrings.pipe
  - typings.node.nodeStrings.ignore
  - typings.node.nodeStrings.inherit
  - js.Array[
js.UndefOr[
  typings.node.nodeStrings.pipe | typings.node.nodeStrings.ipc | typings.node.nodeStrings.ignore | typings.node.nodeStrings.inherit | typings.node.streamMod.Stream | scala.Double | scala.Null
]]
*/
type StdioOptions = _StdioOptions | (js.Array[js.UndefOr[pipe | ipc | ignore | inherit | Stream | Double | Null]])

type StdioPipe = js.UndefOr[Null | pipe]
