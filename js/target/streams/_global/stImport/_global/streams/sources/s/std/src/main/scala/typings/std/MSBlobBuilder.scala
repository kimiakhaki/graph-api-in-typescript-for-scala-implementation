package typings.std

import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

@js.native
trait MSBlobBuilder extends StObject {
  
  /* standard dom */
  def append(data: Any): Unit = js.native
  def append(data: Any, endings: java.lang.String): Unit = js.native
  
  /* standard dom */
  def getBlob(): org.scalajs.dom.Blob = js.native
  def getBlob(contentType: java.lang.String): org.scalajs.dom.Blob = js.native
}
