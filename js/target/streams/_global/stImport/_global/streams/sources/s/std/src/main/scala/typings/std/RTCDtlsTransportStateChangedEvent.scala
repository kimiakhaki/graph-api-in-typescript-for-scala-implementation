package typings.std

import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

@js.native
trait RTCDtlsTransportStateChangedEvent
  extends StObject
     with Event {
  
  /* standard dom */
  val state: RTCDtlsTransportState = js.native
}
