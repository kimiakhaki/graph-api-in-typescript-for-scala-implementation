package typings.node.NodeJS

import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

// compatibility with older typings
trait Timer
  extends StObject
     with RefCounted {
  
  def hasRef(): Boolean
  
  def refresh(): this.type
}
object Timer {
  
  inline def apply(hasRef: () => Boolean, ref: () => Timer, refresh: () => Timer, unref: () => Timer): Timer = {
    val __obj = js.Dynamic.literal(hasRef = js.Any.fromFunction0(hasRef), ref = js.Any.fromFunction0(ref), refresh = js.Any.fromFunction0(refresh), unref = js.Any.fromFunction0(unref))
    __obj.asInstanceOf[Timer]
  }
  
  @scala.inline
  implicit open class MutableBuilder[Self <: Timer] (val x: Self) extends AnyVal {
    
    inline def setHasRef(value: () => Boolean): Self = StObject.set(x, "hasRef", js.Any.fromFunction0(value))
    
    inline def setRefresh(value: () => Timer): Self = StObject.set(x, "refresh", js.Any.fromFunction0(value))
  }
}
