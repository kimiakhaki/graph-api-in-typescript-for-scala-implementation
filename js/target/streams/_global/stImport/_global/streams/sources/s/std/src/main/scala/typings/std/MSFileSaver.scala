package typings.std

import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

@js.native
trait MSFileSaver extends StObject {
  
  /* standard dom */
  def msSaveBlob(blob: Any): scala.Boolean = js.native
  def msSaveBlob(blob: Any, defaultName: java.lang.String): scala.Boolean = js.native
  
  /* standard dom */
  def msSaveOrOpenBlob(blob: Any): scala.Boolean = js.native
  def msSaveOrOpenBlob(blob: Any, defaultName: java.lang.String): scala.Boolean = js.native
}
