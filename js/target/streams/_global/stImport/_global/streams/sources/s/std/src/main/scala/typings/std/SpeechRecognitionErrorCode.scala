package typings.std

import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

/* Rewritten from type alias, can be one of: 
  - typings.std.stdStrings.aborted
  - typings.std.stdStrings.`audio-capture`
  - typings.std.stdStrings.`bad-grammar`
  - typings.std.stdStrings.`language-not-supported`
  - typings.std.stdStrings.network
  - typings.std.stdStrings.`no-speech`
  - typings.std.stdStrings.`not-allowed`
  - typings.std.stdStrings.`service-not-allowed`
*/
trait SpeechRecognitionErrorCode extends StObject
object SpeechRecognitionErrorCode {
  
  inline def aborted: typings.std.stdStrings.aborted = "aborted".asInstanceOf[typings.std.stdStrings.aborted]
  
  inline def `audio-capture`: typings.std.stdStrings.`audio-capture` = "audio-capture".asInstanceOf[typings.std.stdStrings.`audio-capture`]
  
  inline def `bad-grammar`: typings.std.stdStrings.`bad-grammar` = "bad-grammar".asInstanceOf[typings.std.stdStrings.`bad-grammar`]
  
  inline def `language-not-supported`: typings.std.stdStrings.`language-not-supported` = "language-not-supported".asInstanceOf[typings.std.stdStrings.`language-not-supported`]
  
  inline def network: typings.std.stdStrings.network = "network".asInstanceOf[typings.std.stdStrings.network]
  
  inline def `no-speech`: typings.std.stdStrings.`no-speech` = "no-speech".asInstanceOf[typings.std.stdStrings.`no-speech`]
  
  inline def `not-allowed`: typings.std.stdStrings.`not-allowed` = "not-allowed".asInstanceOf[typings.std.stdStrings.`not-allowed`]
  
  inline def `service-not-allowed`: typings.std.stdStrings.`service-not-allowed` = "service-not-allowed".asInstanceOf[typings.std.stdStrings.`service-not-allowed`]
}
