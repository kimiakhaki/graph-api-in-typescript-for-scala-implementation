package typings.std.global

import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

/* This class was inferred from a value with a constructor. In rare cases (like HTMLElement in the DOM) it might not work as you expect. */
@JSGlobal("HTMLMediaElement")
@js.native
/* standard dom */
open class HTMLMediaElement ()
  extends StObject
     with typings.std.HTMLMediaElement {
  
  /* standard dom */
  /* CompleteClass */
  override val assignedSlot: typings.std.HTMLSlotElement | Null = js.native
  
  /* standard dom */
  /* CompleteClass */
  var contentEditable: java.lang.String = js.native
  
  /* standard dom */
  /* CompleteClass */
  var enterKeyHint: java.lang.String = js.native
  
  /* standard dom */
  /* CompleteClass */
  var innerHTML: java.lang.String = js.native
  
  /* standard dom */
  /* CompleteClass */
  var inputMode: java.lang.String = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val isContentEditable: scala.Boolean = js.native
  
  /**
    * Returns the first following sibling that is an element, and null otherwise.
    */
  /* standard dom */
  /* CompleteClass */
  override val nextElementSibling: org.scalajs.dom.Element | Null = js.native
  
  /**
    * Returns the first preceding sibling that is an element, and null otherwise.
    */
  /* standard dom */
  /* CompleteClass */
  override val previousElementSibling: org.scalajs.dom.Element | Null = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val style: org.scalajs.dom.CSSStyleDeclaration = js.native
}
object HTMLMediaElement {
  
  /* standard dom */
  @JSGlobal("HTMLMediaElement.HAVE_CURRENT_DATA")
  @js.native
  val HAVE_CURRENT_DATA: Double = js.native
  
  /* standard dom */
  @JSGlobal("HTMLMediaElement.HAVE_ENOUGH_DATA")
  @js.native
  val HAVE_ENOUGH_DATA: Double = js.native
  
  /* standard dom */
  @JSGlobal("HTMLMediaElement.HAVE_FUTURE_DATA")
  @js.native
  val HAVE_FUTURE_DATA: Double = js.native
  
  /* standard dom */
  @JSGlobal("HTMLMediaElement.HAVE_METADATA")
  @js.native
  val HAVE_METADATA: Double = js.native
  
  /* standard dom */
  @JSGlobal("HTMLMediaElement.HAVE_NOTHING")
  @js.native
  val HAVE_NOTHING: Double = js.native
  
  /* standard dom */
  @JSGlobal("HTMLMediaElement.NETWORK_EMPTY")
  @js.native
  val NETWORK_EMPTY: Double = js.native
  
  /* standard dom */
  @JSGlobal("HTMLMediaElement.NETWORK_IDLE")
  @js.native
  val NETWORK_IDLE: Double = js.native
  
  /* standard dom */
  @JSGlobal("HTMLMediaElement.NETWORK_LOADING")
  @js.native
  val NETWORK_LOADING: Double = js.native
  
  /* standard dom */
  @JSGlobal("HTMLMediaElement.NETWORK_NO_SOURCE")
  @js.native
  val NETWORK_NO_SOURCE: Double = js.native
}
