package typings.node.NodeJS

import org.scalablytyped.runtime.StringDictionary
import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}


type ArrayBufferView = TypedArray | js.typedarray.DataView

type BeforeExitListener = js.Function1[/* code */ Double, Unit]

type Dict[T] = StringDictionary[js.UndefOr[T]]

type DisconnectListener = js.Function0[Unit]

type ExitListener = js.Function1[/* code */ Double, Unit]

type HRTime = js.Function1[/* time */ js.UndefOr[js.Tuple2[Double, Double]], js.Tuple2[Double, Double]]

type MessageListener = js.Function2[/* message */ Any, /* sendHandle */ Any, Unit]

type MultipleResolveListener = js.Function3[/* type */ MultipleResolveType, /* promise */ js.Promise[Any], /* value */ Any, Unit]

type NewListenerListener = js.Function2[
/* type */ String | js.Symbol, 
/* listener */ js.Function1[/* repeated */ Any, Unit], 
Unit]

// Alias for compatibility
type ProcessEnv = Dict[String]

type ReadOnlyDict[T] = StringDictionary[js.UndefOr[T]]

type RejectionHandledListener = js.Function1[/* promise */ js.Promise[Any], Unit]

type RemoveListenerListener = js.Function2[
/* type */ String | js.Symbol, 
/* listener */ js.Function1[/* repeated */ Any, Unit], 
Unit]

type SignalsListener = js.Function1[/* signal */ Signals, Unit]

type TypedArray = js.typedarray.Uint8Array | js.typedarray.Uint8ClampedArray | js.typedarray.Uint16Array | js.typedarray.Uint32Array | js.typedarray.Int8Array | js.typedarray.Int16Array | js.typedarray.Int32Array | js.typedarray.Float32Array | js.typedarray.Float64Array

type UncaughtExceptionListener = js.Function1[/* error */ js.Error, Unit]

type UnhandledRejectionListener = js.Function2[/* reason */ js.UndefOr[js.Object | Null], /* promise */ js.Promise[Any], Unit]

type WarningListener = js.Function1[/* warning */ js.Error, Unit]
