package typings.std

import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

/* Rewritten from type alias, can be one of: 
  - typings.std.stdStrings.`candidate-pair`
  - typings.std.stdStrings.certificate
  - typings.std.stdStrings.codec
  - typings.std.stdStrings.csrc
  - typings.std.stdStrings.`data-channel`
  - typings.std.stdStrings.`ice-server`
  - typings.std.stdStrings.`inbound-rtp`
  - typings.std.stdStrings.`local-candidate`
  - typings.std.stdStrings.`media-source`
  - typings.std.stdStrings.`outbound-rtp`
  - typings.std.stdStrings.`peer-connection`
  - typings.std.stdStrings.receiver
  - typings.std.stdStrings.`remote-candidate`
  - typings.std.stdStrings.`remote-inbound-rtp`
  - typings.std.stdStrings.`remote-outbound-rtp`
  - typings.std.stdStrings.`sctp-transport`
  - typings.std.stdStrings.sender
  - typings.std.stdStrings.stream
  - typings.std.stdStrings.track
  - typings.std.stdStrings.transceiver
  - typings.std.stdStrings.transport
*/
trait RTCStatsType extends StObject
object RTCStatsType {
  
  inline def `candidate-pair`: typings.std.stdStrings.`candidate-pair` = "candidate-pair".asInstanceOf[typings.std.stdStrings.`candidate-pair`]
  
  inline def certificate: typings.std.stdStrings.certificate = "certificate".asInstanceOf[typings.std.stdStrings.certificate]
  
  inline def codec: typings.std.stdStrings.codec = "codec".asInstanceOf[typings.std.stdStrings.codec]
  
  inline def csrc: typings.std.stdStrings.csrc = "csrc".asInstanceOf[typings.std.stdStrings.csrc]
  
  inline def `data-channel`: typings.std.stdStrings.`data-channel` = "data-channel".asInstanceOf[typings.std.stdStrings.`data-channel`]
  
  inline def `ice-server`: typings.std.stdStrings.`ice-server` = "ice-server".asInstanceOf[typings.std.stdStrings.`ice-server`]
  
  inline def `inbound-rtp`: typings.std.stdStrings.`inbound-rtp` = "inbound-rtp".asInstanceOf[typings.std.stdStrings.`inbound-rtp`]
  
  inline def `local-candidate`: typings.std.stdStrings.`local-candidate` = "local-candidate".asInstanceOf[typings.std.stdStrings.`local-candidate`]
  
  inline def `media-source`: typings.std.stdStrings.`media-source` = "media-source".asInstanceOf[typings.std.stdStrings.`media-source`]
  
  inline def `outbound-rtp`: typings.std.stdStrings.`outbound-rtp` = "outbound-rtp".asInstanceOf[typings.std.stdStrings.`outbound-rtp`]
  
  inline def `peer-connection`: typings.std.stdStrings.`peer-connection` = "peer-connection".asInstanceOf[typings.std.stdStrings.`peer-connection`]
  
  inline def receiver: typings.std.stdStrings.receiver = "receiver".asInstanceOf[typings.std.stdStrings.receiver]
  
  inline def `remote-candidate`: typings.std.stdStrings.`remote-candidate` = "remote-candidate".asInstanceOf[typings.std.stdStrings.`remote-candidate`]
  
  inline def `remote-inbound-rtp`: typings.std.stdStrings.`remote-inbound-rtp` = "remote-inbound-rtp".asInstanceOf[typings.std.stdStrings.`remote-inbound-rtp`]
  
  inline def `remote-outbound-rtp`: typings.std.stdStrings.`remote-outbound-rtp` = "remote-outbound-rtp".asInstanceOf[typings.std.stdStrings.`remote-outbound-rtp`]
  
  inline def `sctp-transport`: typings.std.stdStrings.`sctp-transport` = "sctp-transport".asInstanceOf[typings.std.stdStrings.`sctp-transport`]
  
  inline def sender: typings.std.stdStrings.sender = "sender".asInstanceOf[typings.std.stdStrings.sender]
  
  inline def stream: typings.std.stdStrings.stream = "stream".asInstanceOf[typings.std.stdStrings.stream]
  
  inline def track: typings.std.stdStrings.track = "track".asInstanceOf[typings.std.stdStrings.track]
  
  inline def transceiver: typings.std.stdStrings.transceiver = "transceiver".asInstanceOf[typings.std.stdStrings.transceiver]
  
  inline def transport: typings.std.stdStrings.transport = "transport".asInstanceOf[typings.std.stdStrings.transport]
}
