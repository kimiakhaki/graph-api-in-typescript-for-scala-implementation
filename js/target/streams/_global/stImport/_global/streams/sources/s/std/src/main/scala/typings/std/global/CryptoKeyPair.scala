package typings.std.global

import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

/* This class was inferred from a value with a constructor. In rare cases (like HTMLElement in the DOM) it might not work as you expect. */
@JSGlobal("CryptoKeyPair")
@js.native
/* standard dom */
open class CryptoKeyPair ()
  extends StObject
     with typings.std.CryptoKeyPair {
  
  /* standard dom */
  /* CompleteClass */
  var privateKey: org.scalajs.dom.CryptoKey = js.native
  
  /* standard dom */
  /* CompleteClass */
  var publicKey: org.scalajs.dom.CryptoKey = js.native
}
