package typings.std

import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

/* Rewritten from type alias, can be one of: 
  - typings.std.stdStrings._empty
  - typings.std.stdStrings.left
  - typings.std.stdStrings.right
*/
trait GamepadHand extends StObject
object GamepadHand {
  
  inline def _empty: typings.std.stdStrings._empty = "".asInstanceOf[typings.std.stdStrings._empty]
  
  inline def left: typings.std.stdStrings.left = "left".asInstanceOf[typings.std.stdStrings.left]
  
  inline def right: typings.std.stdStrings.right = "right".asInstanceOf[typings.std.stdStrings.right]
}
