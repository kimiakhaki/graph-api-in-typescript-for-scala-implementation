package typings.node.NodeJS

import typings.node.nodeBooleans.`true`
import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

@js.native
trait Socket
  extends StObject
     with ReadWriteStream {
  
  var isTTY: js.UndefOr[`true`] = js.native
}
