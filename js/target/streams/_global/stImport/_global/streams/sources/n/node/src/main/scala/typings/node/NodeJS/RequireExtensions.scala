package typings.node.NodeJS

import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

trait RequireExtensions
  extends StObject
     with Dict[js.Function2[/* m */ Module, /* filename */ String, Any]] {
  
  @JSName(".js")
  def Dotjs(m: Module, filename: String): Any
  
  @JSName(".json")
  def Dotjson(m: Module, filename: String): Any
  
  @JSName(".node")
  def Dotnode(m: Module, filename: String): Any
}
object RequireExtensions {
  
  inline def apply(Dotjs: (Module, String) => Any, Dotjson: (Module, String) => Any, Dotnode: (Module, String) => Any): RequireExtensions = {
    val __obj = js.Dynamic.literal()
    __obj.updateDynamic(".js")(js.Any.fromFunction2(Dotjs))
    __obj.updateDynamic(".json")(js.Any.fromFunction2(Dotjson))
    __obj.updateDynamic(".node")(js.Any.fromFunction2(Dotnode))
    __obj.asInstanceOf[RequireExtensions]
  }
  
  @scala.inline
  implicit open class MutableBuilder[Self <: RequireExtensions] (val x: Self) extends AnyVal {
    
    inline def setDotjs(value: (Module, String) => Any): Self = StObject.set(x, ".js", js.Any.fromFunction2(value))
    
    inline def setDotjson(value: (Module, String) => Any): Self = StObject.set(x, ".json", js.Any.fromFunction2(value))
    
    inline def setDotnode(value: (Module, String) => Any): Self = StObject.set(x, ".node", js.Any.fromFunction2(value))
  }
}
