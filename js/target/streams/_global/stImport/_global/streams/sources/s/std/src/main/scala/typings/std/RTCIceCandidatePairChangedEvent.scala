package typings.std

import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

@js.native
trait RTCIceCandidatePairChangedEvent
  extends StObject
     with Event {
  
  /* standard dom */
  val pair: RTCIceCandidatePair = js.native
}
