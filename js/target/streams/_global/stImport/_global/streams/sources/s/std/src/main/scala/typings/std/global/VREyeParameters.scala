package typings.std.global

import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

/* This class was inferred from a value with a constructor. In rare cases (like HTMLElement in the DOM) it might not work as you expect. */
@JSGlobal("VREyeParameters")
@js.native
/* standard dom */
open class VREyeParameters ()
  extends StObject
     with typings.std.VREyeParameters {
  
  /** @deprecated */
  /* standard dom */
  /* CompleteClass */
  override val fieldOfView: typings.std.VRFieldOfView = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val offset: js.typedarray.Float32Array = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val renderHeight: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val renderWidth: Double = js.native
}
