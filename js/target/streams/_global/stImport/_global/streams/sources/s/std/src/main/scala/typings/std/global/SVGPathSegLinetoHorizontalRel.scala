package typings.std.global

import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

/* This class was inferred from a value with a constructor. In rare cases (like HTMLElement in the DOM) it might not work as you expect. */
@JSGlobal("SVGPathSegLinetoHorizontalRel")
@js.native
/* standard dom */
open class SVGPathSegLinetoHorizontalRel ()
  extends StObject
     with typings.std.SVGPathSegLinetoHorizontalRel {
  
  /* standard dom */
  /* CompleteClass */
  override val PATHSEG_ARC_ABS: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val PATHSEG_ARC_REL: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val PATHSEG_CLOSEPATH: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val PATHSEG_CURVETO_CUBIC_ABS: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val PATHSEG_CURVETO_CUBIC_REL: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val PATHSEG_CURVETO_CUBIC_SMOOTH_ABS: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val PATHSEG_CURVETO_CUBIC_SMOOTH_REL: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val PATHSEG_CURVETO_QUADRATIC_ABS: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val PATHSEG_CURVETO_QUADRATIC_REL: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val PATHSEG_CURVETO_QUADRATIC_SMOOTH_ABS: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val PATHSEG_CURVETO_QUADRATIC_SMOOTH_REL: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val PATHSEG_LINETO_ABS: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val PATHSEG_LINETO_HORIZONTAL_ABS: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val PATHSEG_LINETO_HORIZONTAL_REL: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val PATHSEG_LINETO_REL: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val PATHSEG_LINETO_VERTICAL_ABS: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val PATHSEG_LINETO_VERTICAL_REL: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val PATHSEG_MOVETO_ABS: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val PATHSEG_MOVETO_REL: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val PATHSEG_UNKNOWN: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val pathSegType: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val pathSegTypeAsLetter: java.lang.String = js.native
  
  /* standard dom */
  /* CompleteClass */
  var x: Double = js.native
}
