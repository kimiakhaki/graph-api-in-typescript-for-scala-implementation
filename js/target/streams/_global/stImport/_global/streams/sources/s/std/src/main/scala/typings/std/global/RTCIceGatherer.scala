package typings.std.global

import typings.std.RTCIceGatherOptions
import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

/* This class was inferred from a value with a constructor. In rare cases (like HTMLElement in the DOM) it might not work as you expect. */
@JSGlobal("RTCIceGatherer")
@js.native
open class RTCIceGatherer protected ()
  extends StObject
     with typings.std.RTCIceGatherer {
  /* standard dom */
  def this(options: RTCIceGatherOptions) = this()
}
