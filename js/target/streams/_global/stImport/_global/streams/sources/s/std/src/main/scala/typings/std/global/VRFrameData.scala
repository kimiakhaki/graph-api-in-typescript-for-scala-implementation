package typings.std.global

import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

/* This class was inferred from a value with a constructor. In rare cases (like HTMLElement in the DOM) it might not work as you expect. */
@JSGlobal("VRFrameData")
@js.native
/* standard dom */
open class VRFrameData ()
  extends StObject
     with typings.std.VRFrameData {
  
  /* standard dom */
  /* CompleteClass */
  override val leftProjectionMatrix: js.typedarray.Float32Array = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val leftViewMatrix: js.typedarray.Float32Array = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val pose: typings.std.VRPose = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val rightProjectionMatrix: js.typedarray.Float32Array = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val rightViewMatrix: js.typedarray.Float32Array = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val timestamp: Double = js.native
}
