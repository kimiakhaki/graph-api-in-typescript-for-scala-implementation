package typings.std.global

import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

/* This class was inferred from a value with a constructor. In rare cases (like HTMLElement in the DOM) it might not work as you expect. */
@JSGlobal("VRFieldOfView")
@js.native
/* standard dom */
open class VRFieldOfView ()
  extends StObject
     with typings.std.VRFieldOfView {
  
  /* standard dom */
  /* CompleteClass */
  override val downDegrees: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val leftDegrees: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val rightDegrees: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val upDegrees: Double = js.native
}
