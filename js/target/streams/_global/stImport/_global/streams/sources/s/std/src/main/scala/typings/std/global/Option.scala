package typings.std.global

import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

/* This class was inferred from a value with a constructor. In rare cases (like HTMLElement in the DOM) it might not work as you expect. */
@JSGlobal("Option")
@js.native
/* standard dom */
open class Option ()
  extends StObject
     with typings.std.HTMLOptionElement {
  def this(text: java.lang.String) = this()
  def this(text: java.lang.String, value: java.lang.String) = this()
  def this(text: Unit, value: java.lang.String) = this()
  def this(text: java.lang.String, value: java.lang.String, defaultSelected: scala.Boolean) = this()
  def this(text: java.lang.String, value: Unit, defaultSelected: scala.Boolean) = this()
  def this(text: Unit, value: java.lang.String, defaultSelected: scala.Boolean) = this()
  def this(text: Unit, value: Unit, defaultSelected: scala.Boolean) = this()
  def this(
    text: java.lang.String,
    value: java.lang.String,
    defaultSelected: scala.Boolean,
    selected: scala.Boolean
  ) = this()
  def this(text: java.lang.String, value: java.lang.String, defaultSelected: Unit, selected: scala.Boolean) = this()
  def this(text: java.lang.String, value: Unit, defaultSelected: scala.Boolean, selected: scala.Boolean) = this()
  def this(text: java.lang.String, value: Unit, defaultSelected: Unit, selected: scala.Boolean) = this()
  def this(text: Unit, value: java.lang.String, defaultSelected: scala.Boolean, selected: scala.Boolean) = this()
  def this(text: Unit, value: java.lang.String, defaultSelected: Unit, selected: scala.Boolean) = this()
  def this(text: Unit, value: Unit, defaultSelected: scala.Boolean, selected: scala.Boolean) = this()
  def this(text: Unit, value: Unit, defaultSelected: Unit, selected: scala.Boolean) = this()
  
  /* standard dom */
  /* CompleteClass */
  override val assignedSlot: typings.std.HTMLSlotElement | Null = js.native
  
  /* standard dom */
  /* CompleteClass */
  var contentEditable: java.lang.String = js.native
  
  /* standard dom */
  /* CompleteClass */
  var enterKeyHint: java.lang.String = js.native
  
  /* standard dom */
  /* CompleteClass */
  var innerHTML: java.lang.String = js.native
  
  /* standard dom */
  /* CompleteClass */
  var inputMode: java.lang.String = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val isContentEditable: scala.Boolean = js.native
  
  /**
    * Returns the first following sibling that is an element, and null otherwise.
    */
  /* standard dom */
  /* CompleteClass */
  override val nextElementSibling: org.scalajs.dom.Element | Null = js.native
  
  /**
    * Returns the first preceding sibling that is an element, and null otherwise.
    */
  /* standard dom */
  /* CompleteClass */
  override val previousElementSibling: org.scalajs.dom.Element | Null = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val style: org.scalajs.dom.CSSStyleDeclaration = js.native
}
