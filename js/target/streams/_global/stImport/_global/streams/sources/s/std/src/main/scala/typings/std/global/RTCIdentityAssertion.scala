package typings.std.global

import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

/* This class was inferred from a value with a constructor. In rare cases (like HTMLElement in the DOM) it might not work as you expect. */
@JSGlobal("RTCIdentityAssertion")
@js.native
open class RTCIdentityAssertion protected ()
  extends StObject
     with typings.std.RTCIdentityAssertion {
  /* standard dom */
  def this(idp: java.lang.String, name: java.lang.String) = this()
  
  /* standard dom */
  /* CompleteClass */
  var idp: java.lang.String = js.native
  
  /* standard dom */
  /* CompleteClass */
  var name: java.lang.String = js.native
}
