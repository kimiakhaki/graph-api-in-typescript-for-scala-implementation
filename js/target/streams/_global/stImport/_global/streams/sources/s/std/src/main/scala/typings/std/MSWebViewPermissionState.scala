package typings.std

import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

/* Rewritten from type alias, can be one of: 
  - typings.std.stdStrings.allow
  - typings.std.stdStrings.defer
  - typings.std.stdStrings.deny
  - typings.std.stdStrings.unknown
*/
trait MSWebViewPermissionState extends StObject
object MSWebViewPermissionState {
  
  inline def allow: typings.std.stdStrings.allow = "allow".asInstanceOf[typings.std.stdStrings.allow]
  
  inline def defer: typings.std.stdStrings.defer = "defer".asInstanceOf[typings.std.stdStrings.defer]
  
  inline def deny: typings.std.stdStrings.deny = "deny".asInstanceOf[typings.std.stdStrings.deny]
  
  inline def unknown: typings.std.stdStrings.unknown = "unknown".asInstanceOf[typings.std.stdStrings.unknown]
}
