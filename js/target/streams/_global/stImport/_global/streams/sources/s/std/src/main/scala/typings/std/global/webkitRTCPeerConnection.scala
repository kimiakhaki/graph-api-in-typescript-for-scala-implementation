package typings.std.global

import org.scalajs.dom.RTCConfiguration
import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

/* This class was inferred from a value with a constructor. In rare cases (like HTMLElement in the DOM) it might not work as you expect. */
@JSGlobal("webkitRTCPeerConnection")
@js.native
open class webkitRTCPeerConnection protected ()
  extends StObject
     with typings.std.webkitRTCPeerConnection {
  /* standard dom */
  def this(configuration: RTCConfiguration) = this()
}
