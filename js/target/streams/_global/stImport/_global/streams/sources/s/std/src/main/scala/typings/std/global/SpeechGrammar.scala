package typings.std.global

import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

/* This class was inferred from a value with a constructor. In rare cases (like HTMLElement in the DOM) it might not work as you expect. */
@JSGlobal("SpeechGrammar")
@js.native
/* standard dom */
open class SpeechGrammar ()
  extends StObject
     with typings.std.SpeechGrammar {
  
  /* standard dom */
  /* CompleteClass */
  var src: java.lang.String = js.native
  
  /* standard dom */
  /* CompleteClass */
  var weight: Double = js.native
}
