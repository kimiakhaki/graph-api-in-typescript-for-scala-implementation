package typings.node.NodeJS

import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

/* Rewritten from type alias, can be one of: 
  - typings.node.nodeStrings.resolve
  - typings.node.nodeStrings.reject
*/
trait MultipleResolveType extends StObject
object MultipleResolveType {
  
  inline def reject: typings.node.nodeStrings.reject = "reject".asInstanceOf[typings.node.nodeStrings.reject]
  
  inline def resolve: typings.node.nodeStrings.resolve = "resolve".asInstanceOf[typings.node.nodeStrings.resolve]
}
