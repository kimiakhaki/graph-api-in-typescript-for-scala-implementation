package typings.std

import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

@js.native
trait OverflowEvent
  extends StObject
     with UIEvent {
  
  /* standard dom */
  val BOTH: Double = js.native
  
  /* standard dom */
  val HORIZONTAL: Double = js.native
  
  /* standard dom */
  val VERTICAL: Double = js.native
  
  /* standard dom */
  val horizontalOverflow: scala.Boolean = js.native
  
  /* standard dom */
  val orient: Double = js.native
  
  /* standard dom */
  val verticalOverflow: scala.Boolean = js.native
}
