package typings.std

import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

@js.native
trait RegExpExecArray
  extends StObject
     with Array[java.lang.String] {
  
  /* standard es5 */
  var index: Double = js.native
  
  /* standard es5 */
  var input: java.lang.String = js.native
}
