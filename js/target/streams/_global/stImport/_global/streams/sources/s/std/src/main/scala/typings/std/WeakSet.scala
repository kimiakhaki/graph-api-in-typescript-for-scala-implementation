package typings.std

import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

@js.native
trait WeakSet[T /* <: js.Object */] extends StObject {
  
  /* standard es2015.collection */
  def add(value: T): this.type = js.native
  
  /* standard es2015.collection */
  def delete(value: T): scala.Boolean = js.native
  
  /* standard es2015.collection */
  def has(value: T): scala.Boolean = js.native
  
  /* standard es2015.symbol.wellknown */
  @JSName(js.Symbol.toStringTag)
  val toStringTag: java.lang.String = js.native
}
