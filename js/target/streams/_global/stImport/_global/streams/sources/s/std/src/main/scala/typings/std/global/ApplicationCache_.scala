package typings.std.global

import typings.std.ApplicationCache
import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

/* This class was inferred from a value with a constructor. In rare cases (like HTMLElement in the DOM) it might not work as you expect. */
@JSGlobal("ApplicationCache")
@js.native
/* standard dom */
open class ApplicationCache_ ()
  extends StObject
     with ApplicationCache
object ApplicationCache_ {
  
  /* standard dom */
  @JSGlobal("ApplicationCache.CHECKING")
  @js.native
  val CHECKING: Double = js.native
  
  /* standard dom */
  @JSGlobal("ApplicationCache.DOWNLOADING")
  @js.native
  val DOWNLOADING: Double = js.native
  
  /* standard dom */
  @JSGlobal("ApplicationCache.IDLE")
  @js.native
  val IDLE: Double = js.native
  
  /* standard dom */
  @JSGlobal("ApplicationCache.OBSOLETE")
  @js.native
  val OBSOLETE: Double = js.native
  
  /* standard dom */
  @JSGlobal("ApplicationCache.UNCACHED")
  @js.native
  val UNCACHED: Double = js.native
  
  /* standard dom */
  @JSGlobal("ApplicationCache.UPDATEREADY")
  @js.native
  val UPDATEREADY: Double = js.native
}
