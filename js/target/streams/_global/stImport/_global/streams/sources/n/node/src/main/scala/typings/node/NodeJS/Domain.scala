package typings.node.NodeJS

import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

@js.native
trait Domain
  extends StObject
     with EventEmitter {
  
  def add(emitter: EventEmitter): Unit = js.native
  def add(emitter: Timer): Unit = js.native
  
  def bind[T /* <: js.Function */](cb: T): T = js.native
  
  def intercept[T /* <: js.Function */](cb: T): T = js.native
  
  def remove(emitter: EventEmitter): Unit = js.native
  def remove(emitter: Timer): Unit = js.native
  
  def run[T](fn: js.Function1[/* repeated */ Any, T], args: Any*): T = js.native
}
