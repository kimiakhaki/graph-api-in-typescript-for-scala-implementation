package typings.std.global

import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

/* This class was inferred from a value with a constructor. In rare cases (like HTMLElement in the DOM) it might not work as you expect. */
@JSGlobal("SVGElementInstanceList")
@js.native
/* standard dom */
open class SVGElementInstanceList ()
  extends StObject
     with typings.std.SVGElementInstanceList {
  
  /** @deprecated */
  /* standard dom */
  /* CompleteClass */
  override def item(index: Double): org.scalajs.dom.SVGElementInstance = js.native
  
  /** @deprecated */
  /* standard dom */
  /* CompleteClass */
  override val length: Double = js.native
}
