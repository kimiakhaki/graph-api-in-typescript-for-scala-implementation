package typings.std.global

import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

/* This class was inferred from a value with a constructor. In rare cases (like HTMLElement in the DOM) it might not work as you expect. */
@JSGlobal("SVGComponentTransferFunctionElement")
@js.native
/* standard dom */
open class SVGComponentTransferFunctionElement ()
  extends StObject
     with typings.std.SVGComponentTransferFunctionElement {
  
  /* standard dom */
  /* CompleteClass */
  override val assignedSlot: typings.std.HTMLSlotElement | Null = js.native
  
  /* standard dom */
  /* CompleteClass */
  var innerHTML: java.lang.String = js.native
  
  /**
    * Returns the first following sibling that is an element, and null otherwise.
    */
  /* standard dom */
  /* CompleteClass */
  override val nextElementSibling: org.scalajs.dom.Element | Null = js.native
  
  /**
    * Returns the first preceding sibling that is an element, and null otherwise.
    */
  /* standard dom */
  /* CompleteClass */
  override val previousElementSibling: org.scalajs.dom.Element | Null = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val style: org.scalajs.dom.CSSStyleDeclaration = js.native
}
object SVGComponentTransferFunctionElement {
  
  /* standard dom */
  @JSGlobal("SVGComponentTransferFunctionElement.SVG_FECOMPONENTTRANSFER_TYPE_DISCRETE")
  @js.native
  val SVG_FECOMPONENTTRANSFER_TYPE_DISCRETE: Double = js.native
  
  /* standard dom */
  @JSGlobal("SVGComponentTransferFunctionElement.SVG_FECOMPONENTTRANSFER_TYPE_GAMMA")
  @js.native
  val SVG_FECOMPONENTTRANSFER_TYPE_GAMMA: Double = js.native
  
  /* standard dom */
  @JSGlobal("SVGComponentTransferFunctionElement.SVG_FECOMPONENTTRANSFER_TYPE_IDENTITY")
  @js.native
  val SVG_FECOMPONENTTRANSFER_TYPE_IDENTITY: Double = js.native
  
  /* standard dom */
  @JSGlobal("SVGComponentTransferFunctionElement.SVG_FECOMPONENTTRANSFER_TYPE_LINEAR")
  @js.native
  val SVG_FECOMPONENTTRANSFER_TYPE_LINEAR: Double = js.native
  
  /* standard dom */
  @JSGlobal("SVGComponentTransferFunctionElement.SVG_FECOMPONENTTRANSFER_TYPE_TABLE")
  @js.native
  val SVG_FECOMPONENTTRANSFER_TYPE_TABLE: Double = js.native
  
  /* standard dom */
  @JSGlobal("SVGComponentTransferFunctionElement.SVG_FECOMPONENTTRANSFER_TYPE_UNKNOWN")
  @js.native
  val SVG_FECOMPONENTTRANSFER_TYPE_UNKNOWN: Double = js.native
}
