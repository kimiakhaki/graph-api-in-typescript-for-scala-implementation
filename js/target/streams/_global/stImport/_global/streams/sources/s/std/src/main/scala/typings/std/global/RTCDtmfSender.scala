package typings.std.global

import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

/* This class was inferred from a value with a constructor. In rare cases (like HTMLElement in the DOM) it might not work as you expect. */
@JSGlobal("RTCDtmfSender")
@js.native
open class RTCDtmfSender protected ()
  extends StObject
     with typings.std.RTCDtmfSender {
  /* standard dom */
  def this(sender: typings.std.RTCRtpSender) = this()
}
