package typings.std.global

import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

/* This class was inferred from a value with a constructor. In rare cases (like HTMLElement in the DOM) it might not work as you expect. */
@JSGlobal("HTMLTrackElement")
@js.native
/* standard dom */
open class HTMLTrackElement ()
  extends StObject
     with typings.std.HTMLTrackElement {
  
  /* standard dom */
  /* CompleteClass */
  override val assignedSlot: typings.std.HTMLSlotElement | Null = js.native
  
  /* standard dom */
  /* CompleteClass */
  var contentEditable: java.lang.String = js.native
  
  /* standard dom */
  /* CompleteClass */
  var enterKeyHint: java.lang.String = js.native
  
  /* standard dom */
  /* CompleteClass */
  var innerHTML: java.lang.String = js.native
  
  /* standard dom */
  /* CompleteClass */
  var inputMode: java.lang.String = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val isContentEditable: scala.Boolean = js.native
  
  /**
    * Returns the first following sibling that is an element, and null otherwise.
    */
  /* standard dom */
  /* CompleteClass */
  override val nextElementSibling: org.scalajs.dom.Element | Null = js.native
  
  /**
    * Returns the first preceding sibling that is an element, and null otherwise.
    */
  /* standard dom */
  /* CompleteClass */
  override val previousElementSibling: org.scalajs.dom.Element | Null = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val style: org.scalajs.dom.CSSStyleDeclaration = js.native
}
object HTMLTrackElement {
  
  /* standard dom */
  @JSGlobal("HTMLTrackElement.ERROR")
  @js.native
  val ERROR: Double = js.native
  
  /* standard dom */
  @JSGlobal("HTMLTrackElement.LOADED")
  @js.native
  val LOADED: Double = js.native
  
  /* standard dom */
  @JSGlobal("HTMLTrackElement.LOADING")
  @js.native
  val LOADING: Double = js.native
  
  /* standard dom */
  @JSGlobal("HTMLTrackElement.NONE")
  @js.native
  val NONE: Double = js.native
}
