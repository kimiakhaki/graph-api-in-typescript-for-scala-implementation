package typings.std.global

import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

/* This class was inferred from a value with a constructor. In rare cases (like HTMLElement in the DOM) it might not work as you expect. */
@JSGlobal("PerformanceResourceTiming")
@js.native
/* standard dom */
open class PerformanceResourceTiming ()
  extends StObject
     with typings.std.PerformanceResourceTiming {
  
  /* standard dom */
  /* CompleteClass */
  override val connectEnd: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val connectStart: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val decodedBodySize: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val domainLookupEnd: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val domainLookupStart: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val duration: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val encodedBodySize: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val entryType: java.lang.String = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val fetchStart: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val initiatorType: java.lang.String = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val name: java.lang.String = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val nextHopProtocol: java.lang.String = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val redirectEnd: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val redirectStart: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val requestStart: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val responseEnd: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val responseStart: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val secureConnectionStart: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val startTime: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override def toJSON(): Any = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val transferSize: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val workerStart: Double = js.native
}
