package typings.std.global

import typings.std.MSCredentialType
import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

/* This class was inferred from a value with a constructor. In rare cases (like HTMLElement in the DOM) it might not work as you expect. */
@JSGlobal("MSFIDOSignatureAssertion")
@js.native
/* standard dom */
open class MSFIDOSignatureAssertion ()
  extends StObject
     with typings.std.MSFIDOSignatureAssertion {
  
  /* standard dom */
  /* CompleteClass */
  override val id: java.lang.String = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val signature: typings.std.MSFIDOSignature = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val `type`: MSCredentialType = js.native
}
