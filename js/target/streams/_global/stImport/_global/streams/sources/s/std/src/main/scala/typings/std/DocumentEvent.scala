package typings.std

import typings.std.stdStrings.Events
import typings.std.stdStrings.MouseEvents
import typings.std.stdStrings.MutationEvents
import typings.std.stdStrings.SVGZoomEvents
import typings.std.stdStrings.UIEvents
import typings.std.stdStrings.`VRDisplayEvent `
import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

@js.native
trait DocumentEvent extends StObject {
  
  /* standard dom */
  def createEvent(eventInterface: java.lang.String): org.scalajs.dom.Event = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_AnimationEvent(eventInterface: typings.std.stdStrings.AnimationEvent): org.scalajs.dom.AnimationEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_AnimationPlaybackEvent(eventInterface: typings.std.stdStrings.AnimationPlaybackEvent): AnimationPlaybackEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_AudioProcessingEvent(eventInterface: typings.std.stdStrings.AudioProcessingEvent): AudioProcessingEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_BeforeUnloadEvent(eventInterface: typings.std.stdStrings.BeforeUnloadEvent): org.scalajs.dom.BeforeUnloadEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_ClipboardEvent(eventInterface: typings.std.stdStrings.ClipboardEvent): org.scalajs.dom.ClipboardEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_CloseEvent(eventInterface: typings.std.stdStrings.CloseEvent): org.scalajs.dom.CloseEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_CompositionEvent(eventInterface: typings.std.stdStrings.CompositionEvent): org.scalajs.dom.CompositionEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_CustomEvent(eventInterface: typings.std.stdStrings.CustomEvent): org.scalajs.dom.CustomEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_DeviceMotionEvent(eventInterface: typings.std.stdStrings.DeviceMotionEvent): org.scalajs.dom.DeviceMotionEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_DeviceOrientationEvent(eventInterface: typings.std.stdStrings.DeviceOrientationEvent): org.scalajs.dom.DeviceOrientationEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_DragEvent(eventInterface: typings.std.stdStrings.DragEvent): org.scalajs.dom.DragEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_ErrorEvent(eventInterface: typings.std.stdStrings.ErrorEvent): org.scalajs.dom.ErrorEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_Event(eventInterface: typings.std.stdStrings.Event): org.scalajs.dom.Event = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_Events(eventInterface: Events): org.scalajs.dom.Event = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_FocusEvent(eventInterface: typings.std.stdStrings.FocusEvent): org.scalajs.dom.FocusEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_FocusNavigationEvent(eventInterface: typings.std.stdStrings.FocusNavigationEvent): FocusNavigationEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_GamepadEvent(eventInterface: typings.std.stdStrings.GamepadEvent): org.scalajs.dom.GamepadEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_HashChangeEvent(eventInterface: typings.std.stdStrings.HashChangeEvent): org.scalajs.dom.HashChangeEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_IDBVersionChangeEvent(eventInterface: typings.std.stdStrings.IDBVersionChangeEvent): org.scalajs.dom.IDBVersionChangeEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_InputEvent(eventInterface: typings.std.stdStrings.InputEvent): InputEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_KeyboardEvent(eventInterface: typings.std.stdStrings.KeyboardEvent): org.scalajs.dom.KeyboardEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_ListeningStateChangedEvent(eventInterface: typings.std.stdStrings.ListeningStateChangedEvent): ListeningStateChangedEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_MediaEncryptedEvent(eventInterface: typings.std.stdStrings.MediaEncryptedEvent): MediaEncryptedEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_MediaKeyMessageEvent(eventInterface: typings.std.stdStrings.MediaKeyMessageEvent): MediaKeyMessageEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_MediaQueryListEvent(eventInterface: typings.std.stdStrings.MediaQueryListEvent): MediaQueryListEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_MediaStreamErrorEvent(eventInterface: typings.std.stdStrings.MediaStreamErrorEvent): MediaStreamErrorEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_MediaStreamEvent(eventInterface: typings.std.stdStrings.MediaStreamEvent): MediaStreamEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_MediaStreamTrackEvent(eventInterface: typings.std.stdStrings.MediaStreamTrackEvent): org.scalajs.dom.MediaStreamTrackEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_MessageEvent(eventInterface: typings.std.stdStrings.MessageEvent): org.scalajs.dom.MessageEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_MouseEvent(eventInterface: typings.std.stdStrings.MouseEvent): org.scalajs.dom.MouseEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_MouseEvents(eventInterface: MouseEvents): org.scalajs.dom.MouseEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_MutationEvent(eventInterface: typings.std.stdStrings.MutationEvent): MutationEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_MutationEvents(eventInterface: MutationEvents): MutationEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_OfflineAudioCompletionEvent(eventInterface: typings.std.stdStrings.OfflineAudioCompletionEvent): org.scalajs.dom.OfflineAudioCompletionEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_OverflowEvent(eventInterface: typings.std.stdStrings.OverflowEvent): OverflowEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_PageTransitionEvent(eventInterface: typings.std.stdStrings.PageTransitionEvent): PageTransitionEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_PaymentMethodChangeEvent(eventInterface: typings.std.stdStrings.PaymentMethodChangeEvent): PaymentMethodChangeEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_PaymentRequestUpdateEvent(eventInterface: typings.std.stdStrings.PaymentRequestUpdateEvent): PaymentRequestUpdateEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_PermissionRequestedEvent(eventInterface: typings.std.stdStrings.PermissionRequestedEvent): PermissionRequestedEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_PointerEvent(eventInterface: typings.std.stdStrings.PointerEvent): org.scalajs.dom.PointerEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_PopStateEvent(eventInterface: typings.std.stdStrings.PopStateEvent): org.scalajs.dom.PopStateEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_ProgressEvent(eventInterface: typings.std.stdStrings.ProgressEvent): org.scalajs.dom.ProgressEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_PromiseRejectionEvent(eventInterface: typings.std.stdStrings.PromiseRejectionEvent): PromiseRejectionEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_RTCDTMFToneChangeEvent(eventInterface: typings.std.stdStrings.RTCDTMFToneChangeEvent): RTCDTMFToneChangeEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_RTCDataChannelEvent(eventInterface: typings.std.stdStrings.RTCDataChannelEvent): org.scalajs.dom.RTCDataChannelEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_RTCDtlsTransportStateChangedEvent(eventInterface: typings.std.stdStrings.RTCDtlsTransportStateChangedEvent): RTCDtlsTransportStateChangedEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_RTCErrorEvent(eventInterface: typings.std.stdStrings.RTCErrorEvent): RTCErrorEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_RTCIceCandidatePairChangedEvent(eventInterface: typings.std.stdStrings.RTCIceCandidatePairChangedEvent): RTCIceCandidatePairChangedEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_RTCIceGathererEvent(eventInterface: typings.std.stdStrings.RTCIceGathererEvent): RTCIceGathererEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_RTCIceTransportStateChangedEvent(eventInterface: typings.std.stdStrings.RTCIceTransportStateChangedEvent): RTCIceTransportStateChangedEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_RTCPeerConnectionIceErrorEvent(eventInterface: typings.std.stdStrings.RTCPeerConnectionIceErrorEvent): RTCPeerConnectionIceErrorEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_RTCPeerConnectionIceEvent(eventInterface: typings.std.stdStrings.RTCPeerConnectionIceEvent): org.scalajs.dom.RTCPeerConnectionIceEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_RTCSsrcConflictEvent(eventInterface: typings.std.stdStrings.RTCSsrcConflictEvent): RTCSsrcConflictEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_RTCTrackEvent(eventInterface: typings.std.stdStrings.RTCTrackEvent): RTCTrackEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_SVGZoomEvent(eventInterface: typings.std.stdStrings.SVGZoomEvent): SVGZoomEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_SVGZoomEvents(eventInterface: SVGZoomEvents): SVGZoomEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_SecurityPolicyViolationEvent(eventInterface: typings.std.stdStrings.SecurityPolicyViolationEvent): SecurityPolicyViolationEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_SpeechRecognitionErrorEvent(eventInterface: typings.std.stdStrings.SpeechRecognitionErrorEvent): SpeechRecognitionErrorEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_SpeechRecognitionEvent(eventInterface: typings.std.stdStrings.SpeechRecognitionEvent): SpeechRecognitionEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_SpeechSynthesisErrorEvent(eventInterface: typings.std.stdStrings.SpeechSynthesisErrorEvent): SpeechSynthesisErrorEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_SpeechSynthesisEvent(eventInterface: typings.std.stdStrings.SpeechSynthesisEvent): SpeechSynthesisEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_StorageEvent(eventInterface: typings.std.stdStrings.StorageEvent): org.scalajs.dom.StorageEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_TextEvent(eventInterface: typings.std.stdStrings.TextEvent): org.scalajs.dom.TextEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_TouchEvent(eventInterface: typings.std.stdStrings.TouchEvent): org.scalajs.dom.TouchEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_TrackEvent(eventInterface: typings.std.stdStrings.TrackEvent): org.scalajs.dom.TrackEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_TransitionEvent(eventInterface: typings.std.stdStrings.TransitionEvent): org.scalajs.dom.TransitionEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_UIEvent(eventInterface: typings.std.stdStrings.UIEvent): org.scalajs.dom.UIEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_UIEvents(eventInterface: UIEvents): org.scalajs.dom.UIEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_VRDisplayEvent(eventInterface: typings.std.stdStrings.VRDisplayEvent): VRDisplayEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_VRDisplayEvent(eventInterface: `VRDisplayEvent `): VRDisplayEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_WebGLContextEvent(eventInterface: typings.std.stdStrings.WebGLContextEvent): WebGLContextEvent = js.native
  /* standard dom */
  @JSName("createEvent")
  def createEvent_WheelEvent(eventInterface: typings.std.stdStrings.WheelEvent): org.scalajs.dom.WheelEvent = js.native
}
