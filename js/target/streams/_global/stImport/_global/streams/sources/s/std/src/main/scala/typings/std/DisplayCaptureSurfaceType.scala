package typings.std

import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

/* Rewritten from type alias, can be one of: 
  - typings.std.stdStrings.application
  - typings.std.stdStrings.browser
  - typings.std.stdStrings.monitor
  - typings.std.stdStrings.window
*/
trait DisplayCaptureSurfaceType extends StObject
object DisplayCaptureSurfaceType {
  
  inline def application: typings.std.stdStrings.application = "application".asInstanceOf[typings.std.stdStrings.application]
  
  inline def browser: typings.std.stdStrings.browser = "browser".asInstanceOf[typings.std.stdStrings.browser]
  
  inline def monitor: typings.std.stdStrings.monitor = "monitor".asInstanceOf[typings.std.stdStrings.monitor]
  
  inline def window: typings.std.stdStrings.window = "window".asInstanceOf[typings.std.stdStrings.window]
}
