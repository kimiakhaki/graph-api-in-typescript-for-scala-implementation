package typings.std

import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

@js.native
trait CanvasUserInterface extends StObject {
  
  /* standard dom */
  def drawFocusIfNeeded(element: org.scalajs.dom.Element): Unit = js.native
  /* standard dom */
  def drawFocusIfNeeded(path: Path2D, element: org.scalajs.dom.Element): Unit = js.native
  
  /* standard dom */
  def scrollPathIntoView(): Unit = js.native
  /* standard dom */
  def scrollPathIntoView(path: Path2D): Unit = js.native
}
