package typings.node.NodeJS

import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

/* Rewritten from type alias, can be one of: 
  - typings.node.nodeStrings.aix
  - typings.node.nodeStrings.android
  - typings.node.nodeStrings.darwin
  - typings.node.nodeStrings.freebsd
  - typings.node.nodeStrings.linux
  - typings.node.nodeStrings.openbsd
  - typings.node.nodeStrings.sunos
  - typings.node.nodeStrings.win32
  - typings.node.nodeStrings.cygwin
  - typings.node.nodeStrings.netbsd
*/
trait Platform extends StObject
object Platform {
  
  inline def aix: typings.node.nodeStrings.aix = "aix".asInstanceOf[typings.node.nodeStrings.aix]
  
  inline def android: typings.node.nodeStrings.android = "android".asInstanceOf[typings.node.nodeStrings.android]
  
  inline def cygwin: typings.node.nodeStrings.cygwin = "cygwin".asInstanceOf[typings.node.nodeStrings.cygwin]
  
  inline def darwin: typings.node.nodeStrings.darwin = "darwin".asInstanceOf[typings.node.nodeStrings.darwin]
  
  inline def freebsd: typings.node.nodeStrings.freebsd = "freebsd".asInstanceOf[typings.node.nodeStrings.freebsd]
  
  inline def linux: typings.node.nodeStrings.linux = "linux".asInstanceOf[typings.node.nodeStrings.linux]
  
  inline def netbsd: typings.node.nodeStrings.netbsd = "netbsd".asInstanceOf[typings.node.nodeStrings.netbsd]
  
  inline def openbsd: typings.node.nodeStrings.openbsd = "openbsd".asInstanceOf[typings.node.nodeStrings.openbsd]
  
  inline def sunos: typings.node.nodeStrings.sunos = "sunos".asInstanceOf[typings.node.nodeStrings.sunos]
  
  inline def win32: typings.node.nodeStrings.win32 = "win32".asInstanceOf[typings.node.nodeStrings.win32]
}
