package typings.std.global

import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

/* This class was inferred from a value with a constructor. In rare cases (like HTMLElement in the DOM) it might not work as you expect. */
@JSGlobal("MSFIDOSignature")
@js.native
/* standard dom */
open class MSFIDOSignature ()
  extends StObject
     with typings.std.MSFIDOSignature {
  
  /* standard dom */
  /* CompleteClass */
  override val authnrData: java.lang.String = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val clientData: java.lang.String = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val signature: java.lang.String = js.native
}
