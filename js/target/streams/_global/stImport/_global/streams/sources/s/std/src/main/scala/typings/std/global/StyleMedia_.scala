package typings.std.global

import typings.std.StyleMedia
import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

/* This class was inferred from a value with a constructor. In rare cases (like HTMLElement in the DOM) it might not work as you expect. */
@JSGlobal("StyleMedia")
@js.native
/* standard dom */
open class StyleMedia_ ()
  extends StObject
     with StyleMedia {
  
  /* standard dom */
  /* CompleteClass */
  override def matchMedium(mediaquery: java.lang.String): scala.Boolean = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val `type`: java.lang.String = js.native
}
