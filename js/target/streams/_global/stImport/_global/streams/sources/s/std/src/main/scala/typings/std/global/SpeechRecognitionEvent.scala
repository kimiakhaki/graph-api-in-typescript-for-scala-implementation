package typings.std.global

import typings.std.SpeechRecognitionEventInit
import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

/* This class was inferred from a value with a constructor. In rare cases (like HTMLElement in the DOM) it might not work as you expect. */
@JSGlobal("SpeechRecognitionEvent")
@js.native
open class SpeechRecognitionEvent protected ()
  extends StObject
     with typings.std.SpeechRecognitionEvent {
  /* standard dom */
  def this(`type`: java.lang.String, eventInitDict: SpeechRecognitionEventInit) = this()
}
