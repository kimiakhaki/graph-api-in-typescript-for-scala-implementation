package typings.std.global

import typings.std.NavigationType
import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

/* This class was inferred from a value with a constructor. In rare cases (like HTMLElement in the DOM) it might not work as you expect. */
@JSGlobal("PerformanceNavigationTiming")
@js.native
/* standard dom */
open class PerformanceNavigationTiming ()
  extends StObject
     with typings.std.PerformanceNavigationTiming {
  
  /* standard dom */
  /* CompleteClass */
  override val connectEnd: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val connectStart: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val decodedBodySize: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val domComplete: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val domContentLoadedEventEnd: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val domContentLoadedEventStart: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val domInteractive: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val domainLookupEnd: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val domainLookupStart: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val duration: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val encodedBodySize: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val entryType: java.lang.String = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val fetchStart: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val initiatorType: java.lang.String = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val loadEventEnd: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val loadEventStart: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val name: java.lang.String = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val nextHopProtocol: java.lang.String = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val redirectCount: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val redirectEnd: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val redirectStart: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val requestStart: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val responseEnd: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val responseStart: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val secureConnectionStart: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val startTime: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override def toJSON(): Any = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val transferSize: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val `type`: NavigationType = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val unloadEventEnd: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val unloadEventStart: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val workerStart: Double = js.native
}
