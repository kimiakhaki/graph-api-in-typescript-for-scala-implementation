package typings.node

import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

// Buffer class
/* Rewritten from type alias, can be one of: 
  - typings.node.nodeStrings.ascii
  - typings.node.nodeStrings.utf8
  - typings.node.nodeStrings.`utf-8`
  - typings.node.nodeStrings.utf16le
  - typings.node.nodeStrings.ucs2
  - typings.node.nodeStrings.`ucs-2`
  - typings.node.nodeStrings.base64
  - typings.node.nodeStrings.latin1
  - typings.node.nodeStrings.binary
  - typings.node.nodeStrings.hex
*/
trait BufferEncoding extends StObject
object BufferEncoding {
  
  inline def ascii: typings.node.nodeStrings.ascii = "ascii".asInstanceOf[typings.node.nodeStrings.ascii]
  
  inline def base64: typings.node.nodeStrings.base64 = "base64".asInstanceOf[typings.node.nodeStrings.base64]
  
  inline def binary: typings.node.nodeStrings.binary = "binary".asInstanceOf[typings.node.nodeStrings.binary]
  
  inline def hex: typings.node.nodeStrings.hex = "hex".asInstanceOf[typings.node.nodeStrings.hex]
  
  inline def latin1: typings.node.nodeStrings.latin1 = "latin1".asInstanceOf[typings.node.nodeStrings.latin1]
  
  inline def `ucs-2`: typings.node.nodeStrings.`ucs-2` = "ucs-2".asInstanceOf[typings.node.nodeStrings.`ucs-2`]
  
  inline def ucs2: typings.node.nodeStrings.ucs2 = "ucs2".asInstanceOf[typings.node.nodeStrings.ucs2]
  
  inline def `utf-8`: typings.node.nodeStrings.`utf-8` = "utf-8".asInstanceOf[typings.node.nodeStrings.`utf-8`]
  
  inline def utf16le: typings.node.nodeStrings.utf16le = "utf16le".asInstanceOf[typings.node.nodeStrings.utf16le]
  
  inline def utf8: typings.node.nodeStrings.utf8 = "utf8".asInstanceOf[typings.node.nodeStrings.utf8]
}
