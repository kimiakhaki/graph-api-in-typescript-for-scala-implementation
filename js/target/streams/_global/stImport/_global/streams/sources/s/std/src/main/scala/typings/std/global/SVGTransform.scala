package typings.std.global

import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

/* This class was inferred from a value with a constructor. In rare cases (like HTMLElement in the DOM) it might not work as you expect. */
@JSGlobal("SVGTransform")
@js.native
/* standard dom */
open class SVGTransform ()
  extends StObject
     with typings.std.SVGTransform {
  
  /* standard dom */
  /* CompleteClass */
  override val SVG_TRANSFORM_MATRIX: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val SVG_TRANSFORM_ROTATE: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val SVG_TRANSFORM_SCALE: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val SVG_TRANSFORM_SKEWX: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val SVG_TRANSFORM_SKEWY: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val SVG_TRANSFORM_TRANSLATE: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val SVG_TRANSFORM_UNKNOWN: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val angle: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val matrix: org.scalajs.dom.SVGMatrix = js.native
  
  /* standard dom */
  /* CompleteClass */
  override def setMatrix(matrix: org.scalajs.dom.SVGMatrix): Unit = js.native
  
  /* standard dom */
  /* CompleteClass */
  override def setRotate(angle: Double, cx: Double, cy: Double): Unit = js.native
  
  /* standard dom */
  /* CompleteClass */
  override def setScale(sx: Double, sy: Double): Unit = js.native
  
  /* standard dom */
  /* CompleteClass */
  override def setSkewX(angle: Double): Unit = js.native
  
  /* standard dom */
  /* CompleteClass */
  override def setSkewY(angle: Double): Unit = js.native
  
  /* standard dom */
  /* CompleteClass */
  override def setTranslate(tx: Double, ty: Double): Unit = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val `type`: Double = js.native
}
object SVGTransform {
  
  /* standard dom */
  @JSGlobal("SVGTransform.SVG_TRANSFORM_MATRIX")
  @js.native
  val SVG_TRANSFORM_MATRIX: Double = js.native
  
  /* standard dom */
  @JSGlobal("SVGTransform.SVG_TRANSFORM_ROTATE")
  @js.native
  val SVG_TRANSFORM_ROTATE: Double = js.native
  
  /* standard dom */
  @JSGlobal("SVGTransform.SVG_TRANSFORM_SCALE")
  @js.native
  val SVG_TRANSFORM_SCALE: Double = js.native
  
  /* standard dom */
  @JSGlobal("SVGTransform.SVG_TRANSFORM_SKEWX")
  @js.native
  val SVG_TRANSFORM_SKEWX: Double = js.native
  
  /* standard dom */
  @JSGlobal("SVGTransform.SVG_TRANSFORM_SKEWY")
  @js.native
  val SVG_TRANSFORM_SKEWY: Double = js.native
  
  /* standard dom */
  @JSGlobal("SVGTransform.SVG_TRANSFORM_TRANSLATE")
  @js.native
  val SVG_TRANSFORM_TRANSLATE: Double = js.native
  
  /* standard dom */
  @JSGlobal("SVGTransform.SVG_TRANSFORM_UNKNOWN")
  @js.native
  val SVG_TRANSFORM_UNKNOWN: Double = js.native
}
