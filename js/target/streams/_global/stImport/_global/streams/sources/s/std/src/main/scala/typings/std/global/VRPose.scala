package typings.std.global

import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

/* This class was inferred from a value with a constructor. In rare cases (like HTMLElement in the DOM) it might not work as you expect. */
@JSGlobal("VRPose")
@js.native
/* standard dom */
open class VRPose ()
  extends StObject
     with typings.std.VRPose {
  
  /* standard dom */
  /* CompleteClass */
  override val angularAcceleration: js.typedarray.Float32Array | Null = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val angularVelocity: js.typedarray.Float32Array | Null = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val linearAcceleration: js.typedarray.Float32Array | Null = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val linearVelocity: js.typedarray.Float32Array | Null = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val orientation: js.typedarray.Float32Array | Null = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val position: js.typedarray.Float32Array | Null = js.native
}
