package typings.std.global

import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

/* This class was inferred from a value with a constructor. In rare cases (like HTMLElement in the DOM) it might not work as you expect. */
@JSGlobal("SVGTransformList")
@js.native
/* standard dom */
open class SVGTransformList ()
  extends StObject
     with typings.std.SVGTransformList {
  
  /* standard dom */
  /* CompleteClass */
  override def appendItem(newItem: org.scalajs.dom.SVGTransform): org.scalajs.dom.SVGTransform = js.native
  
  /* standard dom */
  /* CompleteClass */
  override def clear(): Unit = js.native
  
  /* standard dom */
  /* CompleteClass */
  override def consolidate(): org.scalajs.dom.SVGTransform = js.native
  
  /* standard dom */
  /* CompleteClass */
  override def createSVGTransformFromMatrix(matrix: org.scalajs.dom.SVGMatrix): org.scalajs.dom.SVGTransform = js.native
  
  /* standard dom */
  /* CompleteClass */
  override def getItem(index: Double): org.scalajs.dom.SVGTransform = js.native
  
  /* standard dom */
  /* CompleteClass */
  override def initialize(newItem: org.scalajs.dom.SVGTransform): org.scalajs.dom.SVGTransform = js.native
  
  /* standard dom */
  /* CompleteClass */
  override def insertItemBefore(newItem: org.scalajs.dom.SVGTransform, index: Double): org.scalajs.dom.SVGTransform = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val numberOfItems: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override def removeItem(index: Double): org.scalajs.dom.SVGTransform = js.native
  
  /* standard dom */
  /* CompleteClass */
  override def replaceItem(newItem: org.scalajs.dom.SVGTransform, index: Double): org.scalajs.dom.SVGTransform = js.native
}
