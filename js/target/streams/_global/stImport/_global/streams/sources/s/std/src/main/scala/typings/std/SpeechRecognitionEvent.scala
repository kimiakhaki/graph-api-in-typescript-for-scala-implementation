package typings.std

import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

@js.native
trait SpeechRecognitionEvent
  extends StObject
     with Event {
  
  /* standard dom */
  val resultIndex: Double = js.native
  
  /* standard dom */
  val results: SpeechRecognitionResultList = js.native
}
