package typings.node.NodeJS

import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

trait RefCounted extends StObject {
  
  def ref(): this.type
  
  def unref(): this.type
}
object RefCounted {
  
  inline def apply(ref: () => RefCounted, unref: () => RefCounted): RefCounted = {
    val __obj = js.Dynamic.literal(ref = js.Any.fromFunction0(ref), unref = js.Any.fromFunction0(unref))
    __obj.asInstanceOf[RefCounted]
  }
  
  @scala.inline
  implicit open class MutableBuilder[Self <: RefCounted] (val x: Self) extends AnyVal {
    
    inline def setRef(value: () => RefCounted): Self = StObject.set(x, "ref", js.Any.fromFunction0(value))
    
    inline def setUnref(value: () => RefCounted): Self = StObject.set(x, "unref", js.Any.fromFunction0(value))
  }
}
