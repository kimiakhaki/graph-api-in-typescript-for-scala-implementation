package typings.std.global

import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

/* This class was inferred from a value with a constructor. In rare cases (like HTMLElement in the DOM) it might not work as you expect. */
@JSGlobal("ClientRect")
@js.native
/* standard dom */
open class ClientRect ()
  extends StObject
     with typings.std.ClientRect {
  
  /* standard dom */
  /* CompleteClass */
  var bottom: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val height: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  var left: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  var right: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  var top: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val width: Double = js.native
}
