package typings.node.inspectorMod

import typings.node.inspectorMod.^
import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}


/**
  * Deactivate the inspector. Blocks until there are no active connections.
  */
inline def close(): Unit = ^.asInstanceOf[js.Dynamic].applyDynamic("close")().asInstanceOf[Unit]

// Top Level API
/**
  * Activate inspector on host and port. Equivalent to node --inspect=[[host:]port], but can be done programatically after node has started.
  * If wait is true, will block until a client has connected to the inspect port and flow control has been passed to the debugger client.
  * @param port Port to listen on for inspector connections. Optional, defaults to what was specified on the CLI.
  * @param host Host to listen on for inspector connections. Optional, defaults to what was specified on the CLI.
  * @param wait Block until a client has connected. Optional, defaults to false.
  */
inline def open(): Unit = ^.asInstanceOf[js.Dynamic].applyDynamic("open")().asInstanceOf[Unit]
inline def open(port: Double): Unit = ^.asInstanceOf[js.Dynamic].applyDynamic("open")(port.asInstanceOf[js.Any]).asInstanceOf[Unit]
inline def open(port: Double, host: String): Unit = (^.asInstanceOf[js.Dynamic].applyDynamic("open")(port.asInstanceOf[js.Any], host.asInstanceOf[js.Any])).asInstanceOf[Unit]
inline def open(port: Double, host: String, wait: Boolean): Unit = (^.asInstanceOf[js.Dynamic].applyDynamic("open")(port.asInstanceOf[js.Any], host.asInstanceOf[js.Any], wait.asInstanceOf[js.Any])).asInstanceOf[Unit]
inline def open(port: Double, host: Unit, wait: Boolean): Unit = (^.asInstanceOf[js.Dynamic].applyDynamic("open")(port.asInstanceOf[js.Any], host.asInstanceOf[js.Any], wait.asInstanceOf[js.Any])).asInstanceOf[Unit]
inline def open(port: Unit, host: String): Unit = (^.asInstanceOf[js.Dynamic].applyDynamic("open")(port.asInstanceOf[js.Any], host.asInstanceOf[js.Any])).asInstanceOf[Unit]
inline def open(port: Unit, host: String, wait: Boolean): Unit = (^.asInstanceOf[js.Dynamic].applyDynamic("open")(port.asInstanceOf[js.Any], host.asInstanceOf[js.Any], wait.asInstanceOf[js.Any])).asInstanceOf[Unit]
inline def open(port: Unit, host: Unit, wait: Boolean): Unit = (^.asInstanceOf[js.Dynamic].applyDynamic("open")(port.asInstanceOf[js.Any], host.asInstanceOf[js.Any], wait.asInstanceOf[js.Any])).asInstanceOf[Unit]

/**
  * Return the URL of the active inspector, or `undefined` if there is none.
  */
inline def url(): js.UndefOr[String] = ^.asInstanceOf[js.Dynamic].applyDynamic("url")().asInstanceOf[js.UndefOr[String]]
