package typings.std.global

import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

/* This class was inferred from a value with a constructor. In rare cases (like HTMLElement in the DOM) it might not work as you expect. */
@JSGlobal("MediaStreamError")
@js.native
/* standard dom */
open class MediaStreamError ()
  extends StObject
     with typings.std.MediaStreamError {
  
  /* standard dom */
  /* CompleteClass */
  override val constraintName: java.lang.String | Null = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val message: java.lang.String | Null = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val name: java.lang.String = js.native
}
