package typings.node.NodeJS

import typings.node.nodeStrings.get
import typings.node.nodeStrings.set
import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

trait InspectOptions extends StObject {
  
  var breakLength: js.UndefOr[Double] = js.undefined
  
  var colors: js.UndefOr[Boolean] = js.undefined
  
  /**
    * Setting this to `false` causes each object key
    * to be displayed on a new line. It will also add new lines to text that is
    * longer than `breakLength`. If set to a number, the most `n` inner elements
    * are united on a single line as long as all properties fit into
    * `breakLength`. Short array elements are also grouped together. Note that no
    * text will be reduced below 16 characters, no matter the `breakLength` size.
    * For more information, see the example below.
    * @default `true`
    */
  var compact: js.UndefOr[Boolean | Double] = js.undefined
  
  var customInspect: js.UndefOr[Boolean] = js.undefined
  
  /**
    * @default 2
    */
  var depth: js.UndefOr[Double | Null] = js.undefined
  
  /**
    * If set to `true`, getters are going to be
    * inspected as well. If set to `'get'` only getters without setter are going
    * to be inspected. If set to `'set'` only getters having a corresponding
    * setter are going to be inspected. This might cause side effects depending on
    * the getter function.
    * @default `false`
    */
  var getters: js.UndefOr[get | set | Boolean] = js.undefined
  
  var maxArrayLength: js.UndefOr[Double | Null] = js.undefined
  
  /**
    * Specifies the maximum number of characters to
    * include when formatting. Set to `null` or `Infinity` to show all elements.
    * Set to `0` or negative to show no characters.
    * @default Infinity
    */
  var maxStringLength: js.UndefOr[Double | Null] = js.undefined
  
  var showHidden: js.UndefOr[Boolean] = js.undefined
  
  var showProxy: js.UndefOr[Boolean] = js.undefined
  
  var sorted: js.UndefOr[Boolean | (js.Function2[/* a */ String, /* b */ String, Double])] = js.undefined
}
object InspectOptions {
  
  inline def apply(): InspectOptions = {
    val __obj = js.Dynamic.literal()
    __obj.asInstanceOf[InspectOptions]
  }
  
  @scala.inline
  implicit open class MutableBuilder[Self <: InspectOptions] (val x: Self) extends AnyVal {
    
    inline def setBreakLength(value: Double): Self = StObject.set(x, "breakLength", value.asInstanceOf[js.Any])
    
    inline def setBreakLengthUndefined: Self = StObject.set(x, "breakLength", js.undefined)
    
    inline def setColors(value: Boolean): Self = StObject.set(x, "colors", value.asInstanceOf[js.Any])
    
    inline def setColorsUndefined: Self = StObject.set(x, "colors", js.undefined)
    
    inline def setCompact(value: Boolean | Double): Self = StObject.set(x, "compact", value.asInstanceOf[js.Any])
    
    inline def setCompactUndefined: Self = StObject.set(x, "compact", js.undefined)
    
    inline def setCustomInspect(value: Boolean): Self = StObject.set(x, "customInspect", value.asInstanceOf[js.Any])
    
    inline def setCustomInspectUndefined: Self = StObject.set(x, "customInspect", js.undefined)
    
    inline def setDepth(value: Double): Self = StObject.set(x, "depth", value.asInstanceOf[js.Any])
    
    inline def setDepthNull: Self = StObject.set(x, "depth", null)
    
    inline def setDepthUndefined: Self = StObject.set(x, "depth", js.undefined)
    
    inline def setGetters(value: get | set | Boolean): Self = StObject.set(x, "getters", value.asInstanceOf[js.Any])
    
    inline def setGettersUndefined: Self = StObject.set(x, "getters", js.undefined)
    
    inline def setMaxArrayLength(value: Double): Self = StObject.set(x, "maxArrayLength", value.asInstanceOf[js.Any])
    
    inline def setMaxArrayLengthNull: Self = StObject.set(x, "maxArrayLength", null)
    
    inline def setMaxArrayLengthUndefined: Self = StObject.set(x, "maxArrayLength", js.undefined)
    
    inline def setMaxStringLength(value: Double): Self = StObject.set(x, "maxStringLength", value.asInstanceOf[js.Any])
    
    inline def setMaxStringLengthNull: Self = StObject.set(x, "maxStringLength", null)
    
    inline def setMaxStringLengthUndefined: Self = StObject.set(x, "maxStringLength", js.undefined)
    
    inline def setShowHidden(value: Boolean): Self = StObject.set(x, "showHidden", value.asInstanceOf[js.Any])
    
    inline def setShowHiddenUndefined: Self = StObject.set(x, "showHidden", js.undefined)
    
    inline def setShowProxy(value: Boolean): Self = StObject.set(x, "showProxy", value.asInstanceOf[js.Any])
    
    inline def setShowProxyUndefined: Self = StObject.set(x, "showProxy", js.undefined)
    
    inline def setSorted(value: Boolean | (js.Function2[/* a */ String, /* b */ String, Double])): Self = StObject.set(x, "sorted", value.asInstanceOf[js.Any])
    
    inline def setSortedFunction2(value: (/* a */ String, /* b */ String) => Double): Self = StObject.set(x, "sorted", js.Any.fromFunction2(value))
    
    inline def setSortedUndefined: Self = StObject.set(x, "sorted", js.undefined)
  }
}
