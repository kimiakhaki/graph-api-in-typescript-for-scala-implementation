package typings.std

import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

/* Rewritten from type alias, can be one of: 
  - typings.std.stdStrings.all
  - typings.std.stdStrings.nohost
  - typings.std.stdStrings.relay
*/
trait RTCIceGatherPolicy extends StObject
object RTCIceGatherPolicy {
  
  inline def all: typings.std.stdStrings.all = "all".asInstanceOf[typings.std.stdStrings.all]
  
  inline def nohost: typings.std.stdStrings.nohost = "nohost".asInstanceOf[typings.std.stdStrings.nohost]
  
  inline def relay: typings.std.stdStrings.relay = "relay".asInstanceOf[typings.std.stdStrings.relay]
}
