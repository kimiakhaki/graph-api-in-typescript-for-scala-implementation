package typings.node

import typings.node.NodeJS.Timer
import typings.node.eventsMod.EventEmitter
import typings.node.eventsMod.EventEmitterOptions
import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

object domainMod {
  
  @JSImport("domain", JSImport.Namespace)
  @js.native
  val ^ : js.Any = js.native
  
  @JSImport("domain", "Domain")
  @js.native
  open class Domain ()
    extends EventEmitter
       with typings.node.NodeJS.Domain {
    def this(options: EventEmitterOptions) = this()
    
    def add(emitter: EventEmitter): Unit = js.native
    
    def enter(): Unit = js.native
    
    def exit(): Unit = js.native
    
    var members: js.Array[EventEmitter | Timer] = js.native
    
    def remove(emitter: EventEmitter): Unit = js.native
  }
  
  inline def create(): Domain = ^.asInstanceOf[js.Dynamic].applyDynamic("create")().asInstanceOf[Domain]
}
