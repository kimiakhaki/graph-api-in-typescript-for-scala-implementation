package typings.std

import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

@js.native
trait SpeechRecognitionErrorEvent
  extends StObject
     with Event {
  
  /* standard dom */
  val error: SpeechRecognitionErrorCode = js.native
  
  /* standard dom */
  val message: java.lang.String = js.native
}
