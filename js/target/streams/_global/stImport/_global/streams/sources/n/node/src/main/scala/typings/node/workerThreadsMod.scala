package typings.node

import typings.node.NodeJS.Dict
import typings.node.eventsMod.EventEmitter
import typings.node.eventsMod.EventEmitterOptions
import typings.node.nodeStrings.close
import typings.node.nodeStrings.error
import typings.node.nodeStrings.exit
import typings.node.nodeStrings.message
import typings.node.nodeStrings.online
import typings.node.streamMod.Readable
import typings.node.streamMod.Writable
import typings.node.urlMod.URL_
import typings.node.vmMod.Context
import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

object workerThreadsMod {
  
  @JSImport("worker_threads", "MessageChannel")
  @js.native
  open class MessageChannel () extends StObject {
    
    val port1: MessagePort = js.native
    
    val port2: MessagePort = js.native
  }
  
  @JSImport("worker_threads", "MessagePort")
  @js.native
  open class MessagePort () extends EventEmitter {
    def this(options: EventEmitterOptions) = this()
    
    @JSName("addListener")
    def addListener_close(event: close, listener: js.Function0[Unit]): this.type = js.native
    @JSName("addListener")
    def addListener_message(event: message, listener: js.Function1[/* value */ Any, Unit]): this.type = js.native
    
    def close(): Unit = js.native
    
    @JSName("emit")
    def emit_close(event: close): Boolean = js.native
    @JSName("emit")
    def emit_message(event: message, value: Any): Boolean = js.native
    
    @JSName("off")
    def off_close(event: close, listener: js.Function0[Unit]): this.type = js.native
    @JSName("off")
    def off_message(event: message, listener: js.Function1[/* value */ Any, Unit]): this.type = js.native
    
    @JSName("on")
    def on_close(event: close, listener: js.Function0[Unit]): this.type = js.native
    @JSName("on")
    def on_message(event: message, listener: js.Function1[/* value */ Any, Unit]): this.type = js.native
    
    @JSName("once")
    def once_close(event: close, listener: js.Function0[Unit]): this.type = js.native
    @JSName("once")
    def once_message(event: message, listener: js.Function1[/* value */ Any, Unit]): this.type = js.native
    
    def postMessage(value: Any): Unit = js.native
    def postMessage(value: Any, transferList: js.Array[js.typedarray.ArrayBuffer | MessagePort]): Unit = js.native
    
    @JSName("prependListener")
    def prependListener_close(event: close, listener: js.Function0[Unit]): this.type = js.native
    @JSName("prependListener")
    def prependListener_message(event: message, listener: js.Function1[/* value */ Any, Unit]): this.type = js.native
    
    @JSName("prependOnceListener")
    def prependOnceListener_close(event: close, listener: js.Function0[Unit]): this.type = js.native
    @JSName("prependOnceListener")
    def prependOnceListener_message(event: message, listener: js.Function1[/* value */ Any, Unit]): this.type = js.native
    
    def ref(): Unit = js.native
    
    @JSName("removeListener")
    def removeListener_close(event: close, listener: js.Function0[Unit]): this.type = js.native
    @JSName("removeListener")
    def removeListener_message(event: message, listener: js.Function1[/* value */ Any, Unit]): this.type = js.native
    
    def start(): Unit = js.native
    
    def unref(): Unit = js.native
  }
  
  @JSImport("worker_threads", "SHARE_ENV")
  @js.native
  val SHARE_ENV: js.Symbol = js.native
  
  @JSImport("worker_threads", "Worker")
  @js.native
  open class Worker protected () extends EventEmitter {
    /**
      * @param filename  The path to the Worker’s main script or module.
      *                  Must be either an absolute path or a relative path (i.e. relative to the current working directory) starting with ./ or ../,
      *                  or a WHATWG URL object using file: protocol. If options.eval is true, this is a string containing JavaScript code rather than a path.
      */
    def this(filename: java.lang.String) = this()
    def this(filename: URL_) = this()
    def this(filename: java.lang.String, options: WorkerOptions) = this()
    def this(filename: URL_, options: WorkerOptions) = this()
    
    @JSName("addListener")
    def addListener_error(event: error, listener: js.Function1[/* err */ js.Error, Unit]): this.type = js.native
    @JSName("addListener")
    def addListener_exit(event: exit, listener: js.Function1[/* exitCode */ Double, Unit]): this.type = js.native
    @JSName("addListener")
    def addListener_message(event: message, listener: js.Function1[/* value */ Any, Unit]): this.type = js.native
    @JSName("addListener")
    def addListener_online(event: online, listener: js.Function0[Unit]): this.type = js.native
    
    @JSName("emit")
    def emit_error(event: error, err: js.Error): Boolean = js.native
    @JSName("emit")
    def emit_exit(event: exit, exitCode: Double): Boolean = js.native
    @JSName("emit")
    def emit_message(event: message, value: Any): Boolean = js.native
    @JSName("emit")
    def emit_online(event: online): Boolean = js.native
    
    /**
      * Returns a readable stream for a V8 snapshot of the current state of the Worker.
      * See [`v8.getHeapSnapshot()`][] for more details.
      *
      * If the Worker thread is no longer running, which may occur before the
      * [`'exit'` event][] is emitted, the returned `Promise` will be rejected
      * immediately with an [`ERR_WORKER_NOT_RUNNING`][] error
      */
    def getHeapSnapshot(): js.Promise[Readable] = js.native
    
    /**
      * Transfer a `MessagePort` to a different `vm` Context. The original `port`
      * object will be rendered unusable, and the returned `MessagePort` instance will
      * take its place.
      *
      * The returned `MessagePort` will be an object in the target context, and will
      * inherit from its global `Object` class. Objects passed to the
      * `port.onmessage()` listener will also be created in the target context
      * and inherit from its global `Object` class.
      *
      * However, the created `MessagePort` will no longer inherit from
      * `EventEmitter`, and only `port.onmessage()` can be used to receive
      * events using it.
      */
    def moveMessagePortToContext(port: MessagePort, context: Context): MessagePort = js.native
    
    @JSName("off")
    def off_error(event: error, listener: js.Function1[/* err */ js.Error, Unit]): this.type = js.native
    @JSName("off")
    def off_exit(event: exit, listener: js.Function1[/* exitCode */ Double, Unit]): this.type = js.native
    @JSName("off")
    def off_message(event: message, listener: js.Function1[/* value */ Any, Unit]): this.type = js.native
    @JSName("off")
    def off_online(event: online, listener: js.Function0[Unit]): this.type = js.native
    
    @JSName("on")
    def on_error(event: error, listener: js.Function1[/* err */ js.Error, Unit]): this.type = js.native
    @JSName("on")
    def on_exit(event: exit, listener: js.Function1[/* exitCode */ Double, Unit]): this.type = js.native
    @JSName("on")
    def on_message(event: message, listener: js.Function1[/* value */ Any, Unit]): this.type = js.native
    @JSName("on")
    def on_online(event: online, listener: js.Function0[Unit]): this.type = js.native
    
    @JSName("once")
    def once_error(event: error, listener: js.Function1[/* err */ js.Error, Unit]): this.type = js.native
    @JSName("once")
    def once_exit(event: exit, listener: js.Function1[/* exitCode */ Double, Unit]): this.type = js.native
    @JSName("once")
    def once_message(event: message, listener: js.Function1[/* value */ Any, Unit]): this.type = js.native
    @JSName("once")
    def once_online(event: online, listener: js.Function0[Unit]): this.type = js.native
    
    def postMessage(value: Any): Unit = js.native
    def postMessage(value: Any, transferList: js.Array[js.typedarray.ArrayBuffer | MessagePort]): Unit = js.native
    
    @JSName("prependListener")
    def prependListener_error(event: error, listener: js.Function1[/* err */ js.Error, Unit]): this.type = js.native
    @JSName("prependListener")
    def prependListener_exit(event: exit, listener: js.Function1[/* exitCode */ Double, Unit]): this.type = js.native
    @JSName("prependListener")
    def prependListener_message(event: message, listener: js.Function1[/* value */ Any, Unit]): this.type = js.native
    @JSName("prependListener")
    def prependListener_online(event: online, listener: js.Function0[Unit]): this.type = js.native
    
    @JSName("prependOnceListener")
    def prependOnceListener_error(event: error, listener: js.Function1[/* err */ js.Error, Unit]): this.type = js.native
    @JSName("prependOnceListener")
    def prependOnceListener_exit(event: exit, listener: js.Function1[/* exitCode */ Double, Unit]): this.type = js.native
    @JSName("prependOnceListener")
    def prependOnceListener_message(event: message, listener: js.Function1[/* value */ Any, Unit]): this.type = js.native
    @JSName("prependOnceListener")
    def prependOnceListener_online(event: online, listener: js.Function0[Unit]): this.type = js.native
    
    /**
      * Receive a single message from a given `MessagePort`. If no message is available,
      * `undefined` is returned, otherwise an object with a single `message` property
      * that contains the message payload, corresponding to the oldest message in the
      * `MessagePort`’s queue.
      */
    def receiveMessageOnPort(port: MessagePort): js.UndefOr[js.Object] = js.native
    
    def ref(): Unit = js.native
    
    @JSName("removeListener")
    def removeListener_error(event: error, listener: js.Function1[/* err */ js.Error, Unit]): this.type = js.native
    @JSName("removeListener")
    def removeListener_exit(event: exit, listener: js.Function1[/* exitCode */ Double, Unit]): this.type = js.native
    @JSName("removeListener")
    def removeListener_message(event: message, listener: js.Function1[/* value */ Any, Unit]): this.type = js.native
    @JSName("removeListener")
    def removeListener_online(event: online, listener: js.Function0[Unit]): this.type = js.native
    
    val resourceLimits: js.UndefOr[ResourceLimits] = js.native
    
    val stderr: Readable = js.native
    
    val stdin: Writable | Null = js.native
    
    val stdout: Readable = js.native
    
    /**
      * Stop all JavaScript execution in the worker thread as soon as possible.
      * Returns a Promise for the exit code that is fulfilled when the `exit` event is emitted.
      */
    def terminate(): js.Promise[Double] = js.native
    
    val threadId: Double = js.native
    
    def unref(): Unit = js.native
  }
  
  @JSImport("worker_threads", "isMainThread")
  @js.native
  val isMainThread: Boolean = js.native
  
  @JSImport("worker_threads", "parentPort")
  @js.native
  val parentPort: Null | MessagePort = js.native
  
  @JSImport("worker_threads", "threadId")
  @js.native
  val threadId: Double = js.native
  
  @JSImport("worker_threads", "workerData")
  @js.native
  val workerData: Any = js.native
  
  trait ResourceLimits extends StObject {
    
    var codeRangeSizeMb: js.UndefOr[Double] = js.undefined
    
    var maxOldGenerationSizeMb: js.UndefOr[Double] = js.undefined
    
    var maxYoungGenerationSizeMb: js.UndefOr[Double] = js.undefined
  }
  object ResourceLimits {
    
    inline def apply(): ResourceLimits = {
      val __obj = js.Dynamic.literal()
      __obj.asInstanceOf[ResourceLimits]
    }
    
    @scala.inline
    implicit open class MutableBuilder[Self <: ResourceLimits] (val x: Self) extends AnyVal {
      
      inline def setCodeRangeSizeMb(value: Double): Self = StObject.set(x, "codeRangeSizeMb", value.asInstanceOf[js.Any])
      
      inline def setCodeRangeSizeMbUndefined: Self = StObject.set(x, "codeRangeSizeMb", js.undefined)
      
      inline def setMaxOldGenerationSizeMb(value: Double): Self = StObject.set(x, "maxOldGenerationSizeMb", value.asInstanceOf[js.Any])
      
      inline def setMaxOldGenerationSizeMbUndefined: Self = StObject.set(x, "maxOldGenerationSizeMb", js.undefined)
      
      inline def setMaxYoungGenerationSizeMb(value: Double): Self = StObject.set(x, "maxYoungGenerationSizeMb", value.asInstanceOf[js.Any])
      
      inline def setMaxYoungGenerationSizeMbUndefined: Self = StObject.set(x, "maxYoungGenerationSizeMb", js.undefined)
    }
  }
  
  trait WorkerOptions extends StObject {
    
    /**
      * List of arguments which would be stringified and appended to
      * `process.argv` in the worker. This is mostly similar to the `workerData`
      * but the values will be available on the global `process.argv` as if they
      * were passed as CLI options to the script.
      */
    var argv: js.UndefOr[js.Array[Any]] = js.undefined
    
    var env: js.UndefOr[Dict[java.lang.String] | js.Symbol] = js.undefined
    
    var eval: js.UndefOr[Boolean] = js.undefined
    
    var execArgv: js.UndefOr[js.Array[java.lang.String]] = js.undefined
    
    var resourceLimits: js.UndefOr[ResourceLimits] = js.undefined
    
    var stderr: js.UndefOr[Boolean] = js.undefined
    
    var stdin: js.UndefOr[Boolean] = js.undefined
    
    var stdout: js.UndefOr[Boolean] = js.undefined
    
    /**
      * Additional data to send in the first worker message.
      */
    var transferList: js.UndefOr[js.Array[js.typedarray.ArrayBuffer | MessagePort]] = js.undefined
    
    var workerData: js.UndefOr[Any] = js.undefined
  }
  object WorkerOptions {
    
    inline def apply(): WorkerOptions = {
      val __obj = js.Dynamic.literal()
      __obj.asInstanceOf[WorkerOptions]
    }
    
    @scala.inline
    implicit open class MutableBuilder[Self <: WorkerOptions] (val x: Self) extends AnyVal {
      
      inline def setArgv(value: js.Array[Any]): Self = StObject.set(x, "argv", value.asInstanceOf[js.Any])
      
      inline def setArgvUndefined: Self = StObject.set(x, "argv", js.undefined)
      
      inline def setArgvVarargs(value: Any*): Self = StObject.set(x, "argv", js.Array(value*))
      
      inline def setEnv(value: Dict[java.lang.String] | js.Symbol): Self = StObject.set(x, "env", value.asInstanceOf[js.Any])
      
      inline def setEnvUndefined: Self = StObject.set(x, "env", js.undefined)
      
      inline def setEval(value: Boolean): Self = StObject.set(x, "eval", value.asInstanceOf[js.Any])
      
      inline def setEvalUndefined: Self = StObject.set(x, "eval", js.undefined)
      
      inline def setExecArgv(value: js.Array[java.lang.String]): Self = StObject.set(x, "execArgv", value.asInstanceOf[js.Any])
      
      inline def setExecArgvUndefined: Self = StObject.set(x, "execArgv", js.undefined)
      
      inline def setExecArgvVarargs(value: java.lang.String*): Self = StObject.set(x, "execArgv", js.Array(value*))
      
      inline def setResourceLimits(value: ResourceLimits): Self = StObject.set(x, "resourceLimits", value.asInstanceOf[js.Any])
      
      inline def setResourceLimitsUndefined: Self = StObject.set(x, "resourceLimits", js.undefined)
      
      inline def setStderr(value: Boolean): Self = StObject.set(x, "stderr", value.asInstanceOf[js.Any])
      
      inline def setStderrUndefined: Self = StObject.set(x, "stderr", js.undefined)
      
      inline def setStdin(value: Boolean): Self = StObject.set(x, "stdin", value.asInstanceOf[js.Any])
      
      inline def setStdinUndefined: Self = StObject.set(x, "stdin", js.undefined)
      
      inline def setStdout(value: Boolean): Self = StObject.set(x, "stdout", value.asInstanceOf[js.Any])
      
      inline def setStdoutUndefined: Self = StObject.set(x, "stdout", js.undefined)
      
      inline def setTransferList(value: js.Array[js.typedarray.ArrayBuffer | MessagePort]): Self = StObject.set(x, "transferList", value.asInstanceOf[js.Any])
      
      inline def setTransferListUndefined: Self = StObject.set(x, "transferList", js.undefined)
      
      inline def setTransferListVarargs(value: (js.typedarray.ArrayBuffer | MessagePort)*): Self = StObject.set(x, "transferList", js.Array(value*))
      
      inline def setWorkerData(value: Any): Self = StObject.set(x, "workerData", value.asInstanceOf[js.Any])
      
      inline def setWorkerDataUndefined: Self = StObject.set(x, "workerData", js.undefined)
    }
  }
}
