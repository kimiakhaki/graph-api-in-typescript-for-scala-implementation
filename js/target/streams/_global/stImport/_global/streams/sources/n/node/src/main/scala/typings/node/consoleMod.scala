package typings.node

import org.scalablytyped.runtime.Shortcut
import typings.node.NodeJS.ConsoleConstructorOptions
import typings.node.NodeJS.WritableStream
import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

object consoleMod extends Shortcut {
  
  @JSImport("console", JSImport.Namespace)
  @js.native
  val ^ : typings.node.Console = js.native
  
  /* This class was inferred from a value with a constructor. In rare cases (like HTMLElement in the DOM) it might not work as you expect. */
  @JSImport("console", "Console")
  @js.native
  open class Console protected ()
    extends StObject
       with typings.node.Console {
    def this(options: ConsoleConstructorOptions) = this()
    def this(stdout: WritableStream) = this()
    def this(stdout: WritableStream, stderr: WritableStream) = this()
    def this(stdout: WritableStream, stderr: Unit, ignoreErrors: Boolean) = this()
    def this(stdout: WritableStream, stderr: WritableStream, ignoreErrors: Boolean) = this()
  }
  
  type _To = typings.node.Console
  
  /* This means you don't have to write `^`, but can instead just say `consoleMod.foo` */
  override def _to: typings.node.Console = ^
}
