package typings.std

import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

/* Rewritten from type alias, can be one of: 
  - typings.std.stdStrings.geolocation
  - typings.std.stdStrings.media
  - typings.std.stdStrings.pointerlock
  - typings.std.stdStrings.unlimitedIndexedDBQuota
  - typings.std.stdStrings.webnotifications
*/
trait MSWebViewPermissionType extends StObject
object MSWebViewPermissionType {
  
  inline def geolocation: typings.std.stdStrings.geolocation = "geolocation".asInstanceOf[typings.std.stdStrings.geolocation]
  
  inline def media: typings.std.stdStrings.media = "media".asInstanceOf[typings.std.stdStrings.media]
  
  inline def pointerlock: typings.std.stdStrings.pointerlock = "pointerlock".asInstanceOf[typings.std.stdStrings.pointerlock]
  
  inline def unlimitedIndexedDBQuota: typings.std.stdStrings.unlimitedIndexedDBQuota = "unlimitedIndexedDBQuota".asInstanceOf[typings.std.stdStrings.unlimitedIndexedDBQuota]
  
  inline def webnotifications: typings.std.stdStrings.webnotifications = "webnotifications".asInstanceOf[typings.std.stdStrings.webnotifications]
}
