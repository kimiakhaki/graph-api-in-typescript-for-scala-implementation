package typings.node.NodeJS

import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

/* Rewritten from type alias, can be one of: 
  - typings.node.nodeStrings.SIGABRT
  - typings.node.nodeStrings.SIGALRM
  - typings.node.nodeStrings.SIGBUS
  - typings.node.nodeStrings.SIGCHLD
  - typings.node.nodeStrings.SIGCONT
  - typings.node.nodeStrings.SIGFPE
  - typings.node.nodeStrings.SIGHUP
  - typings.node.nodeStrings.SIGILL
  - typings.node.nodeStrings.SIGINT
  - typings.node.nodeStrings.SIGIO
  - typings.node.nodeStrings.SIGIOT
  - typings.node.nodeStrings.SIGKILL
  - typings.node.nodeStrings.SIGPIPE
  - typings.node.nodeStrings.SIGPOLL
  - typings.node.nodeStrings.SIGPROF
  - typings.node.nodeStrings.SIGPWR
  - typings.node.nodeStrings.SIGQUIT
  - typings.node.nodeStrings.SIGSEGV
  - typings.node.nodeStrings.SIGSTKFLT
  - typings.node.nodeStrings.SIGSTOP
  - typings.node.nodeStrings.SIGSYS
  - typings.node.nodeStrings.SIGTERM
  - typings.node.nodeStrings.SIGTRAP
  - typings.node.nodeStrings.SIGTSTP
  - typings.node.nodeStrings.SIGTTIN
  - typings.node.nodeStrings.SIGTTOU
  - typings.node.nodeStrings.SIGUNUSED
  - typings.node.nodeStrings.SIGURG
  - typings.node.nodeStrings.SIGUSR1
  - typings.node.nodeStrings.SIGUSR2
  - typings.node.nodeStrings.SIGVTALRM
  - typings.node.nodeStrings.SIGWINCH
  - typings.node.nodeStrings.SIGXCPU
  - typings.node.nodeStrings.SIGXFSZ
  - typings.node.nodeStrings.SIGBREAK
  - typings.node.nodeStrings.SIGLOST
  - typings.node.nodeStrings.SIGINFO
*/
trait Signals extends StObject
object Signals {
  
  inline def SIGABRT: typings.node.nodeStrings.SIGABRT = "SIGABRT".asInstanceOf[typings.node.nodeStrings.SIGABRT]
  
  inline def SIGALRM: typings.node.nodeStrings.SIGALRM = "SIGALRM".asInstanceOf[typings.node.nodeStrings.SIGALRM]
  
  inline def SIGBREAK: typings.node.nodeStrings.SIGBREAK = "SIGBREAK".asInstanceOf[typings.node.nodeStrings.SIGBREAK]
  
  inline def SIGBUS: typings.node.nodeStrings.SIGBUS = "SIGBUS".asInstanceOf[typings.node.nodeStrings.SIGBUS]
  
  inline def SIGCHLD: typings.node.nodeStrings.SIGCHLD = "SIGCHLD".asInstanceOf[typings.node.nodeStrings.SIGCHLD]
  
  inline def SIGCONT: typings.node.nodeStrings.SIGCONT = "SIGCONT".asInstanceOf[typings.node.nodeStrings.SIGCONT]
  
  inline def SIGFPE: typings.node.nodeStrings.SIGFPE = "SIGFPE".asInstanceOf[typings.node.nodeStrings.SIGFPE]
  
  inline def SIGHUP: typings.node.nodeStrings.SIGHUP = "SIGHUP".asInstanceOf[typings.node.nodeStrings.SIGHUP]
  
  inline def SIGILL: typings.node.nodeStrings.SIGILL = "SIGILL".asInstanceOf[typings.node.nodeStrings.SIGILL]
  
  inline def SIGINFO: typings.node.nodeStrings.SIGINFO = "SIGINFO".asInstanceOf[typings.node.nodeStrings.SIGINFO]
  
  inline def SIGINT: typings.node.nodeStrings.SIGINT = "SIGINT".asInstanceOf[typings.node.nodeStrings.SIGINT]
  
  inline def SIGIO: typings.node.nodeStrings.SIGIO = "SIGIO".asInstanceOf[typings.node.nodeStrings.SIGIO]
  
  inline def SIGIOT: typings.node.nodeStrings.SIGIOT = "SIGIOT".asInstanceOf[typings.node.nodeStrings.SIGIOT]
  
  inline def SIGKILL: typings.node.nodeStrings.SIGKILL = "SIGKILL".asInstanceOf[typings.node.nodeStrings.SIGKILL]
  
  inline def SIGLOST: typings.node.nodeStrings.SIGLOST = "SIGLOST".asInstanceOf[typings.node.nodeStrings.SIGLOST]
  
  inline def SIGPIPE: typings.node.nodeStrings.SIGPIPE = "SIGPIPE".asInstanceOf[typings.node.nodeStrings.SIGPIPE]
  
  inline def SIGPOLL: typings.node.nodeStrings.SIGPOLL = "SIGPOLL".asInstanceOf[typings.node.nodeStrings.SIGPOLL]
  
  inline def SIGPROF: typings.node.nodeStrings.SIGPROF = "SIGPROF".asInstanceOf[typings.node.nodeStrings.SIGPROF]
  
  inline def SIGPWR: typings.node.nodeStrings.SIGPWR = "SIGPWR".asInstanceOf[typings.node.nodeStrings.SIGPWR]
  
  inline def SIGQUIT: typings.node.nodeStrings.SIGQUIT = "SIGQUIT".asInstanceOf[typings.node.nodeStrings.SIGQUIT]
  
  inline def SIGSEGV: typings.node.nodeStrings.SIGSEGV = "SIGSEGV".asInstanceOf[typings.node.nodeStrings.SIGSEGV]
  
  inline def SIGSTKFLT: typings.node.nodeStrings.SIGSTKFLT = "SIGSTKFLT".asInstanceOf[typings.node.nodeStrings.SIGSTKFLT]
  
  inline def SIGSTOP: typings.node.nodeStrings.SIGSTOP = "SIGSTOP".asInstanceOf[typings.node.nodeStrings.SIGSTOP]
  
  inline def SIGSYS: typings.node.nodeStrings.SIGSYS = "SIGSYS".asInstanceOf[typings.node.nodeStrings.SIGSYS]
  
  inline def SIGTERM: typings.node.nodeStrings.SIGTERM = "SIGTERM".asInstanceOf[typings.node.nodeStrings.SIGTERM]
  
  inline def SIGTRAP: typings.node.nodeStrings.SIGTRAP = "SIGTRAP".asInstanceOf[typings.node.nodeStrings.SIGTRAP]
  
  inline def SIGTSTP: typings.node.nodeStrings.SIGTSTP = "SIGTSTP".asInstanceOf[typings.node.nodeStrings.SIGTSTP]
  
  inline def SIGTTIN: typings.node.nodeStrings.SIGTTIN = "SIGTTIN".asInstanceOf[typings.node.nodeStrings.SIGTTIN]
  
  inline def SIGTTOU: typings.node.nodeStrings.SIGTTOU = "SIGTTOU".asInstanceOf[typings.node.nodeStrings.SIGTTOU]
  
  inline def SIGUNUSED: typings.node.nodeStrings.SIGUNUSED = "SIGUNUSED".asInstanceOf[typings.node.nodeStrings.SIGUNUSED]
  
  inline def SIGURG: typings.node.nodeStrings.SIGURG = "SIGURG".asInstanceOf[typings.node.nodeStrings.SIGURG]
  
  inline def SIGUSR1: typings.node.nodeStrings.SIGUSR1 = "SIGUSR1".asInstanceOf[typings.node.nodeStrings.SIGUSR1]
  
  inline def SIGUSR2: typings.node.nodeStrings.SIGUSR2 = "SIGUSR2".asInstanceOf[typings.node.nodeStrings.SIGUSR2]
  
  inline def SIGVTALRM: typings.node.nodeStrings.SIGVTALRM = "SIGVTALRM".asInstanceOf[typings.node.nodeStrings.SIGVTALRM]
  
  inline def SIGWINCH: typings.node.nodeStrings.SIGWINCH = "SIGWINCH".asInstanceOf[typings.node.nodeStrings.SIGWINCH]
  
  inline def SIGXCPU: typings.node.nodeStrings.SIGXCPU = "SIGXCPU".asInstanceOf[typings.node.nodeStrings.SIGXCPU]
  
  inline def SIGXFSZ: typings.node.nodeStrings.SIGXFSZ = "SIGXFSZ".asInstanceOf[typings.node.nodeStrings.SIGXFSZ]
}
