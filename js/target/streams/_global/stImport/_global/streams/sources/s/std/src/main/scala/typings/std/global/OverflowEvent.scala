package typings.std.global

import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

/* This class was inferred from a value with a constructor. In rare cases (like HTMLElement in the DOM) it might not work as you expect. */
@JSGlobal("OverflowEvent")
@js.native
/* standard dom */
open class OverflowEvent ()
  extends StObject
     with typings.std.OverflowEvent
object OverflowEvent {
  
  /* standard dom */
  @JSGlobal("OverflowEvent.BOTH")
  @js.native
  val BOTH: Double = js.native
  
  /* standard dom */
  @JSGlobal("OverflowEvent.HORIZONTAL")
  @js.native
  val HORIZONTAL: Double = js.native
  
  /* standard dom */
  @JSGlobal("OverflowEvent.VERTICAL")
  @js.native
  val VERTICAL: Double = js.native
}
