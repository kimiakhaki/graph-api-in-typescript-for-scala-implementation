package typings.std.global

import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

/* This class was inferred from a value with a constructor. In rare cases (like HTMLElement in the DOM) it might not work as you expect. */
@JSGlobal("VRDisplayCapabilities")
@js.native
/* standard dom */
open class VRDisplayCapabilities ()
  extends StObject
     with typings.std.VRDisplayCapabilities {
  
  /* standard dom */
  /* CompleteClass */
  override val canPresent: scala.Boolean = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val hasExternalDisplay: scala.Boolean = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val hasOrientation: scala.Boolean = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val hasPosition: scala.Boolean = js.native
  
  /* standard dom */
  /* CompleteClass */
  override val maxLayers: Double = js.native
}
