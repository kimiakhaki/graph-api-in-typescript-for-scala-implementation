package typings.node

import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

// Node.js ESNEXT support
trait String extends StObject {
  
  /** Removes whitespace from the left end of a string. */
  def trimLeft(): java.lang.String
  
  /** Removes whitespace from the right end of a string. */
  def trimRight(): java.lang.String
}
object String {
  
  inline def apply(trimLeft: () => java.lang.String, trimRight: () => java.lang.String): String = {
    val __obj = js.Dynamic.literal(trimLeft = js.Any.fromFunction0(trimLeft), trimRight = js.Any.fromFunction0(trimRight))
    __obj.asInstanceOf[String]
  }
  
  @scala.inline
  implicit open class MutableBuilder[Self <: String] (val x: Self) extends AnyVal {
    
    inline def setTrimLeft(value: () => java.lang.String): Self = StObject.set(x, "trimLeft", js.Any.fromFunction0(value))
    
    inline def setTrimRight(value: () => java.lang.String): Self = StObject.set(x, "trimRight", js.Any.fromFunction0(value))
  }
}
