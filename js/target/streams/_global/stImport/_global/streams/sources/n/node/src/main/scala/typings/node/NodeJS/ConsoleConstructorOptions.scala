package typings.node.NodeJS

import typings.node.nodeStrings.auto
import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

trait ConsoleConstructorOptions extends StObject {
  
  var colorMode: js.UndefOr[Boolean | auto] = js.undefined
  
  var ignoreErrors: js.UndefOr[Boolean] = js.undefined
  
  var inspectOptions: js.UndefOr[InspectOptions] = js.undefined
  
  var stderr: js.UndefOr[WritableStream] = js.undefined
  
  var stdout: WritableStream
}
object ConsoleConstructorOptions {
  
  inline def apply(stdout: WritableStream): ConsoleConstructorOptions = {
    val __obj = js.Dynamic.literal(stdout = stdout.asInstanceOf[js.Any])
    __obj.asInstanceOf[ConsoleConstructorOptions]
  }
  
  @scala.inline
  implicit open class MutableBuilder[Self <: ConsoleConstructorOptions] (val x: Self) extends AnyVal {
    
    inline def setColorMode(value: Boolean | auto): Self = StObject.set(x, "colorMode", value.asInstanceOf[js.Any])
    
    inline def setColorModeUndefined: Self = StObject.set(x, "colorMode", js.undefined)
    
    inline def setIgnoreErrors(value: Boolean): Self = StObject.set(x, "ignoreErrors", value.asInstanceOf[js.Any])
    
    inline def setIgnoreErrorsUndefined: Self = StObject.set(x, "ignoreErrors", js.undefined)
    
    inline def setInspectOptions(value: InspectOptions): Self = StObject.set(x, "inspectOptions", value.asInstanceOf[js.Any])
    
    inline def setInspectOptionsUndefined: Self = StObject.set(x, "inspectOptions", js.undefined)
    
    inline def setStderr(value: WritableStream): Self = StObject.set(x, "stderr", value.asInstanceOf[js.Any])
    
    inline def setStderrUndefined: Self = StObject.set(x, "stderr", js.undefined)
    
    inline def setStdout(value: WritableStream): Self = StObject.set(x, "stdout", value.asInstanceOf[js.Any])
  }
}
