package typings.std

import typings.std.stdStrings.nfc_
import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

/* Rewritten from type alias, can be one of: 
  - typings.std.stdStrings.accelerometer
  - typings.std.stdStrings.`ambient-light-sensor`
  - typings.std.stdStrings.`background-fetch`
  - typings.std.stdStrings.`background-sync`
  - typings.std.stdStrings.bluetooth
  - typings.std.stdStrings.camera
  - typings.std.stdStrings.`clipboard-read`
  - typings.std.stdStrings.`clipboard-write`
  - typings.std.stdStrings.`device-info`
  - typings.std.stdStrings.`display-capture`
  - typings.std.stdStrings.geolocation
  - typings.std.stdStrings.gyroscope
  - typings.std.stdStrings.magnetometer
  - typings.std.stdStrings.microphone
  - typings.std.stdStrings.midi
  - typings.std.stdStrings.nfc_
  - typings.std.stdStrings.notifications
  - typings.std.stdStrings.`persistent-storage`
  - typings.std.stdStrings.push
  - typings.std.stdStrings.speaker
*/
trait PermissionName extends StObject
object PermissionName {
  
  inline def accelerometer: typings.std.stdStrings.accelerometer = "accelerometer".asInstanceOf[typings.std.stdStrings.accelerometer]
  
  inline def `ambient-light-sensor`: typings.std.stdStrings.`ambient-light-sensor` = "ambient-light-sensor".asInstanceOf[typings.std.stdStrings.`ambient-light-sensor`]
  
  inline def `background-fetch`: typings.std.stdStrings.`background-fetch` = "background-fetch".asInstanceOf[typings.std.stdStrings.`background-fetch`]
  
  inline def `background-sync`: typings.std.stdStrings.`background-sync` = "background-sync".asInstanceOf[typings.std.stdStrings.`background-sync`]
  
  inline def bluetooth: typings.std.stdStrings.bluetooth = "bluetooth".asInstanceOf[typings.std.stdStrings.bluetooth]
  
  inline def camera: typings.std.stdStrings.camera = "camera".asInstanceOf[typings.std.stdStrings.camera]
  
  inline def `clipboard-read`: typings.std.stdStrings.`clipboard-read` = "clipboard-read".asInstanceOf[typings.std.stdStrings.`clipboard-read`]
  
  inline def `clipboard-write`: typings.std.stdStrings.`clipboard-write` = "clipboard-write".asInstanceOf[typings.std.stdStrings.`clipboard-write`]
  
  inline def `device-info`: typings.std.stdStrings.`device-info` = "device-info".asInstanceOf[typings.std.stdStrings.`device-info`]
  
  inline def `display-capture`: typings.std.stdStrings.`display-capture` = "display-capture".asInstanceOf[typings.std.stdStrings.`display-capture`]
  
  inline def geolocation: typings.std.stdStrings.geolocation = "geolocation".asInstanceOf[typings.std.stdStrings.geolocation]
  
  inline def gyroscope: typings.std.stdStrings.gyroscope = "gyroscope".asInstanceOf[typings.std.stdStrings.gyroscope]
  
  inline def magnetometer: typings.std.stdStrings.magnetometer = "magnetometer".asInstanceOf[typings.std.stdStrings.magnetometer]
  
  inline def microphone: typings.std.stdStrings.microphone = "microphone".asInstanceOf[typings.std.stdStrings.microphone]
  
  inline def midi: typings.std.stdStrings.midi = "midi".asInstanceOf[typings.std.stdStrings.midi]
  
  inline def nfc: nfc_ = "nfc".asInstanceOf[nfc_]
  
  inline def notifications: typings.std.stdStrings.notifications = "notifications".asInstanceOf[typings.std.stdStrings.notifications]
  
  inline def `persistent-storage`: typings.std.stdStrings.`persistent-storage` = "persistent-storage".asInstanceOf[typings.std.stdStrings.`persistent-storage`]
  
  inline def push: typings.std.stdStrings.push = "push".asInstanceOf[typings.std.stdStrings.push]
  
  inline def speaker: typings.std.stdStrings.speaker = "speaker".asInstanceOf[typings.std.stdStrings.speaker]
}
