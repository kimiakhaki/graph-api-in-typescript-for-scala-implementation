package typings.node

import typings.node.NodeJS.Immediate
import typings.node.NodeJS.Timeout
import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

object timersMod {
  
  @JSImport("timers", JSImport.Namespace)
  @js.native
  val ^ : js.Any = js.native
  
  inline def clearImmediate(immediateId: Immediate): Unit = ^.asInstanceOf[js.Dynamic].applyDynamic("clearImmediate")(immediateId.asInstanceOf[js.Any]).asInstanceOf[Unit]
  
  inline def clearInterval(intervalId: Timeout): Unit = ^.asInstanceOf[js.Dynamic].applyDynamic("clearInterval")(intervalId.asInstanceOf[js.Any]).asInstanceOf[Unit]
  
  inline def clearTimeout(timeoutId: Timeout): Unit = ^.asInstanceOf[js.Dynamic].applyDynamic("clearTimeout")(timeoutId.asInstanceOf[js.Any]).asInstanceOf[Unit]
  
  inline def setImmediate(callback: js.Function1[/* repeated */ Any, Unit], args: Any*): Immediate = ^.asInstanceOf[js.Dynamic].applyDynamic("setImmediate")(scala.List(callback.asInstanceOf[js.Any]).`++`(args.asInstanceOf[Seq[js.Any]])*).asInstanceOf[Immediate]
  
  inline def setInterval(callback: js.Function1[/* repeated */ Any, Unit], ms: Double, args: Any*): Timeout = (^.asInstanceOf[js.Dynamic].applyDynamic("setInterval")((scala.List(callback.asInstanceOf[js.Any], ms.asInstanceOf[js.Any])).`++`(args.asInstanceOf[Seq[js.Any]])*)).asInstanceOf[Timeout]
  
  inline def setTimeout(callback: js.Function1[/* repeated */ Any, Unit], ms: Double, args: Any*): Timeout = (^.asInstanceOf[js.Dynamic].applyDynamic("setTimeout")((scala.List(callback.asInstanceOf[js.Any], ms.asInstanceOf[js.Any])).`++`(args.asInstanceOf[Seq[js.Any]])*)).asInstanceOf[Timeout]
}
