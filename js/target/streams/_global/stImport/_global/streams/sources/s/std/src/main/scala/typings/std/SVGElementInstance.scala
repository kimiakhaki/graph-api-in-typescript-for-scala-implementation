package typings.std

import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

@js.native
trait SVGElementInstance
  extends StObject
     with EventTarget {
  
  /* standard dom */
  val correspondingElement: org.scalajs.dom.SVGElement = js.native
  
  /* standard dom */
  val correspondingUseElement: org.scalajs.dom.SVGUseElement = js.native
}
