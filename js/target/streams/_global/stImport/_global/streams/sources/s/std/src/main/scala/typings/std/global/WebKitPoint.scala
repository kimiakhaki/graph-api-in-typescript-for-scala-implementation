package typings.std.global

import org.scalablytyped.runtime.StObject
import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobalScope, JSGlobal, JSImport, JSName, JSBracketAccess}

/* This class was inferred from a value with a constructor. In rare cases (like HTMLElement in the DOM) it might not work as you expect. */
@JSGlobal("WebKitPoint")
@js.native
/* standard dom */
open class WebKitPoint ()
  extends StObject
     with typings.std.WebKitPoint {
  def this(x: Double) = this()
  def this(x: Double, y: Double) = this()
  def this(x: Unit, y: Double) = this()
  
  /* standard dom */
  /* CompleteClass */
  var x: Double = js.native
  
  /* standard dom */
  /* CompleteClass */
  var y: Double = js.native
}
