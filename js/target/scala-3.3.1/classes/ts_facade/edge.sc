import typings.scalaTSFacade.srcMainTsEdgeMod
import scala.scalajs.js
import ts_facade.Node

trait edge extends srcMainTsEdgeMod.Edge {
  var id: js.UndefOr[String]
  var source: Node
  var target: Node
}