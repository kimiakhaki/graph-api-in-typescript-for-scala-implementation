package ts_facade

import typings.scalaTSFacade.srcMainTsNodeMod
import scala.scalajs.js

trait node extends srcMainTsNodeMod.Node {
  var id: String
  var title: js.UndefOr[String]
}