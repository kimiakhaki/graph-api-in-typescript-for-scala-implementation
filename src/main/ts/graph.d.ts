export interface Graph<NT, ET> {
    id: string;
    title: string;
    nodes: NT[];
    edges: ET[];
    addNode(node: NT): void;
    removeNode(nodeId: string): void;
    addEdge(edge: ET): void;
    removeEdge(source: NT, target: NT): void;
    size(): number;
}
