export interface Node {
    id: string,
    title: string,
}

export namespace Node {
    // export declare function mk(id: string, title?: string): Node;
    export interface mkInterface {
        mkFunction(id: string, title: string): Node;
    }

}