export interface Node {
    id: string;
    title: string;
}
export declare namespace Node {
    interface mkInterface {
        mkFunction(id: string, title: string): Node;
    }
}
