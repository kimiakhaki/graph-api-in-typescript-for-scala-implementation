import {Node} from "./node";

export interface Edge {
    id: string,
    title: string,
    source: Node,
    target: Node
}